package de.fu_berlin.crawler.utils;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fu_berlin.crawler.datastructure.Standard;

public class JexlExecuterUnitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void setAndGetAllStandardAttributesViaJexl() {
//		Standard testStandard = new Standard("title", "linkToStandard", null, new Date(), null, "status", "author", "standardID");
//		JexlExecutor jexlExecuter =  new JexlExecutor(testStandard);
//		assertEquals("Der Wert des Objektes sollte gleich dem aus Jexl sein.", (String) jexlExecuter.execute("currentStandard.linkToStandard"), testStandard.getLinkToStandard());
//		assertEquals("Der Wert des Objektes sollte gleich dem aus Jexl sein.", (String) jexlExecuter.execute("currentStandard.title"), testStandard.getTitle());
//		assertEquals("Der Wert des Objektes sollte gleich dem aus Jexl sein.", (String) jexlExecuter.execute("currentStandard.status"), testStandard.getStatus());
//		assertEquals("Der Wert des Objektes sollte gleich dem aus Jexl sein.", (String) jexlExecuter.execute("currentStandard.author"), testStandard.getAuthor());
//		assertEquals("Der Wert des Objektes sollte gleich dem aus Jexl sein.", (String) jexlExecuter.execute("currentStandard.standardID"), testStandard.getStandardID());
		
		
	}
	
	
	@Test
	public void testIfStatementInJexle(){
		Standard testStandard = new Standard("www.test.de");
		testStandard.setLinkToStandard("DSP1034_1.0");
		JexlExecutor jexlExecuter =  new JexlExecutor(testStandard);
		String execute1 = "if(currentStandard.linkToStandard.toString().length()<12){currentStandard.linkToStandard = currentStandard.linkToStandard +'.0'}";
		String execute2 = "currentStandard.linkToStandard = currentStandard.linkToStandard +'.0'";
		String execute3 = "currentStandard.linkToStandard = currentStandard.linkToStandard.toString().length()";
		jexlExecuter.execute(execute1);
		System.out.println(testStandard);
		
		
		Standard w3cStandard = new Standard("www.test.de");
		w3cStandard.setID("WD-xhtml-prof-req-19990906");
		JexlExecutor jexlExecuterW3c =  new JexlExecutor(w3cStandard);
		//String execute1w3c = "currentStandard.ID = currentStandard.ID.subString(currentStandard.ID.indexOf('-'))";
		//String execute2 = "currentStandard.linkToStandard = currentStandard.linkToStandard +'.0'";
		//String execute3 = "currentStandard.linkToStandard = currentStandard.linkToStandard.toString().length()";
		//jexlExecuterW3c.execute(execute1w3c);
		System.out.println(w3cStandard);
	}

}

package de.fu_berlin.crawler;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Zum Starten von Konfigurationen.
 * 
 * 
 * @author Jens.Hantke
 *
 */
public class ConfigLoaderJUnitTest {
	
	/**
	 * Wir erwarten keine Fehler. Daher werden alle Fehler aufgefuehrt.
	 */
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	/**
	 * Dieser Test soll sicherstellen, das das gesamte Werkzeug mit der Default Configuration durchlaeuft.
	 * Dabei wird das LogLevel so gesetzt, das alles ausgegeben wird.
	 * Dieser Test bietet sich an um auf Jenkins zu testen und im Nachhinein den gesamten Ueberblick zu haben.
	 * 
	 */
	@Test
	public void testRunWithConfigLoader() {
		//Logger.getRootLogger().setLevel(Level.ALL);
		String[] xml = {"config/DmtfCrawlerConfig.xml"};
		ConfigLoader.main(xml);
	}
	
	/**
	 * Dieser Test soll sicherstellen, das das gesamte Werkzeug mit der Default Configuration durchlaeuft.
	 * Dabei wird das LogLevel so gesetzt, das alles ausgegeben wird.
	 * Dieser Test bietet sich an um auf Jenkins zu testen und im Nachhinein den gesamten Ueberblick zu haben.
	 * 
	 */
	@Test
	public void testRunWithW3CConfigLoader() {
		//Logger.getRootLogger().setLevel(Level.ALL);
		String[] xml = {"config/W3CCrawlerConfig.xml"};
		ConfigLoader.main(xml);
	}
	
	@Test
	public void testRunWithIETFConfigLoader() {
		//Logger.getRootLogger().setLevel(Level.ALL);
		String[] xml = {"config/IETFCrawlerConfig.xml"};
		ConfigLoader.main(xml);
	}

	@Test
	public void testRunWithDMTFAnalyseOnlyConfigLoader() {
		//Logger.getRootLogger().setLevel(Level.ALL);
		String[] xml = {"config/DmtfConfigAnalyseOnly.xml"};
		ConfigLoader.main(xml);
	}
	
	
	@Test
	public void testRunWithW3CAnalyseOnlyConfigLoader() {
		//Logger.getRootLogger().setLevel(Level.ALL);
		String[] xml = {"config/W3CCrawlerConfigAnalyseOnly.xml"};
		ConfigLoader.main(xml);
	}
	
	 @Test
	  public void testRunWithCustomAnalyseOnlyConfigLoader() {
	    //Logger.getRootLogger().setLevel(Level.ALL);
	    String[] xml = {"config/DmtfConfigAnalyseOnly_CountTimeInterval.xml"};
	    ConfigLoader.main(xml);
	  }
	 
	 @Test
	  public void testRunWithW3CCustomAnalyseOnlyConfigLoader() {
	    //Logger.getRootLogger().setLevel(Level.ALL);
	    String[] xml = {"config/W3CCrawlerConfigAnalyseOnly_Correlation.xml"};
	    ConfigLoader.main(xml);
	  }


}

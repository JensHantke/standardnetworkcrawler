package de.fu_berlin.crawler.ContentExtractor;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import de.fu_berlin.crawler.Configuration.SaxXmlParser;
import de.fu_berlin.crawler.datastructure.Standard;
/**
 * Zum Ausprobieren von Pattern, nicht zum Testen.
 */
public class PatternTest {
	
		@Test
		public void testPattern() {
			List<String> liste = Arrays.asList("http://www.w3.org/TR/1999/WAI-WEBCONTENT-19990505.html","http://www.w3.org/TR/1999/WAI-WEBCONTENT-19990505/index.html"
					,"http://www.w3.org/TR/2004/REC-rdf-syntax-grammar-20040210/index.html","http://localhost/Uni/www.w3.org/TR/CSS2/index.html",
					"http://localhost/Uni/www.w3.org/TR/REC-html40/index.html","http://localhost/Uni/www.w3.org/TR/1998/REC-xml-19980210.html","http://www.w3.org/TR/2004/REC-xml-names11-20040204");
			String pattern ="(((https?|ftp|file):((//)|(\\\\))*|www.)[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*w3.org[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*TR[\\w\\d/-]*)";
			//compilePattern(pattern,liste,-1);
			//System.out.println("Pattern.CASE_INSENSITIVE");
			//compilePattern(pattern,liste, Pattern.CASE_INSENSITIVE);
			
			
			String pattern2 ="/[\\w]*(-[\\w]*)+(-[\\d]*)?(/|.html)";
			String pattern3 ="/[\\w]*-[\\w]*-[\\d]*/";
			String pattern4 ="/[\\w]*(-[\\w]*)+(-[\\d]{8})";
			compilePattern(pattern4,liste, Pattern.CASE_INSENSITIVE);
			
			String dmtfPattern = "DSP\\d+_\\d(\\.\\d)*";
			List<String> dmtfListe = Arrays.asList("DSP4004_2.2V3","http://localhost/DMTFNeu/www.dmtf.org/sites/default/files/standards/documents/DSP0004_2.5.0.pdf");
			compilePattern(dmtfPattern,dmtfListe, Pattern.CASE_INSENSITIVE);
			
			String dmtfVersionPattern = "(\\d)(\\.\\d)(\\.\\d)";
			List<String> dmtfVersionen = Arrays.asList("Version: 1.0.2 4");
			compilePattern(dmtfVersionPattern,dmtfVersionen, Pattern.CASE_INSENSITIVE);
			
			String test = "DMTF Work-In-Progress";
			List<String> test2 = Arrays.asList(" .pdf file. DMTF Work-In-Progress, expiring: 2013-07-15");
			compilePattern(test,test2, Pattern.CASE_INSENSITIVE);
			
			
			String member = "(Apple$)|(Apple[\\s,])";
			List<String> memberTarget = Arrays.asList("Applet class file. See the code attr" , "Apple", "Apple ","Eric Carlson, Apple, Inc.");
			compilePattern(member,memberTarget, Pattern.CASE_INSENSITIVE);

			
//			String w3cVersionPattern = "(\\d{8})";
//			String w3cTitlePattern = "[a-zA-Z]*(-([0-9]{1,2})?[a-zA-Z]+([0-9]{1,2})?)+";
//			List<String> w3cVersionen = Arrays.asList("WD-xhtml-prof-req-19990906","WD-2dcontext2-20121217","WD-its20-20121206","NOTE-xhtml1-schema-20020902");
//			compilePattern(w3cVersionPattern,w3cVersionen, Pattern.CASE_INSENSITIVE);
//			compilePattern(w3cTitlePattern,w3cVersionen, Pattern.CASE_INSENSITIVE);
			
			String w3cTitlePattern1 = "([a-zA-Z]*([0-9]{1,6})?[a-zA-Z]+([0-9]{1,6})?[a-zA-Z]*)(-(([0-9]{1,6})?[a-zA-Z]+([0-9]{1,6})?))*(-[\\.0-9]{1,5}-[a-zA-Z]+|-[\\.0-9]{1,5}-)?";
			List<String> w3cTitle1 = Arrays.asList("xhtml-prof-req-19990906","2dcontext2-20121217","its20-20121206","xhtml1-schema-20020902","CR-mediaont-api-1.0-20020902","CR-mediaont-api-1-20020902","REC-DOM-Level-2-Events-20001113","WD-P3P-arch-971023","REC-CSS2-20110607","WD-http-pep-960220");
			compilePattern(w3cTitlePattern1,w3cTitle1, Pattern.CASE_INSENSITIVE);
			
			
			
			String w3cVersionPattern2 = "(\\d{8})";
			List<String> w3cVersionen2 = Arrays.asList("WD-xhtml-prof-req-19990906","WD-2dcontext2-20121217","WD-its20-20121206","NOTE-xhtml1-schema-20020902");
			compilePattern(w3cVersionPattern2,w3cVersionen2, Pattern.CASE_INSENSITIVE);
			
//			String dmtfStatusPattern2 = "(\\d{8})";
//			List<String> dmtfStatus = Arrays.asList("WD-xhtml-prof-req-19990906","WD-2dcontext2-20121217","WD-its20-20121206","NOTE-xhtml1-schema-20020902");
//			compilePattern(dmtfStatusPattern2,dmtfStatus, Pattern.CASE_INSENSITIVE);
			
			String dmtfStatusPattern2 = "STATUS: (\\w*) (\\w*)?";
			List<String> dmtfStatus = Arrays.asList("Document Status: DMTF Standard");
			compilePattern(dmtfStatusPattern2,dmtfStatus, Pattern.CASE_INSENSITIVE);
			
			String dmtfStatusPattern3 = "DMTF ([ \\-_a-zA-Z])+";
      List<String> dmtfStatus3 = Arrays.asList("DMTF Specifications (DSP0001-0999)");
      compilePattern(dmtfStatusPattern3,dmtfStatus3, Pattern.CASE_INSENSITIVE);
			
			List<String> dmtfPdfFind = Arrays.asList("Normative references: * DMTF DSP0004, CIM Infrastructure Specification 2.6, http://www.dmtf.org/standards/published_documents/DSP0004_2.6.pdf * DMTF DSP8028, Management Profile XML Schema 1.0, http://schemas.dmtf.org/wbem/mgmtprofile/1.0/dsp8028_1.0.xsd * IETF RFC3066, Tags for the Identification of Languages, January 2001, http://tools.ietf.org/html/rfc3066 * W3C Extensible Markup Language (XML) 1.0 (Second Edition), W3C Recommendation 6 October 2000, http://www.w3.org/TR/2000/REC-xml-20001006 * W3C XML Schema Part 0: Primer (Second Edition), W3C Recommendation 28 October 2004, http://www.w3.org/TR/2004/REC-xmlschema-0-20041028 * W3C XML Schema Part 1: Structures (Second Edition), W3C Recommendation 28 October 2004, http://www.w3.org/TR/2004/REC-xmlschema-1-20041028 * W3C XML Schema Part 2: Datatypes (Second Edition), W3C Recommendation 28 October 2004, http://www.w3.org/TR/2004/REC-xmlschema-2-20041028 * ISO/IEC Directives, Part 2:2004, Rules for the structure and drafting of International Standards, http://isotc.iso.org/livelink/livelink?func=ll&objId=4230456&objAction=browse");
			String dmtfPdfPattern = "(((https?|ftp|file):((//)|(\\\\))*|www.)[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*dmtf.org[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*(dsp|wbem.|CIM_HTTP_Mapping)\\d+[\\d_.]*.(xsl|pdf|xsd|en|xml|html|txt))";
			compilePattern(dmtfPdfPattern,dmtfPdfFind, Pattern.CASE_INSENSITIVE);
			
			List<String> w3cidentifikation = Arrays.asList("http://www.w3.org/TR/html401/","http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001","/REC-DOM-Level-2-Core-20001113","http://www.w3.org/TR/1998/NOTE-datetime-19980827","http://www.w3.org/TR/1999/REC-html401-19991224");
			String w3cidentifikationpattern = "/[\\w]*(-[\\w]*)+(-[\\d]{8})?(/|.html)?";
			compilePattern(w3cidentifikationpattern,w3cidentifikation, Pattern.CASE_INSENSITIVE);
		}
		
	private static void compilePattern(String pattern, List<String> targets, int sensitiv) {
		Pattern attributePattern4;
		if (sensitiv != -1) {
			attributePattern4 = Pattern.compile(pattern, sensitiv);
		} else {
			attributePattern4 = Pattern.compile(pattern);
		}
		for (String target : targets) {
			Matcher matcher4 = attributePattern4.matcher(target);
			if (matcher4.find()) {
				System.out.println("+" + matcher4.group().toString());
			} else {
				System.out.println("-" + target);
			}

		}
	}
}

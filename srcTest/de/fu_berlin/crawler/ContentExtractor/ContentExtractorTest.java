package de.fu_berlin.crawler.ContentExtractor;

import org.junit.Test;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.SaxXmlParser;
import de.fu_berlin.crawler.datastructure.Standard;

public class ContentExtractorTest {
	
	
	
	@Test
	public void testAttributeExtractorInStandardDocument() {
		
			System.out.println(Identifier.validateURL("http://localhost/Uni/www.w3.org/TR/SOAP/index-2.html"));
			
			W3C.testHTMLStandard("http://localhost/Uni/www.w3.org/TR/1999/WAI-WEBCONTENT-19990505");
			W3C.testHTMLStandard("http://localhost/Uni/www.w3.org/TR/2003/NOTE-soap12-n11n-20031008/index.html");
			
	}
	
	@Test
	public void testDMTFExtractor() {
		
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP1059_1.0.0.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0223_1.0.0_1.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0004_2.6.0_0.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0004V2.3_final.pdf");
		DMTF.testPdfStandard("http://localhost/DMTFNeu/www.dmtf.org/sites/default/files/standards/documents/DSP1001_1.0.1.pdf");
		DMTF.testPdfStandard("http://localhost/DMTFNeu/www.dmtf.org/sites/default/files/standards/documents/DSP1001_1.1.0_0.pdf");
		
			
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0236_1.0.0.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0249_1.0.0.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0134v2.5Final.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0216_1.0.0.pdf");
		DMTF.testPdfStandard("http://localhost/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0105.pdf");
		
		DMTF.testXMLStandard("http://schemas.dmtf.org/wbem/wsman/1/dsp8035_1.0.wsdl");
		DMTF.testXMLStandard("http://schemas.dmtf.org/wbem/messageregistry/1/dsp8007_1.0.xml");
		DMTF.testXMLStandard("http://schemas.dmtf.org/ovf/networkportprofile/1/dsp8049_1.0.0.xsd");
		DMTF.testXMLStandard("http://schemas.dmtf.org/wbem/mgmtprofile/1/dsp8029_1.0.1.xsl");
		DMTF.testXMLStandard("http://localhost/DMTFNeu/schemas.dmtf.org/wbem/metricregistry/1/dsp8024_1.0.xsl");
		
		
		DMTF.testTextStandard("http://dmtf.org/sites/default/files/standards/documents/wbem.1.0.en");
		DMTF.testTextStandard("http://localhost/DMTFNeu/www.dmtf.org/sites/default/files/standards/documents/DSP0206_2.0.0.txt");
	
		DMTF.testHTMLStandard("http://dmtf.org/sites/default/files/standards/documents/CIM_HTTP_Mapping10.html");
		DMTF.testHTMLStandard("http://www.dmtf.org/sites/default/files/standards/documents/DSP0200_1.1.0.html");
		DMTF.testHTMLStandard("http://www.dmtf.org/sites/default/files/standards/documents/DSP200.html");
		DMTF.testHTMLStandard("http://www.dmtf.org/sites/default/files/standards/documents/CIM_XML_Mapping20.html");
		DMTF.testHTMLStandard("http://www.dmtf.org/sites/default/files/standards/documents/DSP201.html");
			
	}
	
	public static class DMTF {
	
		public static void testPdfStandard(String link){
			SaxXmlParser parser = new SaxXmlParser("config/test_config/ContentExtractor.xml");
			PDFContent pdf = new PDFContent( Config.getConfigNode("PDFContent", Config.getRootConfigNode()) );
			Standard standard = new  Standard("Test Standard", link, null);
			pdf.crawlContent(standard);
			System.out.println(standard);
		}
		
		public static void testXMLStandard(String link){
			Config parser = new Config("config/test_config/ContentExtractor.xml");
			XMLCommentContent xml = new XMLCommentContent(Config.getConfigNode( "XMLCommentContent", Config.getRootConfigNode()) );
			Standard standard = new  Standard("Test Standard", link, null);
			xml.crawlContent(standard);
			System.out.println(standard);
		}
		
		public static void testTextStandard(String link){
			Config parser = new Config("config/test_config/ContentExtractor.xml");
			TextContent xml = new TextContent(Config.getConfigNode( "TextContent", Config.getRootConfigNode()) );
			Standard standard = new  Standard("Test Standard", link, null);
			xml.crawlContent(standard);
			System.out.println(standard);
		}
		
		public static void testHTMLStandard(String link){
			Config parser = new Config("config/test_config/ContentExtractor.xml");
			HtmlContent html = new HtmlContent(Config.getConfigNode( "HtmlContent", Config.getRootConfigNode()) );
			Standard standard = new  Standard("Test Standard", link, null);
			html.crawlContent(standard);
			System.out.println(standard);
		}
	}
	
	public static class W3C{
		
		private static String PATH_TO_TEST_XML_CONFIG = "config/test_config/W3CContentExtractor.xml";
		
		public static void testHTMLStandard(String link){
			Config parser = new Config(PATH_TO_TEST_XML_CONFIG);
			HtmlContent html = new HtmlContent(Config.getConfigNode( "HtmlContent", Config.getRootConfigNode()) );
			Standard standard = new  Standard("Test Standard", link, null);
			html.crawlContent(standard);
		}
		
	}

}

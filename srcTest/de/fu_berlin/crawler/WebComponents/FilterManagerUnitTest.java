package de.fu_berlin.crawler.WebComponents;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.Configuration.FilterManager;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.utils.JexlExecutor;

public class FilterManagerUnitTest {

	static String crawlerConfig = "config/test_config/FilterManagerConfig.xml";

	private static FilterManager filterManager;
	static FilterManager manager;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Config config = new Config(crawlerConfig);
		ConfigNode filtermanager = Config.getConfigNode("FilterManager", Config.getRootConfigNode());
		manager = new FilterManager(filtermanager);
	}
	
	@Test
	public void testDMFT() {
	  
	  JexlExecutor executer = new JexlExecutor(new HtmlElementWrapper("../sites/default/files/standards/documents/DSP-IS0403_1.0.0.pdf","DSP-IS0403"));
	  System.out.println(manager.executeFilters(executer));
	  testFilterTree("http://schemas.dmtf.org/wbem/messageregistry/1/dsp8000_1.0.0.xsl","dsp8000");
//    assertTrue( filterManager.executeFilters("../sites/default/files/standards/documents/DSP-IS0403_1.0.0.pdf") );
//		assertTrue( filterManager.executeFilters("http://192.168.178.46/DMTF/www.dmtf.org/sites/default/files/standards/documents/DSP0004_2.7.0.pdf") );
//		assertFalse( filterManager.executeFilters("smbios.html") );
//		assertFalse( filterManager.executeFilters("feedback.html") );
//		assertFalse( filterManager.executeFilters("http://192.168.178.46/DMTF/www.dmtf.org/standards/historical_documents.html") );
//		assertFalse( filterManager.executeFilters("http://www.dmtf.org/user") );
//		assertFalse( filterManager.executeFilters("http://schemas.dmtf.org/wbem/wsman/identity/1/wsmanidentity.xsd") );
//		assertFalse( filterManager.executeFilters("http://schemas.dmtf.org/wbem/messageregistry/1/dsp8000_1.0.0.xsl") );
	}
	
	private void testFilterTree(String testString,String text) {
	  JexlExecutor executer = new JexlExecutor(new HtmlElementWrapper(testString,text));
	  assertTrue("Die URL '"+testString+"' und der Text '"+text+"' sollten durch den Filterbaum akzeptert werden.", manager.executeFilters(executer));
	}


}

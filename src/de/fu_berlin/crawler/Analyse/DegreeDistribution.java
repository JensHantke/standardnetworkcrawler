package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.Degree;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;

/**
 * Analysiert die Knotengradverteilung des eingelesenen Graphen.
 * Dazu kann der Ingrad, Ausgrad und Grad als Option angegeben werden.
 * Weiterhin kann angegeben werden, ob die Wahrscheinlichkeit und die Ckumulative Wahrscheinlichkeit hinzugezogen werden sollen.
 * 
 * @author Jens Hantke
 *
 */
public class DegreeDistribution {
	boolean asProbability = false;
	boolean asCommulativeProbability = false;
	String degreeVariant;
	Map<String, String> toolLabelTogephiToolLabel = new HashMap<String, String>();
	
	
	public DegreeDistribution(ConfigNode degreeDistributionNode) {
		Map<String, String> attributes = degreeDistributionNode.getAttributes();
		/*Bilden die Werkzeug eigene Labels fuer Degree auf die von Gephie ab, da diese sich eventuell aendern.*/
		toolLabelTogephiToolLabel.put(DegreeOfDistribution.INDEGREE, Degree.INDEGREE);
		toolLabelTogephiToolLabel.put(DegreeOfDistribution.DEGREE, Degree.DEGREE);
		toolLabelTogephiToolLabel.put(DegreeOfDistribution.OUTDEGREE, Degree.OUTDEGREE);
		
		fillAttributes(attributes);
	}

	/**
	 * @param filterAttributes
	 */
	private void fillAttributes(Map<String, String> attributes) {
		asProbability = getBooleanFromAttribute("asProbability",attributes);
		asCommulativeProbability = getBooleanFromAttribute("asCommulativeProbability",attributes);
		degreeVariant = attributes.get("Target");
		if(!DegreeOfDistribution.Variants.contains(degreeVariant)) {
			degreeVariant = null;
		} else {
			degreeVariant = toolLabelTogephiToolLabel.get(degreeVariant);
		}
	}
	
	
	/**
	 * Holt dich Boolean aus dem XMLAttribut herraus.
	 * 
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	private boolean getBooleanFromAttribute(String attributeName, Map<String, String> attributes)
	{
		String tempOption = attributes.get(attributeName);
		if(tempOption != null) {
			if(tempOption.toString().equals("true"))	{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Fuehrt die Untersuchung der Knotengradverteilung aus.
	 * 
	 * @param graphModel
	 * @param attributeModel
	 * @param graph
	 */
	public void executeDistribution(GraphModel graphModel, AttributeModel attributeModel, Graph graph) {
		int nodes = graph.getNodeCount();
		Degree degree = new Degree();
		degree.execute(graphModel, attributeModel);
		String timeStamp = CrawlerTimeUtils.getFormatedTime()+"_";
		OutputHelper.writeCsv(""+degreeVariant+";NumberOfNodesWithDegree;"+degreeVariant+"Probability;"+degreeVariant+"CommulativeProbability", timeStamp+""+degreeVariant+"Distribution");
		Map<Integer, Integer> mapOfDegreeToCountOfNodes = new TreeMap<Integer, Integer>();
		for (Node node : graph.getNodes().toArray()) {
			int currentDegree = Integer.parseInt(node.getNodeData().getAttributes().getValue(degreeVariant).toString());
			if (mapOfDegreeToCountOfNodes.containsKey(currentDegree)) {
				int countOfCurrentDegree = mapOfDegreeToCountOfNodes.get(currentDegree);
				countOfCurrentDegree++;
				mapOfDegreeToCountOfNodes.put(currentDegree, countOfCurrentDegree);
			} else {
				mapOfDegreeToCountOfNodes.put(currentDegree, 1);
			}
		}
		for (Entry<Integer, Integer> entry : mapOfDegreeToCountOfNodes.entrySet()) {
			int numberOfNodes = entry.getValue();
			int degreeOfNodes = entry.getKey();
			String concatCSV = entry.getKey() + ";" + numberOfNodes;
			if (asProbability) {
				concatCSV += ";" + ((float) numberOfNodes / (float) nodes);
			}
			if (asCommulativeProbability) {
				int sumUpAllWithDegreeGreaterAndEQ = 0;
				for(Integer currentDegree : mapOfDegreeToCountOfNodes.keySet()){
					if(currentDegree >= degreeOfNodes){
						sumUpAllWithDegreeGreaterAndEQ += mapOfDegreeToCountOfNodes.get(currentDegree);
					}
				}
				concatCSV += ";" + ((float) sumUpAllWithDegreeGreaterAndEQ / (float) nodes);
			}
			OutputHelper.writeCsv(concatCSV, timeStamp+""+degreeVariant+"Distribution");
		}
	}
	
	private interface DegreeOfDistribution {
		public String INDEGREE = "Indegree";
		public String OUTDEGREE = "Outdegree";
		public String DEGREE = "Degree";
		List<String> Variants = new ArrayList<String>(Arrays.asList(INDEGREE, OUTDEGREE, DEGREE));
	}

}

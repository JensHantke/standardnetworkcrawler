package de.fu_berlin.crawler.Analyse;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gephi.data.attributes.api.AttributeColumn;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.data.attributes.type.Interval;
import org.gephi.dynamic.api.DynamicController;
import org.gephi.dynamic.api.DynamicGraph;
import org.gephi.dynamic.api.DynamicModel;
import org.gephi.dynamic.api.DynamicModel.TimeFormat;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.api.Range;
import org.gephi.filters.plugin.graph.DegreeRangeBuilder.DegreeRangeFilter;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.UndirectedGraph;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.ranking.api.Ranking;
import org.gephi.ranking.api.RankingController;
import org.gephi.ranking.api.Transformer;
import org.gephi.ranking.plugin.transformer.AbstractColorTransformer;
import org.gephi.ranking.plugin.transformer.AbstractSizeTransformer;
import org.gephi.statistics.plugin.Degree;
import org.gephi.statistics.plugin.GraphDensity;
import org.gephi.statistics.plugin.GraphDistance;
import org.openide.util.Lookup;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.utils.AnalyseUtil;
import de.fu_berlin.crawler.utils.OutputHelper;

/**
 * Anaylisert die angegebenen Graphen.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkAnalyzer {


  // Gephi Toolkit Variablen:
  private File file;
  private ProjectController pc;
  private Workspace workspace;
  private AttributeModel attributeModel;
  private PreviewModel model;
  private ImportController importController;
  private FilterController filterController;
  private RankingController rankingController;
  private GraphModel graphModel;
  private DirectedGraph graph;
  private DynamicController dc;
  private DynamicModel dynamicmodel;
  private DynamicGraph dyGraph;
  
  // Metriken: 
  private CountOldReferences countOldReferences;
  private CountTimeInterval countTimeInterval;
  private NetworkAnalyseMultiDataAttribute multiData;
  public String pathToTarget = null;
  public String targetGraphId = null;
  private List<DegreeDistribution> degDistributions = null;
  private List<NetworkResilience> networkResiliences = null;
  private List<NetworkTimeDynamic> networkTimeDynamics = null;
  private NetworkCorrelation networkCorrelation = null;
  
  //Plott des Graphen:
  private int minDegreeForPlott = 0;
  private ConfigNode plottNode;

  /**
   * Lie�t die Konfiguration ein und instanziiert die Metriken Objekte.
   * 
   * @param xmlNetworkAnalyzerNode
   */
  public NetworkAnalyzer(ConfigNode xmlNetworkAnalyzerNode) {
    Map<String, String> attributes = xmlNetworkAnalyzerNode.getAttributes();
    networkResiliences = new ArrayList<NetworkResilience>();
    networkTimeDynamics = new ArrayList<NetworkTimeDynamic>();
    degDistributions = new ArrayList<DegreeDistribution>();

    fillNetworkAnalyzerFromXmlAttributes(attributes);
    List<ConfigNode> degDistributionNodes = Config.getConfigNodes("DegreeDistribution", xmlNetworkAnalyzerNode);
    if (degDistributionNodes.size() > 0) {
      for (ConfigNode xmlAttributeElement : degDistributionNodes) {
        degDistributions.add(new DegreeDistribution(xmlAttributeElement));
      }
    }

    List<ConfigNode> networkResilienceNodes = Config.getConfigNodes("NetworkResilience", xmlNetworkAnalyzerNode);
    if (networkResilienceNodes.size() > 0) {
      for (ConfigNode xmlAttributeElement : networkResilienceNodes) {
        networkResiliences.add(new NetworkResilience(xmlAttributeElement));
      }
    }

    List<ConfigNode> networkTimeDynamicNodes = Config.getConfigNodes("NetworkTimeDynamic", xmlNetworkAnalyzerNode);
    if (networkTimeDynamicNodes.size() > 0) {
      for (ConfigNode xmlAttributeElement : networkTimeDynamicNodes) {
        networkTimeDynamics.add(new NetworkTimeDynamic(xmlAttributeElement));
      }
    }

    if (Config.getConfigNode("NetworkCorrelation", xmlNetworkAnalyzerNode) != null) {
      networkCorrelation = new NetworkCorrelation(Config.getConfigNode("NetworkCorrelation", xmlNetworkAnalyzerNode));
    }

    if (Config.getConfigNode("CountOldReferences", xmlNetworkAnalyzerNode) != null) {
      countOldReferences = new CountOldReferences(Config.getConfigNode("CountOldReferences", xmlNetworkAnalyzerNode));
    }

    if (Config.getConfigNode("NetworkMultiDataAnalyse", xmlNetworkAnalyzerNode) != null) {
      multiData = new NetworkAnalyseMultiDataAttribute(Config.getConfigNode("NetworkMultiDataAnalyse", xmlNetworkAnalyzerNode));
    }
    if (Config.getConfigNode("CountTimeInterval", xmlNetworkAnalyzerNode) != null) {
      countTimeInterval = new CountTimeInterval(Config.getConfigNode("CountTimeInterval", xmlNetworkAnalyzerNode));
    }
    plottNode = Config.getConfigNode("Plott", xmlNetworkAnalyzerNode);
    if(plottNode != null){
      Object tempInt = plottNode.getAttribute("minDegree");
      if(tempInt != null && !tempInt.equals("")) {
        minDegreeForPlott = Integer.parseInt( plottNode.getAttribute("minDegree"));
      }
    }
  }

  /**
   * Fuellt die Variablen anhand der konfigurierten XmlAttribute.
   * 
   * @param filterAttributes
   */
  private void fillNetworkAnalyzerFromXmlAttributes(Map<String, String> filterAttributes) {
    pathToTarget = (String) filterAttributes.get("PathToTarget");
    targetGraphId = (String) filterAttributes.get("TargetGraphId");
  }

  /**
   * Fuehrt verschiedene Analyse aus, die in der Konfiguration angegeben wurden.
   * 
   * @param pathToFile - Pfad zur Graphen-Datei
   */
  public void executeAnalyse(String pathToFile) {

    createGraphFromFile(pathToFile);
    
    if (multiData != null) {
      multiData.printDataDistribution(graph);
    }
    if (countTimeInterval != null) {
      countTimeInterval.countTimeInterval(graph);
    }
    if (networkCorrelation != null) {
      networkCorrelation.calculateCorrelation(graph);
    }
    if (countOldReferences != null) {
      countOldReferences.countOldReferences(graph);
    }
    for (DegreeDistribution degDistribution : degDistributions) {
      degDistribution.executeDistribution(graphModel, attributeModel, graph);
    }
    UndirectedGraph graphVisible = graphModel.getUndirectedGraphVisible();

    // Speichern Standardmetriken:
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "Nodes", graph.getNodeCount());
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "Edges", graph.getEdgeCount());
    GraphDistance distance = new GraphDistance();
    distance.setDirected(true);
    distance.execute(graphModel, attributeModel);
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "AveragePathLength", distance.getPathLength());
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "Diameter", distance.getDiameter());

    Degree degree = new Degree();
    degree.execute(graphModel, attributeModel);
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "AverageDegree", degree.getAverageDegree());

    GraphDensity density = new GraphDensity();
    density.execute(graphModel, attributeModel);
    density.setDirected(true);
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "AverageDensity", density.getDensity());
    
    if(plottNode != null){
      this.plott(pathToFile);
    }
    
    // Robustheit zum Schluss, dennn diese kann laenger Dauern
    for (NetworkResilience networkResilience : networkResiliences) {
      networkResilience.executeNetworkResilienceTest(graphModel, attributeModel, graphVisible);
    }
  }

  
  /**
   * Fuehrt die Analysen fuer den zeitlich dynamischen Graphen aus.
   *  - Einlesen des Graphen von einer Datei
   *  - Starten der Analyse fuer dynamische Graphen 
   * 
   * @param pathToFile - Pfad zur Graphen-Datei
   */
  public void executeDynamicAnalyse(String pathToFile) {
    createDynamicGraphFromFile(pathToFile);
    for (NetworkTimeDynamic networkTimeDynamic : networkTimeDynamics) {
      networkTimeDynamic.executeNetworkTimeDynamic(graphModel, dynamicmodel, attributeModel, graph, dyGraph);
    }
  }
  
  /**
   * Zeichnet den Graphen in die angegeben PDF.
   * Ist am Beispiel vom Gephi Toolkit selbst orientiert.
   * Der Graph wird anhand des YifanHuLayouts gezeichnet und ein Grad Filter fuer die Farbe benutzt.
   *  Die Knotengrae�e wird protortional der Centralitaet gehalten.
   * 
   * @param pathToFile - Pfad zur Graphen-Datei
   */
  public void plott(String pathToFile) {
    
    //
   
    // Filter des Grades
    DegreeRangeFilter degreeFilter = new DegreeRangeFilter();
    degreeFilter.init(graph);
    degreeFilter.setRange(new Range(minDegreeForPlott, Integer.MAX_VALUE));
    
    Query query = filterController.createQuery(degreeFilter);
    GraphView graphView = filterController.filter(query);
    graphModel.setVisibleView(graphView);

    // lassen YifanHu-Algorithmus 100 mal laufen
    YifanHuLayout yiHuLayout = new YifanHuLayout(null, new StepDisplacement(1f));
    yiHuLayout.setGraphModel(graphModel);
    yiHuLayout.resetPropertiesValues();
    yiHuLayout.setOptimalDistance(200f);

    yiHuLayout.initAlgo();
    for (int i = 0; i < 100 && yiHuLayout.canAlgo(); i++) {
      yiHuLayout.goAlgo();
    }
    yiHuLayout.endAlgo();
    
    
    // Farben der Grade fuer den Plott
    Ranking degreeRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, Ranking.DEGREE_RANKING);
    AbstractColorTransformer colorTransformer = (AbstractColorTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_COLOR);
    colorTransformer.setColors(new Color[] { new Color(0xFEF0D9), new Color(0xB30000) });
    rankingController.transform(degreeRanking, colorTransformer);

    // Grae�e der Knoten fuer den Plott
    AttributeColumn centralityColumn = attributeModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
    Ranking centralityRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, centralityColumn.getId());
    AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
    sizeTransformer.setMinSize(4);
    sizeTransformer.setMaxSize(11);
    rankingController.transform(centralityRanking, sizeTransformer);

    // Optionen fuer den Plott, in Gephi zu finden unter Preview
    model.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
    model.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(Color.GRAY));
    model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, new Float(0.1f));
    model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));

    // Schreiben der Datei:
    ExportController exportController = Lookup.getDefault().lookup(ExportController.class);
    try {
      exportController.exportFile(new File(OutputHelper.CSV_OUTPUT_DIR + getFileName(pathToFile) + ".pdf"));
    } catch (IOException exception) {
      exception.printStackTrace();
      return;
    }

  }

  /**
   * Lie�t einen Graphen aus einer Graphen-Datei.
   * 
   * @param pathTofile
   */
  private void createGraphFromFile(String pathTofile) {
    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
    if (workspace != null) {
      pc.deleteWorkspace(workspace);
    }
    pc.newProject();
    workspace = pc.getCurrentWorkspace();

    // Modelle, Controller,... Vorbereitung fuer den Workspace
    attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
    graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
    model = Lookup.getDefault().lookup(PreviewController.class).getModel();
    importController = Lookup.getDefault().lookup(ImportController.class);
    filterController = Lookup.getDefault().lookup(FilterController.class);
    rankingController = Lookup.getDefault().lookup(RankingController.class);

    // Einlesen der Datei
    Container container;
    try {
      System.out.println(pathTofile);
      File file = new File(pathTofile);
      container = importController.importFile(file);
      Thread.sleep(10000);
      container.getLoader().setEdgeDefault(EdgeDefault.DIRECTED); // wuerden spaeter auf Undirected umstellen
      /* Knoten werden nicht automatisch erstellt, denn dann wuerden Knoten eingefuegt die nur von einer Referenz her bekannt sind.
      Eventuell wuerden beim "True"-Fall wichtige Attribute fehlen. 
      Der StandardCrawler gibt keine Referenzen an, bei denen ein Knoten fehlt, daher ist diese Option zur Sicherheit.*/
      container.setAllowAutoNode(false); 
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

    // In den Workspace einlesen und den Graphen als JavaObjekt holen.
    importController.process(container, new DefaultProcessor(), workspace);
    graph = graphModel.getDirectedGraph();
  }

  /**
   * Lie�t einen zeitlich dynamischen Graphen aus einer Graphen-Datei.
   * 
   * @param pathTofile - Pfad zur Graphen-Datei
   */
  private void createDynamicGraphFromFile(String pathTofile) {
    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
    if (workspace != null) {
      pc.deleteWorkspace(workspace);
    }
    pc.newProject();
    workspace = pc.getCurrentWorkspace();
    dc = Lookup.getDefault().lookup(DynamicController.class);
    dynamicmodel = dc.getModel();
    importController = Lookup.getDefault().lookup(ImportController.class);

    // Importieren die Graphen-Datei
    Container container;
    try {
      System.out.println(pathTofile);
      File file = new File(pathTofile);
      container = importController.importFile(file);
      /* Damit der Graph vollstaendig eingelesen wird, hat es sich bewehhrt zu warten, da kein entsprechender Evendhandler des Gephi-Toolkit gefunden wurde/exsistiert. */
      Thread.sleep(10000);
      container.getLoader().setEdgeDefault(EdgeDefault.DIRECTED);
      container.getLoader().setTimeFormat(TimeFormat.DATE);
      container.setAllowAutoNode(false);
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

    // Importieren die GraphenDaten in den Workspace.
    importController.process(container, new DefaultProcessor(), workspace);

    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
    graph = graphModel.getDirectedGraph();

    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "DynamicGraphMinDate", AnalyseUtil.dateDoubleToFormatedString(dynamicmodel.getMin()));
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyzer.class.getSimpleName(), "DynamicGraphMaxDate", AnalyseUtil.dateDoubleToFormatedString(dynamicmodel.getMax()));
    Interval time = new Interval(dynamicmodel.getMin(), dynamicmodel.getMax());
    dyGraph = dynamicmodel.createDynamicGraph(graph, time);
  }

  /**
   * 
   * @param path - Pfad zur Graphen-Datei
   * @return Aus dem Pfad extrahierter DateiName
   */
  public String getFileName(String path) {
    String[] partedLink = path.split("/");
    int index = partedLink.length - 1;
    String result = "";
    if (partedLink[index].equals("index.html")) {
      index--;
    }
    result = partedLink[index];
    if (result.contains(".")) {
      result = result.substring(0, result.indexOf("."));
    }
    return result;
  }

}

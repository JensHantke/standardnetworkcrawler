package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.gephi.data.attributes.type.TimeInterval;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.ClusteringCoefficient;
import org.gephi.statistics.plugin.Degree;
import org.gephi.statistics.plugin.GraphDistance;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;
/**
 * Zaehlt die fuer veraltete Standard-Dokumente, das Zeitintervall.
 * Dies geht nur fuer veraltete Standard-Dokumente, da diese mit dem Ablaesenden Standard-Dokument, ein Enddatum haben.
 * Daraus ergibt sich die jeweilige Gueltigkeit des Dokumentes und die durchschnittliche Gueltigkeit von mehreren Dokumenten.
 * Erstellt, falls konfiguriert, daraus Korrelationen(fuer Beispielsweise den Reifegrad von W3C).
 * 
 * @author Jens Hantke
 *
 */
public class CountTimeInterval {

  private static final String AVERAGE_ENDING = "_average";
	String removeNodeMethode;
  Degree degreeOnReduceNetwork;
  GraphDistance directedDistance;
  GraphDistance undirectedDistance;
  ClusteringCoefficient clustercoefficient;
  ArrayList<String> colleationGraphColumn;

  public CountTimeInterval(ConfigNode networkCorrelation) {
    if (networkCorrelation != null) {
      Map<String, String> attributes = networkCorrelation.getAttributes();
      colleationGraphColumn = new ArrayList<String>();
      fillAttributes(attributes);
    }
  }

  /**
   * @param filterAttributes
   */
  private void fillAttributes(Map<String, String> attributes) {
    String tempColumnString =  attributes.get("CorrelationGraphColumn");
    if (tempColumnString != null) {
      colleationGraphColumn = new ArrayList<String>(Arrays.asList(tempColumnString.split(" ")));
    }
    ArrayList<String> temp = new ArrayList<String>();
    for(String colum : colleationGraphColumn){
    	temp.add(colum+AVERAGE_ENDING);
    }
    colleationGraphColumn.addAll(temp);
  }
  /**
   * Geht den gesamten Graphen Knotenweise durch und verwendet die veralteten Knoten.
   * Deren  Zeitintervall wird betrachtet und gegebenenfalls eine Korrelation erstellt.
   * Berechnet au�erdem die Anzahl der veralteten Dokumente und die durchschnittliche Gueltigkeit.
   * 
   * @param graph
   */
  public void countTimeInterval(Graph graph) {
    int countDocuments = 0;
    int countDaysBetween = 0;

    // benutzerdefinierte Counter:
    Map<String, Map<String, Integer>> column_columnMap = new HashMap<String, Map<String, Integer>>();
    Map<String, SortedSet<String>> column_possibleTypes = new HashMap<String, SortedSet<String>>();
    for (String colum : colleationGraphColumn) {
      column_columnMap.put(colum, new HashMap<String, Integer>());
      column_possibleTypes.put(colum, new TreeSet<String>());
      if(!colum.endsWith(AVERAGE_ENDING)){
      	column_possibleTypes.put(colum+AVERAGE_ENDING, new TreeSet<String>());
      }
    }
    for (Node node : graph.getNodes().toArray()) {
      TimeInterval interval = (TimeInterval) node.getAttributes().getValue("Time Interval");
      if (interval != null) {
        double source_start = interval.getLow();
        double source_end = interval.getHigh();
        if (source_end != Double.POSITIVE_INFINITY) {
          if (source_end != Double.POSITIVE_INFINITY) {
            // Sammeln benutzerdefinierte Spalten ein:
            int daysBetween = (Integer) node.getAttributes().getValue("durationTillNext");
            for (String colum : colleationGraphColumn) {
              Map<String, Integer> tempMap = column_columnMap.get(colum);
              SortedSet<String> setOfPossitbleTypes = column_possibleTypes.get(colum);
              String columValueSource = node.getAttributes().getValue(colum.replace(AVERAGE_ENDING, "")).toString();
              String mapColumnKey = columValueSource;
              
              if (!setOfPossitbleTypes.contains(columValueSource)) {
                setOfPossitbleTypes.add(columValueSource);
              }
              
              if(colum.endsWith(AVERAGE_ENDING)){
	              if (!tempMap.containsKey(mapColumnKey)) {
	                tempMap.put(mapColumnKey, daysBetween);
	              } else {
	                int counter = tempMap.get(mapColumnKey);
	                counter += daysBetween;
	                tempMap.put(mapColumnKey, counter);
	              }
              } else {
              	if (!tempMap.containsKey(mapColumnKey)) {
	                tempMap.put(mapColumnKey, 1);
	              } else {
	                int counter = tempMap.get(mapColumnKey);
	                counter += 1;
	                tempMap.put(mapColumnKey, counter);
	              }
              }
              
            } 
            //System.out.println(" " + source_end + " || (" + AnalyseUtil.dateDoubleToFormatedString(source_start) + "/" + AnalyseUtil.dateDoubleToFormatedString(source_end) + "durationTillNext: " + node.getAttributes().getValue("durationTillNext"));
            countDaysBetween += daysBetween;
            countDocuments++;
          }
        }
      }
    }
    NetworkResultCollector.setNewValueToCollector(CountTimeInterval.class.getSimpleName(), "NumberOfOldDocuments", countDocuments);
    NetworkResultCollector.setNewValueToCollector(CountTimeInterval.class.getSimpleName(), "AverageDaysOfOldDocuments", (float)countDaysBetween/(float)countDocuments);
    printMatrixOfCustomColumnsCorrelationToCSV(column_columnMap, column_possibleTypes, countDocuments);
  }
  /**
   * Speichert die Korrelationen der Gueltigkeiten von veralteten Standard-Dokumenten.
   * 
   * @param column_columnMap
   * @param column_possibleTypes
   * @param countDocuments
   */
  private void printMatrixOfCustomColumnsCorrelationToCSV(Map<String, Map<String, Integer>> column_columnMap, Map<String, SortedSet<String>> column_possibleTypes, int countDocuments) {
    for (Entry<String, Map<String, Integer>> columnWithFilledMap : column_columnMap.entrySet()) {
      String columnName = columnWithFilledMap.getKey();
      Map<String, Integer> mapOfValues = columnWithFilledMap.getValue();
      SortedSet<String> setOfPossibleTypes = column_possibleTypes.get(columnName);

      String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
      if(!columnName.endsWith(AVERAGE_ENDING)){
      	OutputHelper.writeCsv("Source | Target =>; Values", timeStamp + "NetworkCountTimeInterval_NumberOfOldDocuments_" + columnName + "_" + Webstructure.organisationName);
      } else {
      	OutputHelper.writeCsv("Source | Target =>; Values", timeStamp + "NetworkCountTimeInterval_AverageDaysOfOldDocuments_" + columnName + "_" + Webstructure.organisationName);
      }
      for (String typeI : setOfPossibleTypes) {
        String contatLine = typeI;
        if (mapOfValues.get(typeI) != null) {
        	if(columnName.endsWith(AVERAGE_ENDING)){
        		contatLine += ";" + (float)mapOfValues.get(typeI)/(float)column_columnMap.get(columnName.replace(AVERAGE_ENDING, "")).get(typeI);
        	} else {
        		contatLine += ";" + mapOfValues.get(typeI);
        	}
        	
        } else {
          contatLine += ";0";
        }
        if(!columnName.endsWith(AVERAGE_ENDING)){
        	OutputHelper.writeCsv(contatLine, timeStamp + "NetworkCountTimeInterval_NumberOfOldDocuments_" + columnName + "_" + Webstructure.organisationName);
        } else {
        	OutputHelper.writeCsv(contatLine, timeStamp + "NetworkCountTimeInterval_AverageDaysOfOldDocuments_" + columnName + "_" + Webstructure.organisationName);
        }
      }
    }
  }
}

package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
/**
 * Veraltet die einzelnen NetwzerkAnalysen.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkAnalyse {
	
	private static final Logger networkAnalyse = Logger.getLogger("NETWORK_ANALYSE");

	List<NetworkAnalyzer> allNetworkAnalyzer;
	Map<String,String> graphIDToFilePath = new HashMap<String,String>();
	
	public NetworkAnalyse(ConfigNode parentPageNode, Map<String, String> graphFiles) {
		ConfigNode networkAnalyseNode = Config.getConfigNode("NetworkAnalyse", parentPageNode);
		allNetworkAnalyzer = new ArrayList<NetworkAnalyzer>();
		if(networkAnalyseNode != null){
			if(graphFiles != null){
				this.graphIDToFilePath.putAll(graphFiles);
			}
			List<ConfigNode> NetworkAnalyzerNodes = Config.getConfigNodes("NetworkAnalyzer", networkAnalyseNode);
			for(ConfigNode xmlAttributeElement : NetworkAnalyzerNodes){
				allNetworkAnalyzer.add(new NetworkAnalyzer(xmlAttributeElement));
			}
		}
	}
	
	/**
	 * Startet die Netzwerkanalysen.
	 * 
	 */
	public void executeExtractors(){
		for(NetworkAnalyzer networkAnalyzer : allNetworkAnalyzer){
			List<String> networks = new ArrayList<String>();
			// Vermengt angegebene GraphenIDs und GraphenPfade
			if(networkAnalyzer.targetGraphId != null) {
				for(String graphID : networkAnalyzer.targetGraphId.split(" ")){
					if(graphIDToFilePath.containsKey(graphID)) {
						networks.add("result/graphml/"+graphIDToFilePath.get(graphID));
					}else {
						networkAnalyse.error("GraphID: "+graphID+" konnte nicht gefunden werden.Rat: Ueberpruefen sie die angegebene ID in der konfiguration.Folgende Ids wurden gefunden: "+networks.toString());
					}
				}
			}
			if(networkAnalyzer.pathToTarget != null){
			  networks.addAll(Arrays.asList(networkAnalyzer.pathToTarget.split(" ")));
			}
			for(String pathToFile:networks) {
				networkAnalyzer.executeAnalyse(pathToFile);
				networkAnalyzer.executeDynamicAnalyse(pathToFile);
			}
		}
	}

	
}

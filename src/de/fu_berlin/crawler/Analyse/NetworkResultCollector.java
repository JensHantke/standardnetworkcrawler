package de.fu_berlin.crawler.Analyse;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;

import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;

/**
 * Zum sammeln und rausschreiben von Informationen.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkResultCollector {
  
  static final String XMl_OUTPUT_DIR = "result/";
  static Map<String, Map<String,String>> nameToValueMap = new HashMap<String, Map<String,String>>();
  
  /**
   * Sammelt Informationen ein.
   * 
   * @param topic - In normal Fall der Klassenname
   * @param name - Was fuer ein Wert soll gespeichert werden?
   * @param value - Wert.
   */
  public static void setNewValueToCollector(String topic, String name, Object value){
    if(nameToValueMap.containsKey(topic)){
      Map<String,String> tempList = nameToValueMap.get(topic);
      tempList.put(name, value.toString());
    } else {
      HashMap<String,String> tempMap = new HashMap<String,String>();
      tempMap.put(name, value.toString());
      nameToValueMap.put(topic, tempMap);
    }
  }
  
  /**
   * Schreibt die gesammelten Informationen in eine XML-Datei.
   */
  public static void printResult() {
    try {
      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder;

      docBuilder = docFactory.newDocumentBuilder();

      Document doc = docBuilder.newDocument();
      Element rootElem = doc.createElement("results");
      doc.appendChild(rootElem);
      
      for(Entry<String,Map<String,String>> entry:nameToValueMap.entrySet()){
        Element topicElem = doc.createElement(entry.getKey());
        rootElem.appendChild(topicElem);
        for(Entry<String,String> nameValuePairs:entry.getValue().entrySet()){
          Element nameValueNode = doc.createElement(nameValuePairs.getKey());
          nameValueNode.appendChild(doc.createTextNode(nameValuePairs.getValue()));
          topicElem.appendChild(nameValueNode);
        }
      }
      DOMSource source = new DOMSource(doc);
      writeXML(formatXML(source));
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * 
   * Erstellen lesbare(formatierte) Graphen-Dateien.
   * Kaennen die Dateien, bei Fehlern/Problemen spaeter noch lesen.
   *
   * @param source
   * @return
   */
  private static String formatXML(DOMSource source){
    String resultString = "";
    Transformer transformer;
    try {
      Writer outWriter = new StringWriter();
      StreamResult result = new StreamResult( outWriter );
      transformer = TransformerFactory.newInstance()
          .newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT,"1");
      transformer.transform(source,
          result);
      resultString = outWriter.toString();
    } catch (TransformerConfigurationException e) {
      e.printStackTrace();
    } catch (TransformerFactoryConfigurationError e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }
    return resultString;
  }
  
  /**
   * Schreibt die XML raus.
   * 
   * @param target
   */
  public static void writeXML(String target){
    
    PrintWriter pw;
    try {
      pw = new PrintWriter(XMl_OUTPUT_DIR+CrawlerTimeUtils.getFormatedTime()+"_result_"+Webstructure.organisationName+".xml");
      pw.print(target);
      pw.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    target = null;
  }

}

package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.ClusteringCoefficient;
import org.gephi.statistics.plugin.Degree;
import org.gephi.statistics.plugin.GraphDistance;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;


/**
 * Erstellt Korrelationen auf Grund von angegebenen Spalten, der Knoten.
 * Hier kann beispielsweise die Korrelation zwischen den Reifegraden der Standard-Dokumente erstellt werden.
 * Berechnet die Korrelationsmatrizen und gibt diese als Werte und normierte Matrix aus.
 * Au�erdem wird der OrdnungsKoeffizienten(assortativity coeficient) berechnet und ausgegeben.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkCorrelation {

  String removeNodeMethode;
  Degree degreeOnReduceNetwork;
  GraphDistance directedDistance;
  GraphDistance undirectedDistance;
  ClusteringCoefficient clustercoefficient;
  List<String> colleationGraphColumn;

  public NetworkCorrelation(ConfigNode networkCorrelation) {
    if(networkCorrelation != null){
      Map<String, String> attributes = networkCorrelation.getAttributes();
      colleationGraphColumn = new ArrayList<String>();
      fillAttributes(attributes);
    }
  }

  /**
   * @param filterAttributes
   */
  private void fillAttributes(Map<String, String> attributes) {
    String tempColumnString = attributes.get("CorrelationGraphColumn");
    if (tempColumnString != null) {
      colleationGraphColumn = Arrays.asList(tempColumnString.split(" "));
    }
  }

  /**
   * Berechnet die Korrelationsmatrizen und gibt diese als Werte und normierte Matrix aus.
   * Berechnet aus�erdem den OrdnungsKoeffizienten(assortativity coeficient).
   * 
   * @param graph
   */
  public void calculateCorrelation(Graph graph) {
    if (colleationGraphColumn != null) {
      for (String column : colleationGraphColumn) {
        String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
        Set<String> possibleTypes = new TreeSet<String>();
        // Suchen alle maeglichen Werte der angegebenen Spalten heraus.
        for (Node node : graph.getNodes().toArray()) {
          if (node.getAttributes().getValue(column) != null) {
            String currentColumnEntry = node.getAttributes().getValue(column).toString();
            if (!possibleTypes.contains(currentColumnEntry)) {
              possibleTypes.add(currentColumnEntry);
            }
          }
        }
        Map<String, Integer> mapOfKorrelation = new HashMap<String, Integer>();
        for (String typeI : possibleTypes) {
          for (String typeJ : possibleTypes) {
            mapOfKorrelation.put(typeI + "_" + typeJ, 0);
          }
        }
        for (Edge edge : graph.getEdges().toArray()) {
          if (edge.getSource().getAttributes().getValue(column) != null && edge.getTarget().getAttributes().getValue(column) != null) {
            String sourceNode = edge.getSource().getAttributes().getValue(column).toString();
            String targetNode = edge.getTarget().getAttributes().getValue(column).toString();
            String korrelation = sourceNode + "_" + targetNode;
            if (mapOfKorrelation.containsKey(korrelation)) {
              int count = mapOfKorrelation.get(korrelation);
              count++;
              mapOfKorrelation.put(korrelation, count);
            }
          }
        }
        int sumOfAllKorelations = 0;
        for (String typeI : possibleTypes) {
          for (String typeJ : possibleTypes) {
            sumOfAllKorelations += mapOfKorrelation.get(typeI + "_" + typeJ);
          }
        }

        OutputHelper.writeCsv("Source | Target =>;" + StringUtils.join(possibleTypes, ";"), timeStamp + "NetworkCorrelation_" + column + "_" + Webstructure.organisationName);
        OutputHelper.writeCsv("Source | Target =>;" + StringUtils.join(possibleTypes, ";"), timeStamp + "normalisedNetworkCorrelation_" + column + "_" + Webstructure.organisationName);

        // 
        float[][] normalisedCorrelation = new float[possibleTypes.size()][possibleTypes.size()];
        int i = 0;
        int j = 0;
        for (String typeI : possibleTypes) {
          String contatLine = typeI;
          String contatLineNormalised = typeI;
          for (String typeJ : possibleTypes) {
            contatLine += ";" + mapOfKorrelation.get(typeI + "_" + typeJ);

            float currentNormalisedCorrelation = ((float) mapOfKorrelation.get(typeI + "_" + typeJ) / (float) sumOfAllKorelations);
            contatLineNormalised += ";" + currentNormalisedCorrelation;
            normalisedCorrelation[i][j] = currentNormalisedCorrelation;
            j++;
          }
          j = 0;
          i++;
          OutputHelper.writeCsv(contatLine, timeStamp + "NetworkCorrelation_" + column + "_" + Webstructure.organisationName);
          OutputHelper.writeCsv(contatLineNormalised, timeStamp + "normalisedNetworkCorrelation_" + column + "_" + Webstructure.organisationName);
        }

//        System.out.println(mapOfKorrelation.toString());
//        System.out.println(normalisedCorrelation.toString());
        float[] ai = new float[possibleTypes.size()];
        float[] bi = new float[possibleTypes.size()];
        for (int x = 0; x < normalisedCorrelation.length; x++) {
          for (int y = 0; y < normalisedCorrelation.length; y++) {
            ai[x] += normalisedCorrelation[x][y];
          }
        }
        for (int x = 0; x < normalisedCorrelation.length; x++) {
          for (int y = 0; y < normalisedCorrelation.length; y++) {
            bi[x] += normalisedCorrelation[y][x];
          }
        }
        float summOfeii = 0.0f;
        for (int x = 0; x < normalisedCorrelation.length; x++) {
          summOfeii += normalisedCorrelation[x][x];
        }

        float e_2 = 0.0f;
        for (int x = 0; x < ai.length; x++) {
          e_2 += ai[x] * bi[x];
        }

//        System.out.println(e_2 + " " + summOfeii);
//        System.out.println("" + ((summOfeii - e_2) / (1 - e_2)));
        NetworkResultCollector.setNewValueToCollector(NetworkCorrelation.class.getSimpleName(), "assortativity_coeficient_e_"+column, ((summOfeii - e_2) / (1 - e_2)));

      }
    }
  }
}

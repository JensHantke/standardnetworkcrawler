package de.fu_berlin.crawler.Analyse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;
import de.fu_berlin.crawler.utils.ValueOrderComparator;

/**
 * Analysieren MultiStringAttribute, also Attribute bei denen ein Knoten mehreren Colums zugeordnet sein kann, die unter einem Begriff zusammengefasst werden.
 * Dieses Attribut wird fuer die Firmen(Members) verwendet, da ein Knoten zu mehreren Firmen zugeordnet sein kann.
 * 
 * Anmerkung: Wird sowohl Alleinstehend, als auch im Kontext der Klasse "NetworkTimeDynamic" verwendet.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkAnalyseMultiDataAttribute {
 
  Map<String, Integer> mapOfMemberToCount = new HashMap<String, Integer>();
  ValueOrderComparator bvc =  new ValueOrderComparator(mapOfMemberToCount);
  Map<String, Integer> sortmapOfMemberToCount = new TreeMap<String, Integer>(bvc);
  HashSet<String> documentCount = new HashSet<String>();
  String indexOfDataOnNodes = "";
  HashSet<String> possibleTypes = new HashSet<String>();
  
  
  public NetworkAnalyseMultiDataAttribute(ConfigNode configNode) {
    if(configNode != null){
      Map<String, String> attributes = configNode.getAttributes();
      fillAttributes(attributes);
    }
  }
  public NetworkAnalyseMultiDataAttribute(String indexOfDataOnNodes) {
    this.indexOfDataOnNodes = indexOfDataOnNodes;
  }

  private void fillAttributes(Map<String, String> attributes) {
    indexOfDataOnNodes = attributes.get("indexColumn");
  }
  
  /**
   * Geben die Verteilung von Firma zu Anzahl der Erwaehnungen aus.
   *  - Schreibt die Anzahl der Standard-Dokumente in denen Erwaehnungen gefunden wurden heraus.
   * @param graph
   */
  public void printDataDistribution(Graph graph) {
    String filename =  CrawlerTimeUtils.getFormatedTime() + "_sortedMultiDataAnalyse_" +Webstructure.organisationName;
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyseMultiDataAttribute.class.getSimpleName(), "PathToCSV", filename);
    // Ueberschrift:
    OutputHelper.writeCsv("Name;Value", filename);
    searchPossibleColumns(graph);
    calcDistribution(graph);
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyseMultiDataAttribute.class.getSimpleName(), "NumberOFStandardDocuments", documentCount.size());
    for(Entry<String,Integer> dataEntry: sortmapOfMemberToCount.entrySet()){
      OutputHelper.writeCsv(dataEntry.getKey()+";"+dataEntry.getValue(), filename);
    }
  }
  /**
   * Sucht die maeglichen Firmen, anhand der Indexierungsspalte, da es problematisch ist ueber 
   * die alle Spalten der Knoten zu gehen und herauszufinden, was davon eine Firma ist und was ein KnotenAttribut.
   * 
   * @param graph
   */
  public void searchPossibleColumns(Graph graph) {
    if (indexOfDataOnNodes != null) {
      for (Node node : graph.getNodes().toArray()) {
        Object currentNodeAttribute = node.getAttributes().getValue(indexOfDataOnNodes);
        if (currentNodeAttribute != null) {
          List<String> possibleSubList = Arrays.asList(currentNodeAttribute.toString().split(";"));
          possibleTypes.addAll(possibleSubList);
        }
      }
    }
  }
  /**
   * Berechnet die Verteilung, zaehlt die Erwaehnungen.
   * 
   * @param graph
   */
  public void calcDistribution(Graph graph){
    mapOfMemberToCount = new HashMap<String, Integer>();
    bvc =  new ValueOrderComparator(mapOfMemberToCount);
    for (Node node : graph.getNodes().toArray()) {
      for (String member : possibleTypes) {
        if(node.getAttributes().getValue(member) != null){
          documentCount.add(node.toString());
          if (!mapOfMemberToCount.containsKey(member)) {
            mapOfMemberToCount.put(member, 1);
          } else {
            int count = mapOfMemberToCount.get(member);
            count++;
            mapOfMemberToCount.put(member, count);
          }
        }
      }
    }
    sortmapOfMemberToCount = new TreeMap<String, Integer>(bvc);
    sortmapOfMemberToCount.putAll(mapOfMemberToCount);
    NetworkResultCollector.setNewValueToCollector(NetworkAnalyseMultiDataAttribute.class.getSimpleName(), "NumberOFStandardDocuments", documentCount.size());
  }
  /**
   * Gibt das Ergebnis zurueck.
   * 
   * @return
   */
  public Map<String, Integer> getResultOfDistribution(){
    return sortmapOfMemberToCount;
  } 
  
}

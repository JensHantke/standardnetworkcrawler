package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.gephi.data.attributes.type.TimeInterval;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.AnalyseUtil;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;

/**
 * Zaehlt Referenzen von aktuellen Standard-Dokumenten zu veralteten Standard-Dokumenten => veraltete Referenzen.
 * 
 * @author Jens Hantke
 *
 */
public class CountOldReferences {

  private List<String> colleationGraphColumn;

  public CountOldReferences(ConfigNode networkCorrelation) {
    if(networkCorrelation != null) {
      Map<String, String> attributes = networkCorrelation.getAttributes();
      colleationGraphColumn = new ArrayList<String>();
      fillAttributes(attributes);
    }
  }

  /**
   * Fuellt die Variablen mit den Attributen aus der XML.
   * 
   * @param filterAttributes
   */
  private void fillAttributes(Map<String, String> attributes) {
    String tempColumnString = attributes.get("CorrelationGraphColumn");
    if (tempColumnString != null) {
      colleationGraphColumn = Arrays.asList(tempColumnString.split(" "));
    }
  }
  
  /**
   * Zaehlt die veralteten Referenzen, speichert Metriken und Korrelationen.
   * 
   * @param graph
   */
  public void countOldReferences(Graph graph) {
    int countOldReferences = 0;
    int countActualDocuments = 0;
    // Korrelation der Jahre:
    SortedSet<String> horizontal_year = new TreeSet<String>();
    SortedSet<String> vertical_year = new TreeSet<String>();
    Map<String,Integer> year_yearMap = new HashMap<String,Integer>();

    // benutzerdefinierte Korrelation:
    Map<String,Map<String,Integer>> column_columnMap = new HashMap<String,Map<String,Integer>>();
    Map<String, SortedSet<String>> column_possibleTypes = new HashMap<String,SortedSet<String>>();
    for(String colum : colleationGraphColumn){
      column_columnMap.put(colum, new HashMap<String,Integer>());
      column_possibleTypes.put(colum, new TreeSet<String>());
    }
    
    Set<String> countOldReferencingDocuments = new HashSet<String>();
    Set<String> countOldReferencedDocuments = new HashSet<String>();
    for (Edge edge : graph.getEdges().toArray()) {
      Node source = edge.getSource();
      Node target = edge.getTarget();
      TimeInterval sourceIntervall = (TimeInterval) source.getAttributes().getValue("Time Interval");
      TimeInterval targetIntervall = (TimeInterval) target.getAttributes().getValue("Time Interval");
      if (sourceIntervall != null && targetIntervall != null) {
        double source_start = sourceIntervall.getLow();
        double source_end = sourceIntervall.getHigh();
        if (!source.toString().contains(target.toString().substring(0, 7))) {
          if (source_end == Double.POSITIVE_INFINITY) {
            double target_end = targetIntervall.getHigh();
            if (target_end != Double.POSITIVE_INFINITY) {
              double target_start = targetIntervall.getLow();
              countOldReferencingDocuments.add(source.toString());
              countOldReferencedDocuments.add(target.toString());
              // Sammelt alle benutzerdefinierten Spalten des Graphen fuer die benutzerdefinierte Korrelation ein.
              for (String colum : colleationGraphColumn) {
                Map<String, Integer> tempMap = column_columnMap.get(colum);
                SortedSet<String> setOfPossitbleTypes = column_possibleTypes.get(colum);
                String columValueSource = source.getAttributes().getValue(colum).toString();
                String columValueTarget = target.getAttributes().getValue(colum).toString();
                String mapColumnKey = columValueSource + "_" + columValueTarget;
                if (!setOfPossitbleTypes.contains(columValueSource)) {
                  setOfPossitbleTypes.add(columValueSource);
                }
                if (!setOfPossitbleTypes.contains(columValueTarget)) {
                  setOfPossitbleTypes.add(columValueTarget);
                }
                if (!tempMap.containsKey(mapColumnKey)) {
                  tempMap.put(mapColumnKey, 1);
                } else {
                  int counter = tempMap.get(mapColumnKey);
                  counter++;
                  tempMap.put(mapColumnKey, counter);
                }
              }
              
              // Fuer die Jahres Korrelation
              horizontal_year.add(AnalyseUtil.dateDoubleToYear(source_start));
              vertical_year.add(AnalyseUtil.dateDoubleToYear(target_start));
              String tempMapIndex = AnalyseUtil.dateDoubleToYear(source_start) + "_" + AnalyseUtil.dateDoubleToYear(target_start);
              if (!year_yearMap.containsKey(tempMapIndex)) {
                year_yearMap.put(tempMapIndex, 1);
              } else {
                int counter = year_yearMap.get(tempMapIndex);
                counter++;
                year_yearMap.put(tempMapIndex, counter);
              }
              //System.out.println("alt:" + source + "->" + target + " " + source_end + "->" + target_end + " || (" + dateDoubleToFormatedString(source_start) + "/" + dateDoubleToFormatedString(source_end) + ") ->" + dateDoubleToFormatedString(target_end));
              countOldReferences++;
            }
            countActualDocuments++;
          }
        }
      }
    }
    NetworkResultCollector.setNewValueToCollector(CountOldReferences.class.getSimpleName(), "CountOldReferences", countOldReferences);
    NetworkResultCollector.setNewValueToCollector(CountOldReferences.class.getSimpleName(), "CountActualDocuments", countActualDocuments);
    NetworkResultCollector.setNewValueToCollector(CountOldReferences.class.getSimpleName(), "CountOldReferencingDocuments", countOldReferencingDocuments.size());
    NetworkResultCollector.setNewValueToCollector(CountOldReferences.class.getSimpleName(), "CountOldReferencedDocuments", countOldReferencedDocuments.size());
    printMatrixOfYearCorrelationToCSV(horizontal_year,vertical_year,year_yearMap);
    printMatrixOfCustomColumnsCorrelationToCSV(column_columnMap, column_possibleTypes);
  }
  
  /**
   * Erstellt eine JahresKorrelation und speichert sie in eine CSV.
   * 
   * @param horizontal_year - Horizontale vorkommende Jahre
   * @param vertical_year - Vertikale vorkommende Jahre
   * @param year_yearMap - Map der Referenzen, zwischen den Jahren
   */
  private void printMatrixOfYearCorrelationToCSV(SortedSet<String> horizontal_year,SortedSet<String> vertical_year,Map<String,Integer> year_yearMap){
    String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
    OutputHelper.writeCsv("Source | Target =>;" + StringUtils.join(vertical_year, ";"), timeStamp + "NetworkOldReferenceCorrelation_year_" + Webstructure.organisationName);
    for (String typeI : horizontal_year) {
      String contatLine = typeI;
      for (String typeJ : vertical_year) {
        if(year_yearMap.get(typeI + "_" + typeJ) != null ){
          contatLine += ";" + year_yearMap.get(typeI + "_" + typeJ);
        }else{
          contatLine += ";0";
        }
      }
      OutputHelper.writeCsv(contatLine, timeStamp + "NetworkOldReferenceCorrelation_year_" + Webstructure.organisationName);
    }
  }
  
  /**
   * Erstellt die benutzerdefinierten Korrelationen.
   * 
   * @param column_columnMap Benutzerdefinierte Map
   * @param column_possibleTypes Vorkommenden Typen
   */
  private void printMatrixOfCustomColumnsCorrelationToCSV(Map<String,Map<String,Integer>> column_columnMap, Map<String, SortedSet<String>> column_possibleTypes){
    for(Entry<String,Map<String,Integer>> columnWithFilledMap : column_columnMap.entrySet()){
      String columnName = columnWithFilledMap.getKey();
      Map<String,Integer> mapOfValues = columnWithFilledMap.getValue();
      SortedSet<String> setOfPossibleTypes = column_possibleTypes.get(columnName);
      String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
      OutputHelper.writeCsv("Source | Target =>;" + StringUtils.join(setOfPossibleTypes, ";"), timeStamp + "NetworkOldReferenceCorrelation_"+columnName+"_" + Webstructure.organisationName);
      for (String typeI : setOfPossibleTypes) {
        String contatLine = typeI;
        for (String typeJ : setOfPossibleTypes) {
          if(mapOfValues.get(typeI + "_" + typeJ) != null ){
            contatLine += ";" + mapOfValues.get(typeI + "_" + typeJ);
          }else{
            contatLine += ";0";
          }
        }
        OutputHelper.writeCsv(contatLine, timeStamp + "NetworkOldReferenceCorrelation_"+columnName+"_" + Webstructure.organisationName );
      }
    }
  }

}

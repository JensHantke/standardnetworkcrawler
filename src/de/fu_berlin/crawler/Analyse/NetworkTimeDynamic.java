package de.fu_berlin.crawler.Analyse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.dynamic.api.DynamicGraph;
import org.gephi.dynamic.api.DynamicModel;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.ClusteringCoefficient;
import org.gephi.statistics.plugin.Degree;
import org.gephi.statistics.plugin.GraphDistance;
import org.jsoup.helper.StringUtil;
import org.openide.util.Lookup;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;

/**
 * Stellt die Funktionalitaet fuer die Analyse von zeitlichen dynamischen Graphen
 * zur Verfuegung.
 * 
 * @author Jens Hantke
 * 
 */
public class NetworkTimeDynamic {

  String removeNodeMethode;
  Degree degreeOnReduceNetwork;
  GraphDistance directedDistance;
  GraphDistance undirectedDistance;
  ClusteringCoefficient clustercoefficient;

  // Xml Konfiguration:
  List<String> columns;
  String multipleValueChangeOverTimeColumn = null;
  String outputTimestamp = null;
  List<String> idsOfTraceGraphNode = new ArrayList<String>();
  NetworkAnalyseMultiDataAttribute networkAnalyseMultiDataAttribute;
  Set<String> setOfMultiDataConcat = new HashSet<String>();
  String indexColumnOfMultiData = "";

  final long MILLISECONDS_PER_DAY = 1000L * 60 * 60 * 24;

  public NetworkTimeDynamic(ConfigNode networkTimeDynamic) {
    Map<String, String> attributes = networkTimeDynamic.getAttributes();
    clustercoefficient = new ClusteringCoefficient();
    degreeOnReduceNetwork = new Degree();
    directedDistance = new GraphDistance();
    directedDistance.setDirected(true);
    undirectedDistance = new GraphDistance();
    undirectedDistance.setDirected(false);
    columns = new ArrayList<String>();

    fillAttributes(attributes);

    networkAnalyseMultiDataAttribute = new NetworkAnalyseMultiDataAttribute(indexColumnOfMultiData);
    // Fuer die Verfolgung von einzelnen Standards
    List<ConfigNode> traceGraphConfigNodes = Config.getConfigNodes("TraceGraphNode", networkTimeDynamic);
    for (ConfigNode traceNode : traceGraphConfigNodes) {
      if (traceNode.getAttribute("ID") != null && traceNode.getAttribute("ID") != "") {
        idsOfTraceGraphNode.add(traceNode.getAttribute("ID"));
      }
    }
  }

  /**
   * @param filterAttributes
   */
  private void fillAttributes(Map<String, String> attributes) {
    String tempColumnString = attributes.get("Columns");
    if (tempColumnString != null) {
      columns = Arrays.asList(tempColumnString.split(" "));
    }
    multipleValueChangeOverTimeColumn = attributes.get("NodeCategory");
    indexColumnOfMultiData = attributes.get("indexColumnOfMultiData");
    outputTimestamp = attributes.get("OutputTimestamp");
    if (outputTimestamp == null) {
      outputTimestamp = "dd/MM/yyyy";
    }
  }

  /**
   * Analysiert den zeitlich dynamischen Graphen.
   * 
   * @param graphModel
   * @param dynamicmodel
   * @param attributeModel
   * @param graph
   * @param dyGraph
   */
  public void executeNetworkTimeDynamic(GraphModel graphModel, DynamicModel dynamicmodel, AttributeModel attributeModel, Graph graph, DynamicGraph dyGraph) {
    String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
    String customColumPart = "";
    if (columns.contains("Undirected-AveragePathLength")) {
      customColumPart += ";Undirected-AveragePathLength;Undirected-Diameter";
    }
    if (columns.contains("Directed-AveragePathLength")) {
      customColumPart += ";Directed-AveragePathLength;Directed-Diameter";
    }
    if (columns.contains("AverageDegree")) {
      customColumPart += ";AverageDegree";
    }
    if (columns.contains("AverageClusterCoefficient")) {
      customColumPart += ";AverageClusterCoefficient";
    }

    Map<String, Integer> countStatus = new TreeMap<String, Integer>();
    List<String> orderedListOfStatus = new ArrayList<String>(); // benaetigen wir, damit die Spaltenimmer in der gleichen Reihenfolge abgefragt werden.
    if (multipleValueChangeOverTimeColumn != null) {
      for (Node currentnode : graph.getNodes().toArray()) {
        if (currentnode.getAttributes().getValue(multipleValueChangeOverTimeColumn) != null) {
          String currentColumnEntry = currentnode.getAttributes().getValue(multipleValueChangeOverTimeColumn).toString();
          if (!countStatus.containsKey(currentColumnEntry)) {
            countStatus.put(currentColumnEntry, 0);
            orderedListOfStatus.add(currentColumnEntry);
          }
        }
      }
    }
    String concatStatus = "";
    for (String currentStatus : countStatus.keySet()) {
      concatStatus += ";" + currentStatus;
    }
    String concatTrace = "";
    for (String currentTrace : idsOfTraceGraphNode) {
      concatTrace += ";" + graph.getNode(currentTrace).getAttributes().getValue("Label") + "(" + currentTrace + ")";
    }
    networkAnalyseMultiDataAttribute.searchPossibleColumns(graph);
    networkAnalyseMultiDataAttribute.calcDistribution(graph);
    setOfMultiDataConcat = networkAnalyseMultiDataAttribute.getResultOfDistribution().keySet();

    OutputHelper.writeCsv("CountedNodes;CountedEdges" + customColumPart + ";ID;Time" + concatStatus + ";AvgOutdegree" + concatTrace + ";" + StringUtil.join(setOfMultiDataConcat, ";"), timeStamp + "TimeDynamicGraph");

    List<String> listOfNodes = new ArrayList<String>();
    List<String> listOfNodesTime = new ArrayList<String>();
    // Gehen jeden Tag durch und sammel die jeweiligen Knoten die an diesem Tag
    // publiziert werden, da wir diese Spaeter in umkehrter zeitlicher Reihenfolge ausgeben.
    // Da die zeitlichen Untergraphen, des Gephi-Toolkits im Test Fehlerbehaftet waren(Metriken wurden falsch berechnet)
    for (double i = dynamicmodel.getMin(); i < (dynamicmodel.getMax() + MILLISECONDS_PER_DAY); i += MILLISECONDS_PER_DAY) {
      double low = i;

      double high = i + MILLISECONDS_PER_DAY;
      if (high > dynamicmodel.getMax()) {
        high = dynamicmodel.getMax() - 1;
      }
      if (low > high) {
        break;
      }
      Graph subGraph = dyGraph.getSnapshotGraph(low, high);
      subGraph.readUnlockAll();
      for (Node node : subGraph.getNodes()) {
        if (!listOfNodes.contains(node.toString())) {
          SimpleDateFormat sdf = new SimpleDateFormat(outputTimestamp);
          Date resultdate = new Date((long) low);
          listOfNodesTime.add(sdf.format(resultdate));
          listOfNodes.add(node.toString());
        }
      }
    }

    attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
    degreeOnReduceNetwork = new Degree();
    directedDistance = new GraphDistance();
    directedDistance.setDirected(true);

    calculateAndSaveEntry(graphModel, attributeModel, graph, null, "NOW", countStatus, orderedListOfStatus, timeStamp + "TimeDynamicGraph");
    for (Entry<String, Integer> entry : countStatus.entrySet()) {
      entry.setValue(0);
    }
    // Speichert die Zeilen in die CSV
    for (int i = listOfNodes.size() - 1; i >= 0; i--) {
      Node nodeToRemove = graph.getNode(listOfNodes.get(i));
      graph.removeNode(nodeToRemove);
      calculateAndSaveEntry(graphModel, attributeModel, graph, nodeToRemove, listOfNodesTime.get(i), countStatus, orderedListOfStatus, timeStamp + "TimeDynamicGraph");
      for (Entry<String, Integer> entry : countStatus.entrySet()) {
        entry.setValue(0);
      }
    }
  }

  /**
   * Speichert Zeilen fuer die CSV zur Auswertung.
   * 
   * @param graphModel
   * @param attributeModel
   * @param graph
   * @param node
   * @param time
   * @param countStatus
   * @param orderedListOfStatus
   * @param fileName
   */
  private void calculateAndSaveEntry(GraphModel graphModel, AttributeModel attributeModel, Graph graph, Node node, String time, Map<String, Integer> countStatus, List<String> orderedListOfStatus, String fileName) {

    String customColumPart = "";
    if (columns.contains("Undirected-AveragePathLength")) {
      undirectedDistance.execute(graphModel, attributeModel);
      customColumPart += ";" + undirectedDistance.getPathLength() + ";" + undirectedDistance.getDiameter();
    }
    if (columns.contains("Directed-AveragePathLength")) {
      directedDistance.execute(graphModel, attributeModel);
      customColumPart += ";" + directedDistance.getPathLength() + ";" + directedDistance.getDiameter();
    }
    if (columns.contains("AverageDegree")) {
      degreeOnReduceNetwork.execute(graphModel, attributeModel);
      customColumPart += ";" + degreeOnReduceNetwork.getAverageDegree();
    }
    if (columns.contains("AverageClusterCoefficient")) {
      clustercoefficient.execute(graphModel, attributeModel);
      customColumPart += ";" + clustercoefficient.getAverageClusteringCoefficient();
    }
    String nodeID = "NoNodesRemoved";
    if (node != null) {
      nodeID = node.getNodeData().getId();
    }
    // Zaehlt die Vorkommen fuer die MultiStringAttributes
    String concatStatus = "";
    if (multipleValueChangeOverTimeColumn != null) {
      for (Node currentnode : graph.getNodes().toArray()) {
        if (currentnode.getAttributes().getValue(multipleValueChangeOverTimeColumn) != null) {
          String currentColumnEntry = currentnode.getAttributes().getValue(multipleValueChangeOverTimeColumn).toString();
          if (countStatus.containsKey(currentColumnEntry)) {
            int oldValue = countStatus.get(currentColumnEntry);
            oldValue++;
            countStatus.put(currentColumnEntry, oldValue);
          }
        }
      }
      for (String currentStatus : countStatus.keySet()) {
        concatStatus += ";" + countStatus.get(currentStatus);
      }
    }
    String multiDataConcat = "";

    networkAnalyseMultiDataAttribute.calcDistribution(graph);
    // Fuegt MultiStringAttribute zur CSV hinzu(Beispielsweise Firmen)
    TreeMap<String, Integer> mapOfMultiData = (TreeMap<String, Integer>) networkAnalyseMultiDataAttribute.getResultOfDistribution();
    for (String columMultiData : setOfMultiDataConcat) {
      int count = 0;
      for (Entry<String, Integer> entry : mapOfMultiData.entrySet()) {
        if (entry.getKey().equals(columMultiData)) {
          count = entry.getValue();
          break;
        }
      }
      multiDataConcat += ";" + count;
    }

    String concatTrace = "";
    for (String currentTrace : idsOfTraceGraphNode) {
      // Falls der Knoten noch im Netzwerk vorliegt, geben wir seinen Ingrad
      // aus.
      if (graph.getNode(currentTrace) != null) {
        concatTrace += ";" + graph.getNode(currentTrace).getAttributes().getValue(Degree.INDEGREE);
      } else { // ansonsten geben wir -1 aus, um zu Kennszeichnen das dieser
               // Knoten nicht im Netzwerk vorliegt.
        concatTrace += ";NaN";
      }
    }
    OutputHelper.writeCsv(graph.getNodeCount() + ";" + graph.getEdgeCount() + customColumPart + ";" + nodeID + ";" + time + concatStatus + ";" + computeAvgeraveOutdegree(graphModel, attributeModel, graph) + concatTrace + multiDataConcat, fileName);
  }

  /**
   * Berechnet den Ausgrad fuer das Pricemodell.
   * 
   * @param graphModel
   * @param attributeModel
   * @param graph
   * @return
   */
  public float computeAvgeraveOutdegree(GraphModel graphModel, AttributeModel attributeModel, Graph graph) {
    degreeOnReduceNetwork.execute(graphModel, attributeModel);
    int sumUpOutDegree = 0;
    float result = 0.0f;
    for (Node node : graph.getNodes().toArray()) {
      sumUpOutDegree += (Integer) node.getAttributes().getValue(Degree.OUTDEGREE);
    }
    result = ((float) sumUpOutDegree / (float) graph.getNodeCount());
    return result;
  }
}

package de.fu_berlin.crawler.Analyse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.statistics.plugin.ClusteringCoefficient;
import org.gephi.statistics.plugin.Degree;
import org.gephi.statistics.plugin.GraphDistance;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.OutputHelper;

/**
 * Fuehrt einen Test auf die Roubustheit des Netzwerks aus, dazu gibt es mehrere Varanten des Entfernens der Knoten:
 * Zufaellig, In-, Aus- und nach dem (haechsten ) Grad absteigend.
 * 
 * @author Jens Hantke
 *
 */
public class NetworkResilience {

  String removeNodeMethode;
  Degree degreeOnReduceNetwork;
  GraphDistance directedDistance;
  GraphDistance undirectedDistance;
  ClusteringCoefficient clustercoefficient;
  List<String> columns;

  public NetworkResilience(ConfigNode networkResilience) {
    Map<String, String> attributes = networkResilience.getAttributes();
    clustercoefficient = new ClusteringCoefficient();
    degreeOnReduceNetwork = new Degree();
    directedDistance = new GraphDistance();
    directedDistance.setDirected(true);
    undirectedDistance = new GraphDistance();
    undirectedDistance.setDirected(false);
    columns = new ArrayList<String>();
    fillAttributes(attributes);
  }

  /**
   * @param filterAttributes
   */
  private void fillAttributes(Map<String, String> attributes) {
    removeNodeMethode = attributes.get("RemoveNodeMethode");
    if (!RemoveNodeMethode.Methods.contains(removeNodeMethode)) {
      removeNodeMethode = null;
    }
    String tempColumnString = attributes.get("Columns");
    if (tempColumnString != null) {
      columns = Arrays.asList(tempColumnString.split(" "));
    }
  }

  public void executeNetworkResilienceTest(GraphModel graphModel, AttributeModel attributeModel, Graph graph) {
    String timeStamp = CrawlerTimeUtils.getFormatedTime() + "_";
    String customColumPart = "";
    if (columns.contains("Undirected-AveragePathLength")) {
      customColumPart += ";Undir-"+removeNodeMethode+"-AvgPathL;Undirected-Diameter";
    }
    if (columns.contains("Directed-AveragePathLength")) {
      customColumPart += ";Dir-"+removeNodeMethode+"-AvgPathL;Directed-Diameter";
    }
    if (columns.contains("AverageDegree")) {
      customColumPart += ";AverageDegree-"+removeNodeMethode+"";
    }
    if (columns.contains("AverageClusterCoefficient")) {
      customColumPart += ";AvgClCoefficient-"+removeNodeMethode+"";
    }

    OutputHelper.writeCsv("CountedNodes;CountedEdges;DegreeOfRemovedNode" + customColumPart + ";ID", timeStamp + "NetworkResilience" + removeNodeMethode);
    // fuer den Fall das zufaellig entfern werden soll :
    if (removeNodeMethode.equals(RemoveNodeMethode.RANDOM)) {
      int numberOfNodes = graph.getNodeCount();
      List<Node> nodes = new LinkedList<Node>(Arrays.asList(graph.getNodes().toArray()));
      saveCsvEntry(graphModel, attributeModel, graph, null, timeStamp + "NetworkResilience" + removeNodeMethode);
      for (int i = numberOfNodes; i > 0; i--) {
        int randomNumber = (int) (Math.random() * i);
        Node node = nodes.get(randomNumber);
        graph.removeNode(node);
        nodes.remove(randomNumber);
        saveCsvEntry(graphModel, attributeModel, graph, node, timeStamp + "NetworkResilience" + removeNodeMethode);
      }
    }
    // Fuer die restlichen Faelle:
    saveCsvEntry(graphModel, attributeModel, graph, null, timeStamp + "NetworkResilience" + removeNodeMethode);
    int sizeOfNodes = graph.getNodeCount();
    for (int i = 0; i < sizeOfNodes; i++) {
      Node node = null;

      if (removeNodeMethode.equals(RemoveNodeMethode.OUTDEGREE)) {
        node = getHighestDegreeNode(Degree.OUTDEGREE, graph.getNodes().toArray());
      }
      if (removeNodeMethode.equals(RemoveNodeMethode.INDEGREE)) {
        node = getHighestDegreeNode(Degree.INDEGREE, graph.getNodes().toArray());
      }
      if (removeNodeMethode.equals(RemoveNodeMethode.DEGREE)) {
        node = getHighestDegreeNode(Degree.DEGREE, graph.getNodes().toArray());
      }
      if (node != null) {
        // das eigentliche entfernen des Knotens
        graph.removeNode(node);
        saveCsvEntry(graphModel, attributeModel, graph, node, timeStamp + "NetworkResilience" + removeNodeMethode);
      }
    }
  }
  /**
   * Speichern in die CSV.
   * 
   * @param graphModel
   * @param attributeModel
   * @param graph
   * @param node - Entfernter Knoten, damit Informationen ueber diesen vorliegen.
   * @param fileName - Name der CSV
   */
  private void saveCsvEntry(GraphModel graphModel, AttributeModel attributeModel, Graph graph, Node node, String fileName) {

    String customColumPart = "";
    if (columns.contains("Undirected-AveragePathLength")) {
      undirectedDistance.execute(graphModel, attributeModel);
      customColumPart += ";" + undirectedDistance.getPathLength() + ";" + undirectedDistance.getDiameter();
    }
    if (columns.contains("Directed-AveragePathLength")) {
      directedDistance.execute(graphModel, attributeModel);
      customColumPart += ";" + directedDistance.getPathLength() + ";" + directedDistance.getDiameter();
    }
    if (columns.contains("AverageDegree")) {
      degreeOnReduceNetwork.execute(graphModel, attributeModel);
      customColumPart += ";" + degreeOnReduceNetwork.getAverageDegree();
    }
    if (columns.contains("AverageClusterCoefficient")) {
      clustercoefficient.execute(graphModel, attributeModel);
      customColumPart += ";" + clustercoefficient.getAverageClusteringCoefficient();
    }
    String nodeID = "NoNodesRemoved";
    String nodeDegree = "0";
    if (node != null) {
      nodeID = node.getNodeData().getId();
      nodeDegree = node.getNodeData().getAttributes().getValue(Degree.DEGREE).toString();
    }
    OutputHelper.writeCsv(graph.getNodeCount() + ";" + graph.getEdgeCount() + ";" + nodeDegree + customColumPart + ";" + nodeID, fileName);
  }

  private interface RemoveNodeMethode {
    public String RANDOM = "Random";
    public String INDEGREE = "Indegree";
    public String OUTDEGREE = "Outdegree";
    public String DEGREE = "Degree";
    List<String> Methods = new ArrayList<String>(Arrays.asList(RANDOM, INDEGREE, OUTDEGREE, DEGREE));
  }
  
  /**
   * Gibt den Knoten mit dem haechsten In,Aus oder Grad zurueck.
   * @param parameter - In-, Ausgrad oder Grad
   * @param nodes - alle uebrigen Knoten.
   * @return
   */
  private Node getHighestDegreeNode(String parameter, Node[] nodes) {
    Node result = null;
    int max = -1;
    for (Node node : nodes) {
      int currentDegree = Integer.parseInt(node.getNodeData().getAttributes().getValue(parameter).toString());
      if (currentDegree > max) {
        max = currentDegree;
        result = node;
      }
    }
    return result;
  }

}

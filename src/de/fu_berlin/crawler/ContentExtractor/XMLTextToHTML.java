package de.fu_berlin.crawler.ContentExtractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.utils.DebugHelper;

/**
 * Erzeugt aus XML HTML.
 * 
 * @author Jens Hantke
 *
 */
public class XMLTextToHTML {
	private URL url;
	private String fileName;
	private long countChars;
	private long countWords;
	
	static final Logger log = Logger.getLogger(XMLTextToHTML.class);
	
	public XMLTextToHTML(URL url) {
		this.url = url;
		getFileName();
	}
	/**
	 * Konvertert XmlText zu HTML.
	 * 
	 * @return
	 */
	public String xmlToHTML() {
		String tempString = "";
		StringBuilder sb = getStringBuilderFromURL(url);
		if (sb != null) {
			Pattern attributePattern = Pattern.compile("<!--(.*?)-->", Pattern.DOTALL);
			Matcher matcher = attributePattern.matcher(sb.toString());
			while (matcher.find()) {
				tempString += matcher.group().toString();
			}
			// entfernen XML-Kommentarzeichen
			tempString = tempString.replace("<!--", "");
			tempString = tempString.replace("-->", "");
			tempString = tempString.replaceAll("<(.*?)>", "");

			countWordsAndChars(tempString);
			
			// Wende eine Heuristik an um HTML zu generieren
			String genereatedHTML = "";
			genereatedHTML = tempString.replace("\n", "<br/>");
			genereatedHTML = genereatedHTML.replace("<br/><br/>", "</p><p>");
			genereatedHTML = "<p>" + genereatedHTML + "</p>";

			genereatedHTML = wrapSimpleHtmlTextWithBody(genereatedHTML);
			DebugHelper.printDebugCrawledString("log/xmlHtmlOutput/"+this.fileName + "_comment.html", genereatedHTML);
			sb = null;

			return genereatedHTML;
		} else {
			return "";
		}
	}
	private StringBuilder getStringBuilderFromURL(URL url) {
		InputStreamReader is;
		try {
			is = new InputStreamReader(url.openStream());
			StringBuilder sb = new StringBuilder();
			BufferedReader br = new BufferedReader(is);
			String read = br.readLine();
			while (read != null) {
				sb.append(read);
				sb.append(System.getProperty("line.separator"));
				read = br.readLine();
			}
			br.close();
			is.close();
			return sb;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 /**
   * Packt HTML Tags um das konvertierte HTML.
   * 
   * @param simpleHtml
   * @return
   */
	private String wrapSimpleHtmlTextWithBody(String simpleHtml){
		simpleHtml = "<html><head></head><body>"+simpleHtml+"</body></html>";
		return simpleHtml;
	}
	
	private String getFileName(){
		String fileName = "blank";
		if(url != null){
			if(url.getFile().contains("/")){
				String[] parts = url.getFile().split("/");
				fileName = parts[parts.length-1];
				fileName = fileName.substring(0,fileName.lastIndexOf("."));
				this.fileName = fileName;
			}
		} else {
			// TODO Exception werfen!!
		}
		return fileName;
	}
	
	private void countWordsAndChars(String plainText){
		try {
			countChars = plainText.length();
			countWords = plainText.split("\\s+").length;
		} catch (NullPointerException e) {
			log.error("Waerter konnten nicht gezaehlt werden, da das Dokument nicht richtig initialisiert wurde. ");
		}
	}

	public Long getCountChars() {
		return countChars;
	}

	public Long getCountWords() {
		return countWords;
	}
}

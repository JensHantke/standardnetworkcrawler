package de.fu_berlin.crawler.ContentExtractor;

import java.util.List;

import de.fu_berlin.crawler.datastructure.Standard;

interface DocumentContent {

	public Standard crawlContent(Standard currentStandard);
	
	/**
	 * Gibt die vom ContentExtraktor untersuetzten Formate zurueck.
	 * @return
	 */
	public List<String> getSupportedFormats();
	
	 /**
   * Folgt einem Link und laesst die Extraktoren darueber laufen.
   */
	public Standard crawlFollowPage(Standard currentStandard);
}

package de.fu_berlin.crawler.ContentExtractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.utils.DebugHelper;

/**
 * Wandelt nach einer Heuristik den Text n HTML um.
 * 
 * @author Jens Hantke
 *
 */
public class Text2HTMLConverter {
	
	private URL url;
	private String fileName;
	static final Logger log = Logger.getLogger(Text2HTMLConverter.class);
	private static long countChars;
	private static long countWords;
	
	public Text2HTMLConverter(URL url) {
		this.url = url;
		getFileName();
	}
	
	public String text2HTML() {
		StringBuilder sb =getStringBuilderFromURL(url);
		
		DebugHelper.printDebugCrawledString("log/xmlHtmlOutput/"+this.fileName + "_fromText.html", Text2HTMLConverter.text2HTML(sb.toString()));
		return Text2HTMLConverter.text2HTML(sb.toString());
	}
	
	/**
	 * Erzeugt aus Text HTML und zaehlt Waerter und Buchstaben.
	 * 
	 * @param string
	 * @return
	 */
	public static String text2HTML(String string){
		countWordsAndChars(string);
		String genereatedHTML = plainText2HTML(string);
		return wrapSimpleHtmlTextWithBody(genereatedHTML);
		
	}
	/**
   * Erzeugt aus Text HTML.
   * 
   * @param string
   * @return
   */
	public static String plainText2HTML(String string){
		String genereatedHTML = "";
		genereatedHTML = string.replace("\n", "<br/>");
		genereatedHTML = genereatedHTML.replace("<br/><br/>", "</p><p>");
		genereatedHTML = "<p>" + genereatedHTML + "</p>";
		return genereatedHTML;
		
	}
	/**
	 * Lie�t Text aus einer URL/Datei aus.
	 * 
	 * @param url
	 * @return
	 */
	public static StringBuilder getStringBuilderFromURL(URL url) {
		InputStreamReader is;
		try {
			is = new InputStreamReader(url.openStream());
			StringBuilder sb = new StringBuilder();
			BufferedReader br = new BufferedReader(is);
			String read = br.readLine();
			while (read != null) {
				sb.append(read);
				sb.append(System.getProperty("line.separator"));
				read = br.readLine();
			}
			br.close();
			is.close();
			br = null;
			is = null;
			return sb;
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Packt HTML Tags um das konvertierte HTML.
	 * 
	 * @param simpleHtml
	 * @return
	 */
	private static String wrapSimpleHtmlTextWithBody(String simpleHtml){
		simpleHtml = "<html><head></head><body>"+simpleHtml+"</body></html>";
		return simpleHtml;
	}

	private String getFileName(){
		String fileName = "blank";
		if(url != null){
			if(url.getFile().contains("/")){
				String[] parts = url.getFile().split("/");
				fileName = parts[parts.length-1];
				fileName = fileName.substring(0,fileName.lastIndexOf("."));
				this.fileName = fileName;
			}
		} 
		return fileName;
	}

	
	private static void countWordsAndChars(String plainText){
		try {
			countChars = plainText.length();
			countWords = plainText.split("\\s+").length;
		} catch (NullPointerException e) {
			log.error("Waerter konnten nicht gezaehlt werden, da das Dokument nicht richtig initialisiert wurde. ");
		}
	}
	
	public Long getCountChars() {
		return countChars;
	}

	public Long getCountWords() {
		return countWords;
	}
}

package de.fu_berlin.crawler.ContentExtractor;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.fu_berlin.crawler.Attribute.AttributeExtraktor;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;
/**
 * Verfolgt einen Link und dessen Inhalt zu Crawlen.
 * Beispielsweise um ein Standard-Dokument zu identifizieren, falls nur die URL vorhanden ist.
 * 
 * @author Jens Hantke
 *
 */
public class LinkFollower {
	AttributeExtraktor attributeExtreactor;
	HtmlContent htmlContent;
	PDFContent pdfContent;
	XMLCommentContent xmlContent;
	public List<String> supportedEndings;
	public List<String> notSupportedEndings;
	public String responseCode;
	public String brokenLinkOption;
	
	public LinkFollower(ConfigNode linkFollowerNode) {
		ConfigNode htmlContentNode = Config.getConfigNode("HtmlContent", linkFollowerNode);
		ConfigNode pdfContentNode = Config.getConfigNode("PDFContent", linkFollowerNode);
		ConfigNode xmlContentNode = Config.getConfigNode("XMLCommentContent", linkFollowerNode);
		
		fillLinkFollowerFromXmlAttributes(linkFollowerNode.getAttributes());
		if(htmlContentNode != null) {
			htmlContent = new HtmlContent(htmlContentNode);
		} else if(pdfContentNode != null) {
			pdfContent = new PDFContent(pdfContentNode);
		}else if(xmlContentNode != null) {
			xmlContent = new XMLCommentContent(xmlContentNode);
		} else {
			ConfigNode attributeExtactorNodes = Config.getConfigNode("AttributeExtractor", linkFollowerNode);
			if(attributeExtactorNodes != null) {
				attributeExtreactor = 	new AttributeExtraktor(attributeExtactorNodes);
			}
		}
	}
	
	private void fillLinkFollowerFromXmlAttributes(Map<String, String> filterAttributes) {
		if(filterAttributes.get("SupportedEndings") != null) {
			supportedEndings = Arrays.asList(filterAttributes.get("SupportedEndings").toString().split(" "));
		}
		responseCode= (String) filterAttributes.get("ResponseCode");
		brokenLinkOption = (String) filterAttributes.get("BrokenLinkOption");
		if(filterAttributes.get("NotSupportedEndings") != null) {
			notSupportedEndings = Arrays.asList(filterAttributes.get("NotSupportedEndings").toString().split(" "));
		}
	}
	/**
	 * Verfolgen den Link mit dem jeweiligen ContentExtraktor.
	 * 
	 * @param linkToFollow
	 */
	public void followLink(Standard linkToFollow){
		if (htmlContent != null) {
			htmlContent.crawlFollowPage(linkToFollow);
		} else if (pdfContent != null) {
			pdfContent.crawlFollowPage(linkToFollow);
		} else if (xmlContent != null) {
			xmlContent.crawlFollowPage(linkToFollow);
		} else if (attributeExtreactor != null) {
			JexlExecutor jexlExecuter = new JexlExecutor(linkToFollow);
			attributeExtreactor.executeExtractor(jexlExecuter);
		}
	}
	
	@Override
	public String toString() {
		if(htmlContent != null){
			return "LinkFollower: [HtmlContent : "+htmlContent+" ]";
		} else if(pdfContent != null){
			return "LinkFollower: [PDFContent : "+pdfContent+" ]";
		} else { 
			return "LinkFollower: [AttributeExtraktor : "+attributeExtreactor+" ]";
		}
		
	}
}

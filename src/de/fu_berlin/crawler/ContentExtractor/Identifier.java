package de.fu_berlin.crawler.ContentExtractor;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Attribute.AttributeExtraktor;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Identifiziert ein Standard-Dokument.
 * 
 * @author Jens Hantke
 *
 */
public class Identifier {
	
	static Logger log = Logger.getLogger(Identifier.class);
	static Logger indentifierLog = Logger.getLogger("IDENTIFIER_LOG");
	
	DocumentContentExtractor contentExtractor;
	org.jdom2.Element alternativ;
	AttributeExtraktor attributeExtreactor;
	List<LinkFollower> linkFollowers;
	boolean collectNewStandard = false;

	String source;
	String target;
	String pattern;
	
	List<AttributeExtraktor> allAttributeExtraktors;

		

	public Identifier(ConfigNode identifierNode) {
		contentExtractor = new DocumentContentExtractor(identifierNode);
		fillIdentifierFromXmlAttributes(identifierNode.getAttributes());
		 
		allAttributeExtraktors = new ArrayList<AttributeExtraktor>();
		List<ConfigNode> attributeExtactorNodes = Config.getConfigNodes("AttributeExtractor", identifierNode);
		for(ConfigNode xmlAttributeElement : attributeExtactorNodes){
			allAttributeExtraktors.add(new AttributeExtraktor(xmlAttributeElement));
		}
		
		List<ConfigNode> linkFollowerNodes = Config.getConfigNodes("LinkFollower", identifierNode);
		linkFollowers = new ArrayList<LinkFollower>();
		if(linkFollowerNodes.size()>0) {
			for(ConfigNode linkFollowerNode : linkFollowerNodes ){
				linkFollowers.add(new LinkFollower(linkFollowerNode));
			}
		} else {
			linkFollowers = null;
		}
	}

	/**
	 * @param filterAttributes
	 */
	private void fillIdentifierFromXmlAttributes(Map<String, String> attributes) {
		source = (String) attributes.get("Source");
		target = (String) attributes.get("Target");
		pattern = (String) attributes.get("Pattern");
		if(attributes.get("CollectNewStandard") != null) {
			if(attributes.get("CollectNewStandard").toString().equals("true")){
				collectNewStandard = true;
			}
		}
	}

	static int Counter404 =0;
	
	/**
	 * Identifiziert eine Referenz und speichert die Id in den Standard.
	 * 
	 * @param originStandard
	 * @param rawReference
	 */
	public void identify(Standard originStandard, String rawReference) {
		
		Standard referenceStandard = null;
		if(linkFollowers == null){ // Falls keinem Link gefolgt werden muss werden direkt Attribute Extrahiert die zum Idenfizieren naetig sind => schnelle Variante
			List<HtmlElementWrapper> htmlWrapperList = originStandard.getCurrentHTMLElements();
			for(HtmlElementWrapper htmlWrapper : htmlWrapperList){
				referenceStandard = new Standard(""); 
				JexlExecutor executor = new JexlExecutor(referenceStandard);
				executor.addJexleObject(htmlWrapper);
				executeAttributeExtractions(executor);
				referenceStandard.setJexleName("referencedStandard");
				//log.info(htmlWrapper.getText()+"=>");
				saveSourceToIdentifier(referenceStandard, originStandard);
			}
			htmlWrapperList = null;
		} else { // Muessen einem Link folgen und dise Seite nach Informationen crawlen => langsame Variante
			if (validateURL(rawReference)) {
			  // Ueberpruefen, ob die Url vielleicht schon in einem der Standards gesetzt ist, um vielleicht den Aufwand zu sparenn
				String id = DocumentContentExtractor.urlInStandardList(rawReference);
				if(id != null){
					referenceStandard = new Standard(""); 
					referenceStandard.setJexleName("referencedStandard");
					JexlExecutor executer = new JexlExecutor(referenceStandard);
					setStringToJexlSource(executer, id);
					idToOrignalStandard(referenceStandard, originStandard);
					referenceStandard = null;
					executer= null;
					return;
				}
			}
			
			referenceStandard = new Standard(rawReference);
			if (validateURL(rawReference)) { // falls die URL valide ist, kein 404 ...
				log.info(targetURL+" - "+rawReference);
				executeLinkFollower(referenceStandard, rawReference); // Link Verfolgen
				saveSourceToIdentifier(referenceStandard, originStandard); // Speichern
			} else {
				tryToFixBrokenLinkReferences(referenceStandard);
				Counter404++;
				log.error("Folgende URL ist nicht valide und wird daher uebersprungen: '" + rawReference + "' - " + Counter404);
			}
			if(collectNewStandard){ // Eventuell in die Liste der zu Crawlenden Dokumente aufnehmen
				if (referenceStandard.getID() != null) {
					if(!DocumentContentExtractor.standardAllreadyInList(referenceStandard)){
						referenceStandard.setJexleName("currentStandard");
						DocumentContentExtractor.addStandardToList(referenceStandard);
					}
				}
			}
		}
		referenceStandard = null;
		
	}
	


	private String getClonedServerLink(String rawReference){
		if (!rawReference.contains("localhost")) {
			// http://www.dmtf.org/standards/published_documents/DSP0215_1.0.pdf
			// http://localhost/DMTFNeu/www.dmtf.org/sites/default/files/standards/documents/DSP1036_1.0.0.pdf
			if(rawReference.endsWith(".pdf")) {
				rawReference = rawReference.replace("www.dmtf.org", "localhost/DMTFNeu/www.dmtf.org");
				rawReference = rawReference.replace("http://schemas.dmtf.org", "http://localhost/DMTFNeu/schemas.dmtf.org");
				log.info("Reference ausgetauscht");
			}
		}
		return rawReference;
	}
	
	private void executeAttributeExtractions(JexlExecutor executor) {
		for (AttributeExtraktor attributeExtraktor : allAttributeExtraktors) {
			attributeExtraktor.executeExtractor(executor);
		}
		executor = null;
	}
	
	private void executeLinkFollower(Standard referenceStandard,String rawReference){
		JexlExecutor executor = new JexlExecutor(referenceStandard);
		
		for (LinkFollower linkFollower : linkFollowers) {
			if (targetURL != null) {
				referenceStandard.setLinkToStandard(targetURL.toString());
			} else {
				referenceStandard.setLinkToStandard(rawReference);
			}
			referenceStandard.setLinkToStandard( getClonedServerLink(referenceStandard.getLinkToStandard()) );
			// Fall: Es wird eine Endung unterstuetzt. Alle nicht unterstuetzen Endungen muessen uebersprungen werden.
			if(linkFollower.supportedEndings == null) {
				if(linkFollower.notSupportedEndings != null ){
					boolean isNotSupported = false;
					for(String ending : linkFollower.notSupportedEndings) {
						if(referenceStandard.getLinkToStandard().endsWith(ending)) {
							isNotSupported = true;
						}
					}
					if(isNotSupported) {
					 continue;
					}
				}
				referenceStandard.setJexleName("currentStandard");
				linkFollower.followLink(referenceStandard);
			} else {
				boolean hasSupportedEnding = false;
				for(String ending : linkFollower.supportedEndings){
					if( referenceStandard.getLinkToStandard().endsWith(ending) ) {
						hasSupportedEnding = true;
					}
				}
				if(hasSupportedEnding){
					referenceStandard.setJexleName("currentStandard");
					linkFollower.followLink(referenceStandard);
				}
			}
			
			/* Ueberpruefen ob der Link verfolger das Feld mit dem wir weiter Arbeiten
			 * wollen korrekt gefuellt hat, in diesen Fall wolle nicht keinen weiteren Link Foller Methoden ausprobieren. */
			referenceStandard.setJexleName("referencedStandard");
			executor = new JexlExecutor(referenceStandard);
			if (executor.execute(source) != null) {
				log.info("Source: "+executor.execute(source));
				break;
			}
		}

		executor = null;
	}
	List<String> onlyWorkOnOnce = new ArrayList<String>();
	/**
	 * Speichert die ID zu den Referenzen des Standard(falls die der gefundene Text auf das Pattern passt).
	 * 
	 * @param referenceStandard
	 * @param originStandard
	 */
	private void saveSourceToIdentifier(Standard referenceStandard, Standard originStandard){
		JexlExecutor innerExecuter = new JexlExecutor(referenceStandard);
		innerExecuter.addJexleObject(originStandard);
		Object referenceSource = innerExecuter.execute(source);
		
		if (referenceSource != null) { // Falls es die ID schon fuer den Standard gefunden wurde, lassen wir ihn die Arbeit nicht nochmal tun.
			if(!onlyWorkOnOnce.contains(referenceSource)){
				onlyWorkOnOnce.add(referenceSource.toString());
			}
			if (pattern != null) {
				Pattern identifierPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
				Matcher matcher = identifierPattern.matcher(innerExecuter.execute(source).toString());
				if (innerExecuter.execute(source).toString().trim().equals("")) {
					innerExecuter = null;
					referenceStandard = null;
					return;
				}
				if (matcher.find()) {
				  // speichern mittels Jexl Ausdruck aus der Konfiguration:
					if (source.contains("getAttribute")) {
						String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
						innerExecuter.execute("referencedStandard.setAttribute('" + sourceAttribute + "','" + matcher.group().toString() + "')");
					} else {
						innerExecuter.execute(source + " = \"" + matcher.group().toString() + "\"");
					}
				} else {
					return; // Kein Treffer.
				}
				
				innerExecuter.execute(target);
				if(getResultFromTargetExpression(innerExecuter).equals("null")) {
					System.out.println(getResultFromTargetExpression(innerExecuter));
				}
				String id = getResultFromTargetExpression(innerExecuter);
				if(id != null && id != ""){
					referenceStandard.setID(getResultFromTargetExpression(innerExecuter));
				}
				indentifierLog.info("Referenz erfolgreich identifiziert:"+innerExecuter.execute(source));
				// Aufraeumen:
				innerExecuter = null;
			}
		}
	}
	
	/**
	 * Identifizieren 
	 * 
	 * @param referenceStandard
	 * @param originStandard
	 */
	private void idToOrignalStandard(Standard referenceStandard,  Standard originStandard) {
		JexlExecutor innerExecuter = new JexlExecutor(referenceStandard);
		innerExecuter.addJexleObject(originStandard);
//		Object referenceSource = innerExecuter.execute(source);
		innerExecuter.execute(target);
//		if(getResultFromTargetExpression(innerExecuter).equals("null")) {
//			System.out.println(getResultFromTargetExpression(innerExecuter));
//		}
		String id = getResultFromTargetExpression(innerExecuter);
		if(id != null && id != ""){
			referenceStandard.setID(getResultFromTargetExpression(innerExecuter));
		}
		
		indentifierLog.info("idToOrignalStandard: Referenz erfolgreich identifiziert:"+innerExecuter.execute(source));
		// Aufraeumen:
		innerExecuter = null;
	}
	
	/**
	 * Holt das derzeitig gesetzte Ergebnis aus dem JexlAusdruck heraus.
	 * 
	 * @param executor
	 * @return
	 */
	private String getResultFromTargetExpression(JexlExecutor executor){
		if (source.contains("setAttribute")) {
			String sourceAttribute = target.substring(target.indexOf("'", 0) + 1, target.indexOf("'", target.indexOf("'", 0) + 1));
			return executor.execute("currentStandard.getAttribute('" + sourceAttribute + "')").toString();
		} else if(source.contains("=")) {
			String strTarget = target.substring(0, target.indexOf("="));
			return executor.execute(strTarget).toString();
		}else {
			return executor.execute(target).toString();
		}
	}
	
	/**
	 * Option TryToFixBrokenLinks eraeffnet die Maeglichkeit Links die beschaedigt sind, insofern zu reparieren, das geschaut wird, ob eine ID Extrahierbar ist.
	 * In dem Fall wird nach einem Titel gesucht und die Versionen werden alle Referenziert.
	 * 
	 * @param referenceStandard
	 */
	private void tryToFixBrokenLinkReferences(Standard referenceStandard){
		for(LinkFollower linkFollower : linkFollowers){
			if(linkFollower.brokenLinkOption != null) {
				if(linkFollower.brokenLinkOption.equals("MatchAllIfStartsWith")) {
					linkFollower.followLink(referenceStandard);
					referenceStandard.setJexleName("referencedStandard");
					JexlExecutor innerExecuter = new JexlExecutor(referenceStandard);
					if(innerExecuter.execute(source) != null){
						String attribteresult = innerExecuter.execute(source).toString();
						boolean wasfound = false;
						for(Map.Entry<String, Standard> entry:Webstructure.linkToStandardMap.entrySet()){
							if(entry.getValue().getID().startsWith(attribteresult)){
								if (source.contains("getAttribute")) {
									String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
									innerExecuter.execute("referencedStandard.setAttribute('" + sourceAttribute + "','" + entry.getValue().getID() + "')");
								} else {
									innerExecuter.execute(source + " = \"" + entry.getValue().getID() + "\"");
								}
								innerExecuter.execute(target);
								wasfound = true;
								log.info("Referenz erfolgreich aus BrokenLink identifiziert:"+entry.getValue().getID());
							}
						}
						if(!wasfound) {
							log.info("Wurde nicht gefunden"+attribteresult);
						}
					}
				}
			}
		}
	}
	
	static URL targetURL;
	
	/**
	 * Untersucht ob die URL Valide ist, also einen 200 zurueck liefert und abgerufen werden kann.
	 * 
	 * @param strUrl
	 * @return
	 */
	public static boolean validateURL(String strUrl) {
		try {
			targetURL = null;
	    URL url = new URL(strUrl);
	    URLConnection conn = url.openConnection();
	    HttpURLConnection huc =  (HttpURLConnection)  conn; 
	    huc.setRequestMethod("GET"); 
	    huc.connect(); 
	    if(huc.getResponseCode() == 200){
	    	targetURL = huc.getURL();
	    	huc.disconnect();
	    	huc = null;
	    	return true;
	    } else {
	    	huc.disconnect();
	    	huc = null;
	    	return false;
	    }
		} catch (MalformedURLException e) {
		  log.error("Die URL ist nicht Vailde:"+strUrl);
			return false;
		} catch (IOException e) {
		  log.error("Konnte keine Verbindung zur folgenden URL aufbauen:"+strUrl);
			return false;
		} catch (NullPointerException e) {
		  log.error("Konnte keine Verbindung zur folgenden URL aufbauen:"+strUrl);
			return false;
		} catch (Exception e) {
      return false;
   }
	}
	/**
	 * Speichert einen String in die Source des JexlContexts.
	 * Dient dazu die Referenz zu einer ID werden zu lassen.
	 * 
	 * @param executer
	 * @param id
	 * @return
	 */
	private String setStringToJexlSource(JexlExecutor executer,String id) {
		String tempSource;
		if (source.contains("getAttribute")) {
			String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
			tempSource = (String) executer.execute("referencedStandard.getAttribute('" + sourceAttribute + "')");
			executer.execute("referencedStandard.setAttribute('" + sourceAttribute + "','" + id + "')");
		} else {
			tempSource = (String) executer.execute(source);
			executer.execute(source + " = \"" + id + "\"");
		}
		return tempSource;
	}
	
}

package de.fu_berlin.crawler.ContentExtractor;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.jdom2.Element;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.Configuration.SaxXmlParser;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.DebugHelper;

/**
 * Veraltet die DokumentExtraktoren und sorgt dafuer das die Liste der Standard-Dokumente abgearbeitet wird.
 * 
 * @author Jens Hantke
 *
 */
public class DocumentContentExtractor {
	
	private static final String PDF_CONTENT = "PDFContent";

	private static Logger log = Logger.getLogger(DocumentContentExtractor.class);
	private static Logger documentExtraktionProcess = Logger.getLogger("DOCUMENT_EXTRAKTION_PROCESS");
	private static final Logger collectedStandardsLogger = Logger.getLogger("COLLECTED_STANDARD_LOGGER");
	
	
	
	ConfigNode documentContentNode;
	List<DocumentContent> differendDocumentContent;
	
	public DocumentContentExtractor(ConfigNode parentPageNode) {
		standardListe = new ArrayList<Standard>();
		documentContentNode = Config.getConfigNode("ContentExtractor", parentPageNode);
		
		differendDocumentContent = new ArrayList<DocumentContent>();
		
		List<ConfigNode> differendContentExtractors = new ArrayList<ConfigNode>();
		if(documentContentNode != null) { // Fuer den Fall das es keinen ContentExtractor gibt.
			differendContentExtractors = documentContentNode.getChildren();
		} 
		for(ConfigNode contentExtractorNode : differendContentExtractors){
			if(contentExtractorNode.getName().equals(PDF_CONTENT)){
				differendDocumentContent.add(new PDFContent(contentExtractorNode));
				continue;
			}
			if(contentExtractorNode.getName().equals("XMLCommentContent")){
				differendDocumentContent.add(new XMLCommentContent(contentExtractorNode));
				continue;
			}
			if(contentExtractorNode.getName().equals("TextContent")){
				differendDocumentContent.add(new TextContent(contentExtractorNode));
			}
			if(contentExtractorNode.getName().equals("HtmlContent")){
				differendDocumentContent.add(new HtmlContent(contentExtractorNode));
			}
		}
	}
	
	/**
	 * Arbeitet die Liste der Standard-Dokumente ab in dem die jeweiligen DocumentExtraktoren zugeteilt und gestartet werden.
	 * 
	 * @param inputStandardListe
	 */
	public void crawlDocuemtens(List<Standard> inputStandardListe){
 		standardListe = inputStandardListe;
		int count =0;
		for(int i =0; i<standardListe.size();i++) {
			Standard currenctStandard = standardListe.get(i);
			count ++;
			if(standardListe.size()>1){
				documentExtraktionProcess.info("("+count+"/"+standardListe.size()+")");
			}
			for(DocumentContent content : differendDocumentContent) {
					for(String supportedFormat : content.getSupportedFormats()) {
						if(currenctStandard.getLinkToStandard() != null ) {
							if(getFileName(currenctStandard).endsWith(supportedFormat)) {
								currenctStandard = content.crawlContent(currenctStandard);
								collectedStandardsLogger.info(currenctStandard);
							}
						} else {
							log.error("Folgender Standard hat keine URL. Um das Dokument zu crawlen benaetigt die Anwendung einen Link zum Standard." +
									" Rat: Gehen Sie sicher das das Attribut linkToStandard richtig gesetzt wurde. Standard:"+currenctStandard);
						}
					}
				
			}
			// Trick wenn man nur 30 oder X Standard-Dokumente Crawlen maechte :-)
//			if(count>10){
//				return;
//			}
		}
	}
	
	public static List<Standard> standardListe;
	
	/**
	 * Fuegt einen Standard zur Liste der abzuarbeitenden Standard-Dokumente hinzu.
	 * 
	 * @param standard
	 */
	public static void addStandardToList(Standard standard){
		standardListe.add(standard);
	}
	
	/**
	 * Ueberprueft ob der Standard, bereits in die Liste der Standards aufgenommen wurde(dabei ist egal ob er bereits abgearbeitet wurde).
	 * 
	 * @param standard
	 * @return
	 */
	public static boolean standardAllreadyInList(Standard standard){
		for(Standard traverseStandard : standardListe){
			if(traverseStandard.getID().equals(standard.getID())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Ueberprueft ob es bereits einen Standard, in der Liste der Standards, mit der angegebenen URL, gibt.
	 * 
	 * @param rawReferenceUrl - URL
	 * @return String ID des Standard, mit der mit der URL uebereinstimmt, sonst null
	 */
	public static String urlInStandardList(String rawReferenceUrl){
		String localDuplicateServerPrefix = Webstructure.configNode.getAttribute("LocalDuplicateServerPrefix");
		
		for(Standard traverseStandard : standardListe){
			String comparativeURL = traverseStandard.getLinkToStandard();
			String comparativeURLWithoutLocalPart = traverseStandard.getLinkToStandard();
			if(localDuplicateServerPrefix != null) {
				comparativeURLWithoutLocalPart = comparativeURL.replace(localDuplicateServerPrefix, "");
			}
			if(comparativeURL.equals(rawReferenceUrl) || comparativeURLWithoutLocalPart.equals(rawReferenceUrl)){
				return traverseStandard.getID();
			}
		}
		return null;
	}
	
  private String getFileName(Standard currenctStandard) {
    String linkOfStandard = currenctStandard.getLinkToStandard();
    String[] partedLinkOfStandard = linkOfStandard.split("/");
    if(!partedLinkOfStandard[partedLinkOfStandard.length-1].contains(".")) { 
      return currenctStandard.getLinkToStandard()+"/index.html";
    }
    return currenctStandard.getLinkToStandard();
  }
}

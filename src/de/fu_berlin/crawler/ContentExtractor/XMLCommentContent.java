package de.fu_berlin.crawler.ContentExtractor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;

/**
 * Extrahiert Attribute und Referenzen aus den uebergebenen Standard-Dokument.
 * 
 * @author Jens Hantke
 *
 */
public class XMLCommentContent extends ContentExtraktor implements DocumentContent  {
	
	Logger log = Logger.getLogger(XMLCommentContent.class);
	
	

	public XMLCommentContent(ConfigNode configNode) {
		super(configNode);
	}

  /**
  * Liesst die XML-Datei zur weiteren Verarbeitung ein.
  * Zaehlt au�erdem die Buchstaben und Waerter.
  * 
  */
	@Override
	public Standard crawlContent(Standard currentStandard) {
		try {
			url = new URL(currentStandard.getLinkToStandard());
			log.info("FileName: " + url.getFile());
			XMLTextToHTML xmlTextToHTML = new XMLTextToHTML(url);
			String comments = xmlTextToHTML.xmlToHTML();
			Document doc = Jsoup.parse(comments);
			currentStandard.setHtmlElement(doc);
			
			currentStandard.setCountedCharacter(xmlTextToHTML.getCountChars());
			currentStandard.setCountedWords(xmlTextToHTML.getCountWords());
			
			executeExtractors(currentStandard);
			executeReferenceCollectors(currentStandard);
			// Aufraeumen:
			xmlTextToHTML = null;
			comments = null;
			currentStandard.setHtmlElement(null);
			doc = null;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return currentStandard;
	}
	
	@Override
	public Standard crawlFollowPage(Standard currentStandard) {
		URL url;
		try {
			url = new URL(currentStandard.getLinkToStandard());
			XMLTextToHTML xmlTextToHTML = new XMLTextToHTML(url);
			String comments = xmlTextToHTML.xmlToHTML();
			
			Document doc = Jsoup.parse(comments);
			
			currentStandard.setHtmlElement(doc);
			executeExtractors(currentStandard);
			
			// aufraeumen
			xmlTextToHTML = null;
			comments = null;
			doc = null;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		currentStandard.setHtmlElement(null);
		return currentStandard;
	}

	@Override
	public List<String> getSupportedFormats() {
		return supportedFormats;
	}

}

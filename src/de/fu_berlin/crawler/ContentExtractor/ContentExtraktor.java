package de.fu_berlin.crawler.ContentExtractor;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Attribute.AttributeExtraktor;
import de.fu_berlin.crawler.Attribute.ReferenceCollector;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Beinhaltet die allgemeinen Funktionalitaeten der ContentExtraktoren.
 * 
 * @author Jens Hantke
 *
 */
public class ContentExtraktor {
  List<String> supportedFormats;
  List<AttributeExtraktor> allAttributeExtraktors;
  URL url;
  protected ReferenceCollector referenceCollector;
  
  public ContentExtraktor(ConfigNode configNode){
    Logger log = Logger.getLogger(ContentExtraktor.class);
    String supportedEndings = configNode.getAttribute("SupportedEndings");
    if(supportedEndings != null) {
      supportedFormats = Arrays.asList( configNode.getAttribute("SupportedEndings").split(" ") );
    }
    log.info(supportedFormats);
    
    allAttributeExtraktors = new ArrayList<AttributeExtraktor>();
    List<ConfigNode> attributeExtactorNodes = Config.getConfigNodes(configNode, "AttributeExtractor",2);
    Iterator<ConfigNode> iter = attributeExtactorNodes.iterator();
    // Suchen alternativen zusammen
    while(iter.hasNext()){
      ConfigNode xmlAttributeElement = iter.next();
      ConfigNode parentelement = xmlAttributeElement.getParent();
      if(xmlAttributeElement.hasAlternativ()){
        AttributeExtraktor attributeExtraktor = new AttributeExtraktor(xmlAttributeElement);
        for(ConfigNode child : parentelement.getChildren()){
          if(xmlAttributeElement.getXmlNode() != child.getXmlNode()){
            attributeExtraktor.addAlternative( new AttributeExtraktor(child) );
            iter.next();
          }
        }
        allAttributeExtraktors.add(attributeExtraktor);
      }else{
        allAttributeExtraktors.add(new AttributeExtraktor(xmlAttributeElement));
      }
    }
    ConfigNode referenceCollectorNode = Config.getConfigNode("ReferenceCollector", configNode);
    if(referenceCollectorNode != null) {
      referenceCollector = new ReferenceCollector(referenceCollectorNode);
    } else {
      referenceCollector = null;
    }
  }
  /**
   * Startet die Attribut Extraktoren
   * 
   * @param executer
   */
  public void executeExtractors(JexlExecutor executer){
    for(AttributeExtraktor attributeExtraktor : allAttributeExtraktors){
      attributeExtraktor.executeExtractor(executer);
    }
  }
  
  /**
   * Fuegt JexleContext(HtmlElemente, Standard) hinzu. 
   * @param currentStandard
   */
  protected void executeExtractors(Standard currentStandard){
    HtmlElementWrapper htmlElement = new HtmlElementWrapper(currentStandard.htmlElement);
    JexlExecutor executer = new JexlExecutor(currentStandard);
    executer.addJexleObject(htmlElement);
    executeExtractors(executer);
  }
  
  /**
   * Fuegt JexleContext(HtmlElemente, Standard) fuer die ReferenzExtraktoren hinzu.
   * @param currentStandard
   */
  protected void executeReferenceCollectors(Standard currentStandard){
    HtmlElementWrapper htmlElement = new HtmlElementWrapper(currentStandard.getHtmlElement() );
    JexlExecutor executer = new JexlExecutor(htmlElement);
    executer.addJexleObject(currentStandard);
    executeReferenceCollectors(executer);
  }
  /**
   * Startet die ReferenzExtraktoren.
   * @param executer
   */
  public void executeReferenceCollectors(JexlExecutor executer){
    referenceCollector.executeCollector(executer);
  }
}

package de.fu_berlin.crawler.ContentExtractor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.DebugHelper;
/**
 * Extrahiert Attribute und Referenzen aus den uebergebenen Standard-Dokument.
 * 
 * @author Jens Hantke
 *
 */
public class HtmlContent extends ContentExtraktor implements DocumentContent {
	
	private final String DEBUG_DIRECTORY ="log"+System.getProperty("file.separator")+"HtmlContent"; 
	
	String fileName;
	boolean loadInternalPages = false;
	
	Logger log = Logger.getLogger(HtmlContent.class);
	Logger loadInternalLog = Logger.getLogger("LOADINTERNALPAGES_LOGGER");
	
 	public HtmlContent(ConfigNode configNode) {
		super(configNode);
	}

  /**
   * Liesst die HTML zur weiteren Verarbeitung ein.
   * Zaehlt au�erdem die Buchstaben und Waerter.
   * 
   */
	@Override
	public Standard crawlContent(Standard currentStandard) {
		try {
			log.info(currentStandard.getLinkToStandard());
			url = new URL(currentStandard.getLinkToStandard());
			log.info(url);
			StringBuilder stringBuilder = Text2HTMLConverter.getStringBuilderFromURL(url);
			if(stringBuilder!= null) {
				Document doc = Jsoup.parse(stringBuilder.toString().replaceAll("&nbsp;", " "));
				
				if(loadInternalPages) {
					loadInternalPage(doc);
				}
				addCommentToHTML(doc);
				
				String plainText = doc.text();
				// TODO: Count Words/Chars immer oder nach Attribute an HTMLContent?
				currentStandard.setCountedCharacter((long) plainText.length());
				currentStandard.setCountedWords((long) plainText.split("\\s+").length);
				plainText = null;
				
				currentStandard.setHtmlElement(doc);
				executeExtractors(currentStandard);
				executeReferenceCollectors(currentStandard);
				DebugHelper.printDebugCrawledDocument(getFileName(currentStandard), doc);
				
				url = null;
				doc = null;
				stringBuilder = null;
				currentStandard.setHtmlElement(null);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return currentStandard;
	}
	
	/**
	 * Es gibt Standards die als HTML-Dokument vorliegen, die sich nicht nur auf ein Dokument beschraenken. 
	 * Diese Funktion sucht nach HTML-Dokument internen Links und fuegt deren Inhalt, dem Body hinzu.
	 * 
	 * @param doc
	 * @throws IOException
	 */
	public void loadInternalPage(Document doc) throws IOException {
		// gehen davon aus das wir im Orginal(index) Document nur einen Body vorfinden.
		org.jsoup.nodes.Element orginalContentPageBody = doc.select("body").get(0);
		doc.setBaseUri(url.toString());
		boolean hasSingleConentSite = false;
		for(org.jsoup.nodes.Element link : doc.select("a[href]")) {
			String href = link.attr("href");
			if(href.contains("complete.html") && !href.contains("http://") && !href.contains("#")  ){
				loadInternalLog.info(href+" # "+link.attr("abs:href"));
				doc = Jsoup.connect(link.attr("abs:href")).get();
				hasSingleConentSite = true;
			}
		}
		if(!hasSingleConentSite){
			Elements toc = doc.select("div.toc");
			org.jsoup.nodes.Element scopeElement;
			if(toc.first() == null){ // suchen im Body
				scopeElement = orginalContentPageBody;
			}else { // suchen in Verzeichnis Div
				scopeElement = toc.first();
			}
			for(org.jsoup.nodes.Element link : scopeElement.select("a[href]")) {
				String href = link.attr("href");
				if(!href.contains("http") && !href.contains("mailto") && !href.contains("..") && href.contains(".html") && !href.contains("index") && !href.contains("TOC") && !href.contains("ftp:") && !href.contains("#") && !href.contains("diff")){
					System.out.println(url+" => "+ link.attr("abs:href") +" - "+link.attr("href") );
					loadInternalLog.info(url+" => "+ link.attr("abs:href") +" - "+link.attr("href"));
					orginalContentPageBody .appendChild(Jsoup.connect(link.attr("abs:href")).get());
				}
			}
		}
		
	}
	
	/**
	 * In manchen Standards(z.B. DMTF) kommt es vor, dass wichtige Informationen(z.B. Referencen, Titel, Datum) im Kommentar des HTML Dokument stehen.
	 * Diese Funktion haengt den Kommentar als div, an den Body des HTML-Dokumentes.
	 * 
	 * @param doc
	 */
	public void addCommentToHTML(Document doc) {
		for (int i = 0; i < doc.childNodes().size();i++) {
      Node child = doc.childNode(i);
      if (child.nodeName().equals("#comment")){
      	doc.select("body").get(0).append("<div>"+Text2HTMLConverter.plainText2HTML(child.toString().replace("<!--", "").replace("-->", ""))+"</div>");
      }
		}
	}
	
	@Override
	public Standard crawlFollowPage(Standard currentStandard){
		Document doc;
		try {
			doc = Jsoup.connect(currentStandard.getLinkToStandard()).get();
			currentStandard.setHtmlElement(doc);
			executeExtractors(currentStandard);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc = null;
		currentStandard.setHtmlElement(null);
		return currentStandard;
	}

	public List<String> getSupportedFormats() {
		return supportedFormats;
	}
	
	private String getFileName(Standard currentStandard) {
		String fileName = "blank";
		String id = null;
		if(( id = currentStandard.getID() )!= null) {
			fileName = id+".html";
		}else if(url != null){
			if(url.getFile().contains("/")){
				String[] parts = url.getFile().split("/");
				fileName = parts[parts.length-1];
				if(fileName.contains(".")) {
					fileName = fileName.substring(0,fileName.lastIndexOf("."));
					fileName += ".html";
				} else {
					fileName += ".html";
				}
			}
			
		}
		fileName = DEBUG_DIRECTORY+System.getProperty("file.separator")+fileName;
		return fileName;
	}
}

package de.fu_berlin.crawler.ContentExtractor;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.PushBackInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.util.PDFText2HTML;
import org.apache.pdfbox.util.PDFTextStripper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Wandelt eine PDF in HTML um und macht sie damit benutzbar und den Zugriff konfigurierbar.
 * 
 * @author Jens Hantke
 *
 */
public class PDF2HTMLParser {

	static final Logger log = Logger.getLogger(PDF2HTMLParser.class);

	private long countChars;
	private long countWords;

	String linkToPDF;

	public PDF2HTMLParser(String linkToPDF) {
		this.linkToPDF = linkToPDF;
	}
	
	/**
	 * Wandelt die PDF in HTML um.
	 * 
	 * @return
	 */
	public String pdfStreamToHTML() {
		try {
			final URL url = new URL(linkToPDF);
			return inputStreamToHTML(url.openStream());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Wandelt den PDF-Stream in Text um, um beispielsweise die Waerter zu zaehlen.
	 * 
	 * @param inputStream
	 * @return
	 */
	private String inputStreamToHTML(InputStream inputStream) {
		PDFParser parser = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText;
		try {
			parser = new PDFParser(new PushBackInputStream(inputStream, 250000));
		} catch (Exception e) {
			return null;
		}
		// Es kann vorkommen, das in der PDF Links auftauchen.
		// Diese muessen Extrahiert und in den HTMLCode uebernommen werden:
		List<String> urlsAsActionInPdf = new ArrayList<String>();
		try {
			parser.parse();
			cosDoc = parser.getDocument();

			PDFText2HTML html = new PDFText2HTML("UTF-8");
			pdDoc = new PDDocument(cosDoc);
			countWordsAndChars(pdDoc);
			parsedText = html.getText(pdDoc);
			
			@SuppressWarnings("rawtypes")
      List allPages = pdDoc.getDocumentCatalog().getAllPages();
			for (int i = 0; i < allPages.size(); i++) {
				{
					PDPage page = (PDPage) allPages.get(i);
					@SuppressWarnings("rawtypes")
          List annotations = page.getAnnotations();
					for (int j = 0; j < annotations.size(); j++) {
						PDAnnotation annot = (PDAnnotation) annotations.get(j);
						if (annot instanceof PDAnnotationLink) {
							PDAnnotationLink link = (PDAnnotationLink) annot;
							PDAction action = link.getAction();
							if (action instanceof PDActionURI) {
								PDActionURI uri = (PDActionURI) action;
								String url = ""+uri.getURI();
								if(url !=null){
									if(!urlsAsActionInPdf.contains(url)){
										urlsAsActionInPdf.add(url);
									}
								}
							}
						}
					}

				}
			}
		} catch (Exception e) {
		  log.error("Problem beim Verarbeiten des PDFDokumentes Rat: Das Dokument ueberpruefen.");
			System.out.println("An exception occured in parsing the PDF Document.");
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
			return null;
		}
		
		Document doc = Jsoup.parse(parsedText);
		Element body = doc.select("body").get(0);
		for(String url: urlsAsActionInPdf){
			body.append("<div class=\"urlFromPdf\"><p>"+url+"</p></div>");
		}
		urlsAsActionInPdf = null;
		return doc.html();
	}

	public long getCountChars() {
		return countChars;
	}

	public long getCountWords() {
		return countWords;
	}

	public String plainText;
	/**
	 * Zaehlt die Waerter
	 * @param pdDoc
	 */
	private void countWordsAndChars(PDDocument pdDoc) {
		try {
			PDFTextStripper textStripper = new PDFTextStripper();
			plainText = textStripper.getText(pdDoc);
			countChars = plainText.length();
			countWords = plainText.split("\\s+").length;
		} catch (IOException e) {
			log.error("Dokument konnte nicht geaeffnet werden um dessen Waerter/Buchstaben zu zaehlen.");
		} catch (NullPointerException e) {
			log.error("Waerter konnten nicht gezaehlt werden, da das Dokument nicht richtig initialisiert wurde. ");
		}
	}

	public static void writeTexttoFile(String pdfText, String fileName) {
		try {
			PrintWriter pw = new PrintWriter(fileName);
			pw.print(pdfText);
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

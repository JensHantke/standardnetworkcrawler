package de.fu_berlin.crawler.ContentExtractor;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import de.fu_berlin.crawler.Attribute.AttributeExtraktor;
import de.fu_berlin.crawler.Attribute.ReferenceCollector;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.DebugHelper;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Extrahiert Attribute und Referenzen aus den uebergebenen Standard-Dokument.
 * 
 * @author Jens Hantke
 *
 */
public class PDFContent implements DocumentContent {

	static Logger log = Logger.getLogger(PDFContent.class);
	
	String linkToPdf;
	String contentOfPdf;
	String nameOfFile;
	List<String> references;

	File standardDocument;
	
	List<String> supportedFormats;
	
	List<AttributeExtraktor> allAttributeExtraktors;


	URL url;

	private List<ReferenceCollector> referenceCollectors;

	public PDFContent(ConfigNode configNode) {
		String supportedEndings = configNode.getAttribute("SupportedEndings");
 		if(supportedEndings != null) {
 			supportedFormats = Arrays.asList( configNode.getAttribute("SupportedEndings").split(" ") );
 		}
		log.info(supportedFormats);
		
		/*Attribute*/
		allAttributeExtraktors = new ArrayList<AttributeExtraktor>();
		List<ConfigNode> attributeExtactorNodes = Config.getConfigNodes(configNode, "AttributeExtractor",2);
		Iterator<ConfigNode> iter = attributeExtactorNodes.iterator();
		while(iter.hasNext()){
			ConfigNode xmlAttributeElement = iter.next();
			ConfigNode parentelement = xmlAttributeElement.getParent();
			if(xmlAttributeElement.hasAlternativ()){
				AttributeExtraktor attributeExtraktor = new AttributeExtraktor(xmlAttributeElement);
				for(ConfigNode child : parentelement.getChildren()){
					if(xmlAttributeElement.getXmlNode() != child.getXmlNode()){
						attributeExtraktor.addAlternative( new AttributeExtraktor(child) );
						iter.next();
					}
				}
				allAttributeExtraktors.add(attributeExtraktor);
			}else{
				allAttributeExtraktors.add(new AttributeExtraktor(xmlAttributeElement));
			}
		}
		/*Referenzen*/
		referenceCollectors = new ArrayList<ReferenceCollector>();
		List<ConfigNode> referenceCollectorNodes = Config.getConfigNodes("ReferenceCollector", configNode);
		if(referenceCollectorNodes.size()>0) {
			for(ConfigNode referenceCollectorNode : referenceCollectorNodes) {
				referenceCollectors.add(new ReferenceCollector(referenceCollectorNode));
			}
		} else {
			referenceCollectors = null;
		}
	}

	/**
	 * Liesst die PDF zur weiteren Verarbeitung ein.
	 * Zaehlt au�erdem die Buchstaben und Waerter.
	 * 
	 */
	public Standard crawlContent(Standard currentStandard) {
		try {
			url = new URL(currentStandard.getLinkToStandard());
			log.info("FileName: " + url.getFile());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		PDF2HTMLParser pdf2Html =  new PDF2HTMLParser(url.toString());
		String tempHtmlString = pdf2Html.pdfStreamToHTML();
		System.out.println("ExtractingPDF: "+url);
		
		currentStandard.setCountedCharacter(pdf2Html.getCountChars());
		currentStandard.setCountedWords(pdf2Html.getCountWords());
		
		PDF2HTMLParser.writeTexttoFile(pdf2Html.plainText , "log/htmlOutput/"+getFileName()+".txt");
		if(tempHtmlString != null) {
			Document doc = Jsoup.parse(tempHtmlString);
			currentStandard.setHtmlElement(doc);
			PDF2HTMLParser.writeTexttoFile(tempHtmlString , "log/htmlOutput/"+getFileName());
			
			executeExtractors(currentStandard);
			executeReferenceCollectors(currentStandard);
			DebugHelper.printDebugCrawledDocument("log/htmlDebugOutput/"+getFileName(), doc);
			// Aufraeumen:
			pdf2Html = null;
			tempHtmlString = null;
			doc = null;
			currentStandard.setHtmlElement(null);
		}
		return currentStandard;
	}
	@Override
	public Standard crawlFollowPage(Standard currentStandard){
		try {
			url = new URL(currentStandard.getLinkToStandard());
			log.info("FileName: " + url.getFile());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		PDF2HTMLParser pdf2Html =  new PDF2HTMLParser(url.toString());
		String tempHtmlString = pdf2Html.pdfStreamToHTML();
		if(tempHtmlString != null) {
			Document doc = Jsoup.parse(tempHtmlString);
			currentStandard.setHtmlElement(doc);
			executeExtractors(currentStandard);
		}
		return currentStandard;
	}
	
	private void executeExtractors(Standard currentStandard){
		HtmlElementWrapper htmlElement = new HtmlElementWrapper(currentStandard.getHtmlElement() );
		JexlExecutor executer = new JexlExecutor(htmlElement);
		executer.addJexleObject(currentStandard);
		executeExtractors(executer);
	}
	
	public void executeExtractors(JexlExecutor executer){
		for(AttributeExtraktor attributeExtraktor : allAttributeExtraktors){
			attributeExtraktor.executeExtractor(executer);
		}
	}
	
	public void executeReferenceCollectors(Standard currentStandard){
		HtmlElementWrapper htmlElement = new HtmlElementWrapper(currentStandard.getHtmlElement() );
		JexlExecutor executer = new JexlExecutor(htmlElement);
		executer.addJexleObject(currentStandard);
		executeReferenceCollectors(executer);
	}
	
	public void executeReferenceCollectors(JexlExecutor executer){
		if(referenceCollectors != null) {
			for(ReferenceCollector referenceCollector : referenceCollectors) {
				referenceCollector.executeCollector(executer);
			}
		}
	}
	
	
	private String getFileName(){
		String fileName = "blank";
		if(url != null){
			if(url.getFile().contains("/")){
				String[] parts = url.getFile().split("/");
				fileName = parts[parts.length-1];
				fileName = fileName.replace(".pdf", ".html"); // Gehen davon aus das die Datei die Endung .pdf hat.
			}
		}
		return fileName;
	}



	@Override
	public List<String> getSupportedFormats() {
		return supportedFormats;
	}

	

}

package de.fu_berlin.crawler.ContentExtractor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
/**
 * Extrahiert Attribute und Referenzen aus den uebergebenen Standard-Dokument.
 * 
 * @author Jens Hantke
 *
 */
public class TextContent  extends ContentExtraktor  implements DocumentContent {
	
	Logger log = Logger.getLogger(TextContent.class);
	
	public TextContent(ConfigNode configNode) {
	  super(configNode);
	}  
	
	/**
   * Liesst die Text-Datei zur weiteren Verarbeitung ein.
   * Zaehlt au�erdem die Buchstaben und Waerter.
   * 
   */
	@Override
	public Standard crawlContent(Standard currentStandard) {
		try {
			url = new URL(currentStandard.getLinkToStandard());
			log.info("FileName: " + url.getFile());
			Text2HTMLConverter textToHTML = new Text2HTMLConverter(url);
			String comments = textToHTML.text2HTML();
			currentStandard.setCountedCharacter(textToHTML.getCountChars());
			currentStandard.setCountedWords(textToHTML.getCountWords());
			Document doc = Jsoup.parse(comments);
			currentStandard.setHtmlElement(doc);
			executeExtractors(currentStandard);
			executeReferenceCollectors(currentStandard);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentStandard;
	}

	@Override
	public Standard crawlFollowPage(Standard currentStandard) {
		try {
			URL url = new URL(currentStandard.getLinkToStandard());
			log.info("FileName: " + url.getFile());
			Text2HTMLConverter textToHTML = new Text2HTMLConverter(url);
			String comments = textToHTML.text2HTML();
			Document doc = Jsoup.parse(comments);
			currentStandard.setHtmlElement(doc);
			executeExtractors(currentStandard);
			doc = null;
			url = null;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		currentStandard.setHtmlElement(null);
		return currentStandard;
	}

	@Override
	public List<String> getSupportedFormats() {
		return supportedFormats;
	}

}

package de.fu_berlin.crawler.Attribute;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.jsoup.helper.StringUtil;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Extrahiert Attribute aus HTMLElementen.
 * 
 * @author Jens.Hantke
 * 
 */
public class AttributeExtraktor {

  String name;
  String source;
  String target;
  String pattern;
  String datePattern;
  boolean hasPath;
  boolean hasComment;
  boolean caseSensitive = true;
  Path path;
  List<AttributeExtraktor> alternativen;
  ConfigNode xmlLine;

  boolean isMultiStringAttribute = false;
  List<String> weakNameblackList;
  List<String> weakNameParts;

  static Logger log = Logger.getLogger(AttributeExtraktor.class);
  static Logger attributeExtraktorLog = Logger.getLogger("AttributeExtraktor");
  static Logger memberExtraktionLog = Logger.getLogger("MemberExtraktion");

  /**
   * Holt die XmlAttribute aus dem XML Knoten und schafft somit eine
   * InformationsGrundlage fuer die Extraktion des jeweiligen zu extrahierenden
   * Attributes.
   * 
   * @param attributeXmlNode
   */
  public AttributeExtraktor(ConfigNode attributeXmlNode) {
    // holen uns die Liste der Attribute
    Map<String, String> filterAttributes = attributeXmlNode.getAttributes();

    weakNameblackList = new ArrayList<String>();
    weakNameParts = new ArrayList<String>();

    // setzen die vorgerufenden Attribute in Attribute des Objektes um
    fillAttributeExtractorFromXmlAttributes(filterAttributes);
    ConfigNode xmlPath = Config.getConfigNode("Path", attributeXmlNode);
    ConfigNode xmlComment = Config.getConfigNode("Comments", attributeXmlNode);
    alternativen = new ArrayList<AttributeExtraktor>();
    if (xmlPath == null) {
      hasPath = false;
    } else {
      path = new Path(xmlPath);
      hasPath = true;
    }
    if (xmlComment == null) {
      hasComment = false;
    } else {
      xmlLine = Config.getConfigNode("Line", xmlComment);
      hasComment = true;
    }

  }

  /**
   * Fuegen einen gefundenen alternativen Extraktor hinzu.
   * 
   * @param alternative
   */
  public void addAlternative(AttributeExtraktor alternative) {
    if (!hasAlternative()) {
      alternativen.add(alternative);
    } else {
      alternativen.get(0).addAlternative(alternative);
    }
  }

  /**
   * Gibt zurueck, ob es noch einen alternativen Extraktor gibt.
   * 
   * @return
   */
  public boolean hasAlternative() {
    return alternativen.size() != 0;
  }

  /**
   * Fuellt Variablen mit Attributen aus der XML-Konfiguration.
   * 
   * @param filterAttributes
   */
  private void fillAttributeExtractorFromXmlAttributes(Map<String, String> filterAttributes) {
    name = (String) filterAttributes.get("Name");
    source = (String) filterAttributes.get("Source");
    target = (String) filterAttributes.get("Target");
    pattern = (String) filterAttributes.get("Pattern");
    datePattern = (String) filterAttributes.get("DatePattern");
    // Ueberprueften ob es sich um ein MultiStringAttribute handelt und holen die
    // Attribute
    String isMultiStringAttributeTemp = (String) filterAttributes.get("MultiStringAttribute");
    String weakNameblackListTemp = (String) filterAttributes.get("WeakNameblackList");
    String weakNamePartsListTemp = (String) filterAttributes.get("WeakNameParts");
    String textBlackListTemp = (String) filterAttributes.get("TextBlackList");

    if (isMultiStringAttributeTemp != null && isMultiStringAttributeTemp.equals("True")) {
      isMultiStringAttribute = true;
    }
    if (isMultiStringAttribute) {
      JSONObject jsobj = JSONObject.fromObject(textBlackListTemp);
      @SuppressWarnings("unchecked")
      Map<String, List<String>> map = jsobj;
      for (Entry<String, List<String>> entry : map.entrySet()) {
        System.out.println(entry.getKey() + " " + StringUtil.join(entry.getValue(), "#"));
      }
      if (weakNameblackListTemp != null && !weakNameblackListTemp.equals("")) {
        weakNameblackList = Arrays.asList(weakNameblackListTemp.split(";"));
      }
      if (weakNamePartsListTemp != null && !weakNamePartsListTemp.equals("")) {
        weakNameParts = Arrays.asList(weakNamePartsListTemp.split(";"));
      }
      MultipleStringAttribute.setWeakNames(weakNameParts);
      MultipleStringAttribute.setWeakNameblackList(weakNameblackList);
      MultipleStringAttribute.setMultiStringBlackList(map);
      MultipleStringAttribute.processWeakNames();
    }

    if (filterAttributes.get("caseSensitive") != null) {
      if (filterAttributes.get("caseSensitive").toString().equals("false")) {
        caseSensitive = false;
      }
    }
  }

  /**
   * Extrahiert die in der Konfiguration angegebenen Werte und speichert sie per
   * Jexle Ausdruck in eine Object vom Interface JexlClass.
   * 
   * @param executer
   */
  public void executeExtractor(JexlExecutor executer) {
    String tempSource = null;
    HtmlElementWrapper tempHtmlElement = (HtmlElementWrapper) executer.execute("currentHtmlElement");
    attributeExtraktorLog.info("Name:" + name + " " + source + " " + target);

    // Wenn die Quelle nicht direkt am HtmlElement liegt.
    List<HtmlElementWrapper> currentHtmlElement = Arrays.asList((HtmlElementWrapper) executer.execute("currentHtmlElement"));
    
    // Falls es einen Pfad gibt, der traversiert werden muss:
    if (hasPath) {
      currentHtmlElement = path.traverseDomWithHtmlElements(currentHtmlElement);
      if (currentHtmlElement.size() <= 0) {
        executeAlternativExtractors(executer); // => Alternative versuchen, wenn keine validen HtmlElemente gefunden wurden
        return;
      } else {
        if (pattern != null) {
          boolean foundOne = false; // zeigt ob ein valides Element gefunden wurde.
          // Laufen alle gefundenen HTMLElemente durch und suchen per PatternMatching das Richtige:
          for (HtmlElementWrapper html : currentHtmlElement) {
            Pattern attributePattern;
            // Setzen die konfigurierte CaseSenetiv Option um-
            if (caseSensitive) {
              attributePattern = Pattern.compile(pattern);
            } else {
              attributePattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            }
            JexlExecutor innerExecuter = new JexlExecutor(html);
            // Escapen den Text.
            String unescaped = StringEscapeUtils.unescapeHtml4(innerExecuter.execute(source).toString());
            innerExecuter = null;
            String out = "";
            try {
              out = new String(unescaped.getBytes("ISO-8859-1"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
              e.printStackTrace();
            }
            Matcher matcher = attributePattern.matcher(out);
            if (matcher.find()) {
              html.setText(matcher.group().toString());
              executer.addJexleObject(html);
              foundOne = true;
              break;
            }
          }
          /*
           * Wenn alle HTML-Elemente durchsucht wurden und keines davon in Frage
           * kam, wird kein Target gesetzt.
           */
          if (!foundOne) {
            if (this.hasAlternative()) {
              executeAlternativExtractors(executer);
              return;
            }
            return;
          }
        } else { // Falls kein Pattern angegeben wurde, fuegen wir das HtmlElement einfach in den JexlKontext.
          executer.addJexleObject(currentHtmlElement.get(0));
        }
      }
    } else { // falls es keinen Pfad gibt:
      if (pattern != null) { // Wenn es ein Pattern gibt => Matcher
        Pattern attributePattern;
        if (caseSensitive) {
          attributePattern = Pattern.compile(pattern);
        } else {
          attributePattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        }
        Matcher matcher = attributePattern.matcher(executer.execute(source).toString());
        if (matcher.find()) {
          if (source.contains("getAttribute")) {
            String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
            tempSource = (String) executer.execute("currentStandard.getAttribute('" + sourceAttribute + "')");
            executer.execute("currentStandard.setAttribute('" + sourceAttribute + "','" + matcher.group().toString() + "')");
          } else {
            tempSource = (String) executer.execute(source);
            executer.execute(source + " = \"" + matcher.group().toString() + "\"");
          }
        } else {
          executeAlternativExtractors(executer); // => Alternative versuchen, wenn kein valides HtmlElement gefunden wurden
          return;
        }
      }
    }

    if (datePattern != null) { // behandeln DatumsPattern gesondert.
      parseDatePattern(executer);
    }

    if (isMultiStringAttribute) { // behandeln MultiStringAttribute gesondert:
      executer.addJexleObject(Webstructure.multipleStringValue);
      for (HtmlElementWrapper htmlWrapper : currentHtmlElement) {
        executer.addJexleObject(htmlWrapper);
        if (target.contains("setAttribute") || target.contains("setMultiValueAttribute")) {
          executer.execute(target);
        } else {
          executer.execute(target + " = " + source);
        }
      }
    } else {
      if (target.contains(".set")) { // set... benaetigt keine Source. 
        executer.execute(target); 
      } else if (source == null) {
        executer.execute(target); // auch in diesem Fall kaennen wir einfach den Ausdruck in target ausfuehren.
      } else { // ansonsonsten verwenden wir die ebend bearbeitete Source indem wir eine Zuweisung von Source auf Target generieren:
        /*
         * Wenn wir ein Attribut nur mit einem String befuellen wollen:
         * 'zuBefuellender String', dann muessen wir fuer Jexel ' gegen \"
         * austauschen.
         */
        if (source.charAt(0) == '\'' && source.charAt(source.length() - 1) == '\'') {
          source.replace("'", "\"");
        }
        executer.execute(target + " = " + source);
      }
    }
    
    if (source != null) {
      attributeExtraktorLog.info("Source: " + executer.execute(source));
    }
    // Setzen maeglicherweise angepasste Werte im JexleObject Source zurueck:
    if (tempSource != null) {
      if (source.contains("getAttribute")) {
        String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
        executer.execute("currentStandard.setAttribute('" + sourceAttribute + "','" + tempSource + "')");
      } else {
        executer.execute(source + " = \"" + tempSource.replace("\"", "\\\"") + "\"");
      }
    }
    // Fuer den Fall, dass das HTMLElement bearbeitet wurde, setzten wir es immer zurueck.
    if (tempHtmlElement != null) {
      executer.addJexleObject(tempHtmlElement);
    }
    // aufraeumen:
    currentHtmlElement = null;
    executer = null;
  }
  
  /**
   * Verwendet Datumspattern um das Datum zu extrahieren und in das gewuenschte globale Format zu speichern. 
   * 
   * @param executer
   */
  private void parseDatePattern(JexlExecutor executer) {
    SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern, Locale.ENGLISH);
    try {
      Date date = dateFormatter.parse(executer.execute(source).toString());
      if (name.equals("Date_1") || name.equals("Date_2") || name.equals("Datex")) {
        attributeExtraktorLog.info("Date:" + date + " -" + executer.execute(source).toString() + "-");
      }
      executer.execute(source + " = \"" + new SimpleDateFormat(Config.INTERNTIMESTAMP).format(date) + "\"");
    } catch (ParseException e) {
      if (this.hasAlternative()) { // fuehren die Alternative aus:
        executeAlternativExtractors(executer);
        return;
      }
      Standard standard = (Standard) executer.execute("currentStandard");
      log.error("Das gefundene Attribut konnte nicht zu einem Datums Object geparsed werden.Standard:" + standard.getTitle() + " Input : " + executer.execute(source).toString() + " Pattern: " + datePattern);
      log.error(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Ruft alternative AttributeExtractoren auf. Diese Methode wird nach dem
   * 
   * @param executer
   */
  private void executeAlternativExtractors(JexlExecutor executer) {
    for (AttributeExtraktor extractor : alternativen) {
      extractor.executeExtractor(executer);
    }
  }

  @Override
  public String toString() {
    return "[Name: " + name + ", Quelle: " + source + ", Ziel: " + target + "]";
  }
}

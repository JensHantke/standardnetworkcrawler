package de.fu_berlin.crawler.Attribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.JexlClass;

/**
 * Das MultiStringAttribute sorgt dafuer, das zu einem Attribut mehrere String-Werte gespeichert werden kaennen. 
 * Au�erdem extrahiert es aus einem Text anhand von mehreren dynamischen Kriterien verschiedene Strings extrahiert werden.
 * Am Beispiel der Firmen-Erwaehnungen hei�t dies:
 * Das die Firmen das Attribut sind und die maeglichen Namen der Firmen die verschiedenen maeglichen Strings die auf das Attribut zu treffen kaennen.
 * 
 * @author Jens Hantke
 *
 */
public class MultipleStringAttribute implements JexlClass {
  static Logger memberExtraktionLog = Logger.getLogger("MemberExtraktion");
  
  static Map<String, List<String>> weakNames;
  static HashSet<String> multiDataSet = new HashSet<String>();

  static List<String> weakNameblackList = new ArrayList<String>();
  static List<String> weakNameParts = new ArrayList<String>();
  static Map<String, List<String>> multiStringBlackList = new HashMap<String, List<String>>();
  
  public static void setWeakNames(List<String> weakNameParts){
    MultipleStringAttribute.weakNameParts = weakNameParts;
  }
  
  public static void setMultiStringBlackList(Map<String, List<String>> multiStringBlackList){
    MultipleStringAttribute.multiStringBlackList = multiStringBlackList;
  }
  
  public static void setWeakNameblackList(List<String> weakNameblackList){
    MultipleStringAttribute.weakNameblackList = weakNameblackList;
  }
  
  public MultipleStringAttribute(ConfigNode xmlNode) {
    if (xmlNode != null) {
      weakNames = new HashMap<String, List<String>>();
      readMultiStringAttribute(xmlNode);
    }
  }
  /**
   * Baut ein Verzeichnis der schwachen Namen in Bezug zu ihrem orginal Namen auf.
   * OrginalName => schwacher Name , schacher Name, ...
   */
  public static void processWeakNames(){
    for (String stringAttribute : multiDataSet) {
      for (String weakNamePart : weakNameParts) {
        if (weakNameblackList.size() == 0 || !weakNameblackList.contains(stringAttribute)) {
          Pattern attributePattern = Pattern.compile(weakNamePart);
          Matcher matcher = attributePattern.matcher(stringAttribute);
          if (matcher.find()) {
            if (!weakNames.containsKey(stringAttribute)) {
              weakNames.put(stringAttribute, new ArrayList<String>(Arrays.asList(stringAttribute.replaceAll(weakNamePart, ""))));
            } else {
              List<String> tempAlternativCompanyList = weakNames.get(stringAttribute);
              tempAlternativCompanyList.add(stringAttribute.replaceAll(weakNamePart, ""));
            }
          }
        }
      }
    }
  }
  
  /**
   * Lesen die maeglichen Strings des MultiStringAttributes aus der Konfiguration.
   * 
   * @param xmlNode
   */
  public void readMultiStringAttribute(ConfigNode xmlNode) {
    String input = xmlNode.getXmlNode().getText();
    if(input != null && !input.equals("")) {
    	String text = xmlNode.getXmlNode().getText().replace("\n", "").replace("\r", "").replace("\t", "");
      List<String> multiString =Arrays.asList( text.split(";"));
      for(String current : multiString){
      	String currentTmp = current.substring(current.indexOf("\"")+1, current.lastIndexOf("\""));
//      	System.out.println("<"+currentTmp+">");
        multiDataSet.add(currentTmp);
      }
    }
  }


  @Override
  public String getJexleName() {
    return "member";
  }

  @Override
  public Object getCurrentElement() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getText() {
    return null;
  }

  /**
   * Ueberprueft ob in einem Text die entsprechenden verschiedenen Strings vorkommen und gibt die vorkommenden Strings zurueck.
   * 
   * @param String targetText
   * @return Set<String> resultSet
   */
  public Set<String> multiContains(String targetText) {

    Set<String> found = new HashSet<String>();
    if (targetText.length() > 1) {
      if (targetText != null && !targetText.equals("")) {
        for (String firma : multiDataSet) {
          if (multiStringBlackList.containsKey(firma)) {
            for (String removeString : multiStringBlackList.get(firma)) {
              targetText = targetText.replace(removeString, "");
            }
          }
          if (containsEntry(firma, targetText)) {
            found.add(firma);
            memberExtraktionLog.info("Gefunden:Firma: '"+firma + "' ;SuchText: " + targetText);
          } else if (weakNames.containsKey(firma)) {
            for (String weakNamePart : weakNames.get(firma)) {
              if (containsEntry(weakNamePart, targetText)) {
                found.add(firma);
                memberExtraktionLog.info("Schwacher Name musste ausgetauscht werden. Zum Ueberpruefen: Neuer Name: '"+weakNamePart 
                                       + "';Gefundene Firma: '" + firma + "';SuchText: '" + targetText+"'");
              }
            }
          }
        }
      }
    }
    return found;
  }
  
  /**
   * Spezielles contains, welches ueberprueft, ob das Wort enthalten ist und das es nicht mitten in einem Wort steht.
   * 
   * @param target
   * @param text
   * @return
   */
  private boolean containsEntry(String target, String text) {
    Pattern attributePattern = Pattern.compile("(" + target + "$)|(" + target + "[\\s,])");
    Matcher matcher = attributePattern.matcher(text);
    if (matcher.find()) {
      return true;
    }
    return false;
  }
}
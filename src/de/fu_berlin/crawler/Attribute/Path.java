package de.fu_berlin.crawler.Attribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom2.Element;
import org.jsoup.select.Elements;


import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.Configuration.SaxXmlParser;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.JexlClass;

/**
 * Verwaltet den in der Konfiguration angegebenen Pfad zu einem Attribut und fuehrt diesen auf einem uebergebenen HTMLElement aus.
 *
 * @author Jens Hantke
 *
 */
public class Path {
	
	List<PathElement> objectPathToSource;
	List<JexlClass> pointerElements;
	
	Logger log = Logger.getLogger(Path.class);

	public Path(ConfigNode path) {
		if(path != null){
			objectPathToSource = new ArrayList<PathElement>();
			traverseXmlPath(path.getXmlNode());
		}
	}
	/**
	 * Traversiert den in der Konfiguration angegebenen Path und instanziiert die jeweiligen Pfad-Elemente. 
	 * 
	 * @param htmlElementWrapper
	 * @return
	 */
	public List<HtmlElementWrapper> traverseDomWithHtmlElements(List<HtmlElementWrapper> htmlElementWrapper){
		List<JexlClass> tempWrappers = new ArrayList<JexlClass>();
		tempWrappers.addAll(htmlElementWrapper);
		tempWrappers = traverseDom(tempWrappers);
		htmlElementWrapper = new ArrayList<HtmlElementWrapper>();
		for(JexlClass temp : tempWrappers){
			htmlElementWrapper.add((HtmlElementWrapper)temp);
		}
		tempWrappers = null;
		return htmlElementWrapper;
	}
	
	/**
	 * Traversiert den Dom mit Hilfe der Path Komponenten.
	 * 
	 * @param htmlElementWrapper
	 * @return
	 */
	public List<JexlClass> traverseDom(List<JexlClass> htmlElementWrapper){
		List<JexlClass> tempElements = htmlElementWrapper;
		List<JexlClass> tempElements2 = new ArrayList<JexlClass>();
		for(PathElement pathElem : objectPathToSource){
			tempElements2 = new ArrayList<JexlClass>();
			for(JexlClass pointerElement : tempElements){
				if(pointerElement.getCurrentElement() == null){
					break;
				}
				List<JexlClass> tempElements3 = pathElem.traverseDom(pointerElement);
				if(tempElements3 == null) {
					
				} else {
					if(tempElements3.size() <= 0) {
						break;
					}
					tempElements2.addAll( tempElements3 );
				}
			}
			tempElements = new ArrayList<JexlClass>();
			tempElements.addAll(tempElements2);
		}
		return tempElements2;
	}
	
	/**
	 * Waehlt die PathKomponente anhand des Names des Konfigurationskontens aus.
	 * 
	 * @param path
	 */
	private void traverseXmlPath(Element path){
		for(Element child :path.getChildren()){
			if(child.getName().equals("Parent")){
				Parent parentPath = new Parent(child);
				objectPathToSource.add(parentPath);
				traverseXmlPath(child);
				continue;
			}
			if(child.getName().equals("Find")){
				Finder finder = new Finder(child);
				objectPathToSource.add(finder);
				traverseXmlPath(child);
				continue;
			}
			if(child.getName().equals("Entry")){
				Entry entry = new Entry(child);
				objectPathToSource.add(entry);
				traverseXmlPath(child);
				continue;
			}
			if(child.getName().equals("Text")){
				Text text = new Text(child);
				objectPathToSource.add(text);
				traverseXmlPath(child);
				continue;
			}
			if(child.getName().equals("Next")){
				Next next = new Next(child);
				objectPathToSource.add(next);
				traverseXmlPath(child);
			}
		}
	}
	
	/**
	 * Geht zum Parent des HtmlElements.
	 * 
	 * @author Jens Hantke
	 *
	 */
	class Parent implements PathElement{
		Element xmlNode;

		public Parent(Element xmlNode) {
			this.xmlNode = xmlNode;
		}
		
		public List<JexlClass> traverseDom(JexlClass jexlElementWrapper){
			HtmlElementWrapper htmlElementWrapper = (HtmlElementWrapper) jexlElementWrapper;
			String htmlElement = SaxXmlParser.getAttributes(xmlNode).get("HTMLElement").toString();
			String className = null;
			// Falls eine StyleKlasse mit . angehangen ist, Trennen wir diese fuer spaetere Ueberpruefung ab.
			if(htmlElement.contains(".")){
			  String[] splittTag_Class = htmlElement.split("\\.");
			  htmlElement = splittTag_Class[0];
			  className = splittTag_Class[1];
			}
			HtmlElementWrapper parent = htmlElementWrapper.getParent(htmlElementWrapper.currentElement);
			if(!htmlElement.equals("")) {
				
				String targetTag = parent.currentElement.tag().toString();
				boolean correctStyleClass = true;
				if(className!= null){
				  correctStyleClass = hasCorrectSyleClass(parent,className);
				  while(!targetTag.equals(htmlElement) || !correctStyleClass) {
            parent = parent.getParent(parent.currentElement);
            if(parent.currentElement == null) {
              break;
            }
            targetTag = parent.currentElement.tag().toString();
            correctStyleClass = hasCorrectSyleClass(parent,className);
          }
				} else {
				  while(!targetTag.equals(htmlElement)) {
  					parent = parent.getParent(parent.currentElement);
  					if(parent.currentElement == null) {
  						break;
  					}
  					targetTag = parent.currentElement.tag().toString();
				  }
				}
			}
			List<JexlClass> tempListOfElements = new ArrayList<JexlClass>();
			tempListOfElements.add(parent);
			parent = null;
			return tempListOfElements;
		}
		/**
		 * Prueft die CSS-StyleKlasse des derzeitigen Elementes, da auch die Klasse, so angegeben ueber einstimmen muss.
		 * 
		 * @param parent
		 * @param className
		 * @return
		 */
    private boolean hasCorrectSyleClass(HtmlElementWrapper parent, String className) {
      if (parent.currentElement.attr("class") != null && parent.currentElement.attr("class") != "") {
        List<String> styeClassList = new ArrayList<String>(Arrays.asList(parent.currentElement.attr("class").split(" ")));
        if (styeClassList.contains(className)) {
          return true;
        }
      }
      return false;
    }
		
		@Override
		public String toString() {
			return "Parent:"+xmlNode;
		}
		
	}
	
	/**
	 * Traversiert zum naechsten HTML, falls mehre Elemente gefunden wurden. 
	 * 
	 * @author Jens Hantke
	 *
	 */
	class Next implements PathElement{
		Element xmlNode;

		public Next(Element xmlNode) {
			this.xmlNode = xmlNode;
		}
		
		public List<JexlClass> traverseDom(JexlClass jexlElementWrapper){
			HtmlElementWrapper htmlElementWrapper = (HtmlElementWrapper) jexlElementWrapper;
			String htmlElement = getAttribute("HTMLElement", null);
			HtmlElementWrapper next = htmlElementWrapper.getNext(htmlElementWrapper.currentElement);
			String targetTag = next.currentElement.tag().toString();
			if( htmlElement != null) {
				while(!targetTag.equals(htmlElement)) {
					next = next.getNext(next.currentElement);
					if(next.currentElement == null) {
						break;
					}
					targetTag = next.currentElement.tag().toString();
				}
			}
			List<JexlClass> tempListOfElements = new ArrayList<JexlClass>();
			tempListOfElements.add(next);
			next = null;
			return tempListOfElements;
		}
		
		@Override
		public String toString() {
			return "Parent:"+xmlNode;
		}
		
		public String getAttribute(String attributeNodeKey) {
			return getAttribute(attributeNodeKey,"");
		}
		
		public String getAttribute(String attributeNodeKey, String defaultResult) {
			if(SaxXmlParser.getAttributes(xmlNode).containsKey(attributeNodeKey)) {
				return SaxXmlParser.getAttributes(xmlNode).get(attributeNodeKey).toString();
			} else {
				return defaultResult;
			}
		}
		
	}
	
	/**
	 * Sucht ein HTML-Element per CSS Selektor.
	 * 
	 * @author Jens Hantke
	 *
	 */
	class Finder implements PathElement{
		Element xmlNode;

		public Finder(Element xmlNode) {
			this.xmlNode = xmlNode;
		}
		
		@Override
		public String toString() {
			return "Finder:"+xmlNode;
		}
		/**
		 * Trifft eine Auswahl, je nach dem was Kofiguriert ist.
		 * Wenn ein Pattern konfiguriert ist, so suchen wir Anhand des CSS-SelektorPattern 
		 * und waehlen ein bestimmtes Element aus, falls ein Index angegeben ist. Ansonsten wird das erste Ergebnis-Element genommen.
		 * Falle ein HTML-Element angegeben ist, dann suchen wir nach diesem Element.
		 */
		@Override
		public List<JexlClass> traverseDom(JexlClass htmlElementWrapper) {
			if( SaxXmlParser.getAttributes(xmlNode).containsKey("Pattern") ){
				 List<JexlClass> tmpList = traverseDomWithAttribute(htmlElementWrapper, "Pattern");
				 
				if (SaxXmlParser.getAttributes(xmlNode).containsKey("Index")) {
					if (tmpList.size() > Integer.parseInt(getAttribute("Index", "0"))) {
						tmpList = Arrays.asList(tmpList.get(Integer.parseInt(getAttribute("Index", "0"))));
					} else {
					}
				}
				return tmpList;
			} else {
				return traverseDomWithAttribute(htmlElementWrapper, "HTMLElement");
			}
		}
		/**
     * Sucht ein HTML-Element per CSS Selektor.
     */
		private List<JexlClass> traverseDomWithAttribute(JexlClass jexlElementWrapper,String attributeName){
			HtmlElementWrapper htmlElementWrapper = (HtmlElementWrapper)jexlElementWrapper;
			Elements elemente = htmlElementWrapper.elementFinder(htmlElementWrapper.currentElement, SaxXmlParser.getAttributes(xmlNode).get(attributeName).toString());
			List<JexlClass> tempListOfElements = new ArrayList<JexlClass>();
			for(org.jsoup.nodes.Element elem : elemente){
				tempListOfElements.add(new HtmlElementWrapper(elem));
			}
			elemente = null;
			return tempListOfElements;
		}
		
		public String getAttribute(String attributeNodeKey) {
			return getAttribute(attributeNodeKey,"");
		}
		
		public String getAttribute(String attributeNodeKey, String defaultResult) {
			if(SaxXmlParser.getAttributes(xmlNode).containsKey(attributeNodeKey)) {
				return SaxXmlParser.getAttributes(xmlNode).get(attributeNodeKey).toString();
			} else {
				return defaultResult;
			}
		}
		
		
	}
	
	class Entry implements PathElement{
		Element xmlNode;

		public Entry(Element xmlNode) {
			this.xmlNode = xmlNode;
		}
		
		@Override
		public String toString() {
			return "Finder:"+xmlNode;
		}

		@Override
		public List<JexlClass> traverseDom(JexlClass htmlElementWrapper) {
			if( SaxXmlParser.getAttributes(xmlNode).containsKey("Value") ){
				return Arrays.asList(htmlElementWrapper);
			} 
			return null;
		}
		
		public String getAttribute(String attributeNodeKey){
			return SaxXmlParser.getAttributes(xmlNode).get(attributeNodeKey).toString();
		}
		
	}
	/**
	 * Pattern Match fuer Text der derzeitigen gefundenen Elemente.
	 * Soll alle unerwuenschten Treffer herausfiltern.
	 * 
	 * @author Jens Hantke
	 *
	 */
	class Text implements PathElement{
		Element xmlNode;

		public Text(Element xmlNode) {
			this.xmlNode = xmlNode;
		}
		
		@Override
		public String toString() {
			return "Finder:"+xmlNode;
		}
		/**
		 * Filtert alle unerwuenschten gefundenen HtmlElemente anhand ihres Textes und des angegebenen Patterns heraus.
		 */
		@Override
		public List<JexlClass> traverseDom(JexlClass htmlElementWrapper) {
			if( SaxXmlParser.getAttributes(xmlNode).containsKey("Contains") ) {
				if( htmlElementWrapper.getText().contains( getAttribute("Contains") )){
					return Arrays.asList(htmlElementWrapper);
				}
			} else if( SaxXmlParser.getAttributes(xmlNode).containsKey("Match") ) {
				Pattern attributePattern;
				if(getAttribute("caseSensitive","false").equals("true"))	{
					attributePattern = Pattern.compile( getAttribute("Match") );
				} else {
					attributePattern = Pattern.compile( getAttribute("Match"), Pattern.CASE_INSENSITIVE );
				}
				Matcher matcher = attributePattern.matcher( htmlElementWrapper.getText() );
				if (matcher.find()) {
					return Arrays.asList(htmlElementWrapper);
				}
			}
			return null;
		}
		
		public String getAttribute(String attributeNodeKey) {
			return getAttribute(attributeNodeKey,"");
		}
		
		public String getAttribute(String attributeNodeKey, String defaultResult) {
			if(SaxXmlParser.getAttributes(xmlNode).containsKey(attributeNodeKey)) {
				return SaxXmlParser.getAttributes(xmlNode).get(attributeNodeKey).toString();
			} else {
				return defaultResult;
			}
		}
	}
	
	interface PathElement{
		public List<JexlClass> traverseDom(JexlClass pointerElement);
	}
}

package de.fu_berlin.crawler.Attribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.ContentExtractor.Identifier;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Extrahiert Referenzen aus einem Standard-Dokument.
 * 
 * @author Jens.Hantke
 * 
 */
public class ReferenceCollector {

	String name;
	String source;
	String target;
	String pattern;
	boolean hasPath;
	Path path;
	Identifier identifier;
	List<ReferenceCollector> alternativen;
	String caseSensitive;

	static Logger log = Logger.getLogger(ReferenceCollector.class);

	/**
	 * Holt die XmlAttribute aus dem XML Knoten und schafft somit eine
	 * InformationsGrundlage fuer die Extraktion des jeweiligen zu extrahierenden
	 * Attributes.
	 * 
	 * @param attributeXmlNode
	 */
	public ReferenceCollector(ConfigNode attributeXmlNode) {
		// holen uns die Liste der Attribute
		Map<String, String> filterAttributes = attributeXmlNode.getAttributes();
		// setzen die vorgerufenden Attribute in Attribute des Objektes um
		fillReferenceCollectorFromXmlAttributes(filterAttributes);
		ConfigNode xmlPath = Config.getConfigNode("Path", attributeXmlNode);
		ConfigNode xmlIdentifier = Config.getConfigNode("Identifier", attributeXmlNode);
		alternativen = new ArrayList<ReferenceCollector>();
		if (xmlPath == null) {
			hasPath = false;
		} else {
			path = new Path(xmlPath);
			hasPath = true;
		}
		if(xmlIdentifier != null){
			identifier = new Identifier(xmlIdentifier);
		} else {
			identifier = null;
		}
	}
	/**
	 * Siehe AttributeExtraktor
	 * 
	 * @param alternative
	 */
	public void addAlternative(ReferenceCollector alternative){
		if(!hasAlternative()){
			alternativen.add(alternative);
		} else {
			alternativen.get(0).addAlternative(alternative);
		}
	}
	/**
	 * Siehe AttributeExtraktor
	 * @return
	 */
	public boolean hasAlternative(){
		return alternativen.size() != 0;
	}

	/**
	 * @param filterAttributes
	 */
	private void fillReferenceCollectorFromXmlAttributes(Map<String, String> filterAttributes) {
		name = (String) filterAttributes.get("Name");
		source = (String) filterAttributes.get("Source");
		target = (String) filterAttributes.get("Target");
		pattern = (String) filterAttributes.get("Pattern");
		caseSensitive = (String) filterAttributes.get("caseSensitive");
	}

	@Override
	public String toString() {
		return "[Name: " + name + ", Quelle: " + source + ", Ziel: " + target + "]";
	}
  /**
   * Extrahiert aus dem derzeitigen HTMLElement die in der Konfiguration angegebenen Referenzen und uebergibt sie dem Identifier. 
   * 
   * @param executer
   */
	public void executeCollector(JexlExecutor executer) {
		// Holen uns die Liste der in fragekommenden derzeitigen HTMLElemente
		List<HtmlElementWrapper> currentHtmlElement = Arrays.asList( (HtmlElementWrapper)executer.execute("currentHtmlElement") );

		List<HtmlElementWrapper> listOfElement = new ArrayList<HtmlElementWrapper>();
		if (hasPath) {
			currentHtmlElement = path.traverseDomWithHtmlElements(currentHtmlElement);
			if (currentHtmlElement.size() <= 0) {
				log.error("Alternative - ReferenceCollector : "+pattern);
				executeReferenceCollector(executer);
				return;
			} else {
				Standard originStandard = (Standard)executer.execute("currentStandard");
				originStandard.setCurrentHTMLElements(currentHtmlElement);
				Iterator<HtmlElementWrapper> htmlIter = currentHtmlElement.iterator();
				while(htmlIter.hasNext()){ // Ueberpruefen die gefundenen HTMLElemente auf das konfigurierte Pattern
					HtmlElementWrapper html = htmlIter.next();
					if (pattern != null) {
						Pattern attributePattern;
						if(caseSensitive != null) {
							attributePattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
						} else {
							attributePattern = Pattern.compile(pattern);
						}
						JexlExecutor innerExecuter = new JexlExecutor(html);
						Matcher matcher = attributePattern.matcher(innerExecuter.execute(source).toString());
						while (matcher.find()) {
							HtmlElementWrapper temp = new HtmlElementWrapper(null);
							JexlExecutor tempExecuter = new JexlExecutor(temp);
							if (source.contains("getAttribute")) {
								String sourceAttribute = source.substring(source.indexOf("'", 0) + 1, source.indexOf("'", source.indexOf("'", 0) + 1));
								tempExecuter.execute("currentStandard.setAttribute('" + sourceAttribute + "','" + matcher.group().toString() + "')");
							} else {
								tempExecuter.execute(source + " = \"" + matcher.group().toString() + "\"");
							}
							log.info("\t"+matcher.group().toString());
							listOfElement.add(temp);
							// Sorgt dafuer das die HTMLDebug-Datei die extrahierten Elemente farblich unterlegt.
							html.currentElement.html(html.currentElement.html().replace(matcher.group().toString(), "<b>"+matcher.group().toString()+"</b>"));
							html.setAttribute("class", "webcrawlerClass");
							html.setAttribute("style", "background-color: red;");
						}
					} else { // falls kein Pattern angegeben ist uebergeben wir das HtmlElement einfach der weiteren extraktion.
						listOfElement.add(html);
					}
				}
			}
		}
		// Ausfuehrung des JexlAusdrucks(set ...  oder Zuweisung) und Zwischenspeicherung in die Standards
		for(HtmlElementWrapper htmlWrapper : listOfElement) {
			executer.addJexleObject(htmlWrapper);
			log.info(executer.execute(source) );
			if(target.contains("setAttribute")){
				executer.execute(target);
			} else {
				executer.execute(target + " = " + source);
			}
		}
		indentifyReferences(executer);
	}
	/**
	 * Ordnet einer Referenz(Text oder URL) der ID eines Standards zu.
	 * @param executer
	 */
	private void indentifyReferences(JexlExecutor executer){
		if( identifier != null ) {
			List<String> listOfRawReferences = extracted(executer);
			Standard originStandard = (Standard)executer.execute("currentStandard");
			originStandard.clearReferenceKeys();
			listOfRawReferences = removeDuplicate(listOfRawReferences);
			for(String rawReference : listOfRawReferences) {
				 identifier.identify(originStandard, rawReference);
			}
			originStandard.setCurrentHTMLElements(null);
		}
	}
	
	/**
	 * Entfernt Duplicate aus einer Liste von Referenzen. Damit diese nicht bearbeitet werden muessen.
	 * 
	 * @param target
	 * @return
	 */
	private List<String> removeDuplicate(List<String> target){
		List<String> singelList = new ArrayList<String>();
		Iterator<String> iter = target.iterator();
		while(iter.hasNext()) {
			String currentItem = iter.next();
			if(!singelList.contains(currentItem)) {
				singelList.add(currentItem);
			}
		}
		return singelList;
	}

	@SuppressWarnings("unchecked")
	private List<String> extracted(JexlExecutor executer) {
		return (List<String>) executer.execute(target);
	}
	
	
	/**
	 * Ruft alternative  vom ReferenceCollector auf.
	 * Diese Methode wird nach dem
	 * 
	 * @param executer
	 */
	private void executeReferenceCollector(JexlExecutor executer){
		for(ReferenceCollector extractor : alternativen){
			extractor.executeCollector(executer);
		}
	}
}

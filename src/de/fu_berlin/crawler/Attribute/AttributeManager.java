package de.fu_berlin.crawler.Attribute;

import java.util.ArrayList;
import java.util.List;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.utils.JexlExecutor;


/**
 * Verwaltet die AttributExtraktoren.
 * 
 * @author Jens Hantke
 *
 */
public class AttributeManager {
	
	List<AttributeExtraktor> allAttributeExtraktors;

	public AttributeManager(ConfigNode attributeManagerNode) {
		allAttributeExtraktors = new ArrayList<AttributeExtraktor>();
		List<ConfigNode> attributeExtactorNodes = Config.getConfigNodes("AttributeExtractor", attributeManagerNode);
		for(ConfigNode xmlAttributeElement : attributeExtactorNodes){
			allAttributeExtraktors.add(new AttributeExtraktor(xmlAttributeElement));
		}
	}
	/**
	 * Fuehrt alle AttributExtraktoren aus.
	 * 
	 * @param executer
	 */
	public void executeExtractors(JexlExecutor executer){
		for(AttributeExtraktor attributeExtraktor : allAttributeExtraktors){
			attributeExtraktor.executeExtractor(executer);
		}
	}
	

}

package de.fu_berlin.crawler.WebComponents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

public class Version {
  static Logger versionLog = Logger.getLogger("VERSION_LOG");
  String versionPattern;
  String titlePattern;
  String source;
  ConfigNode versionNode;

  boolean allVersionInOneStandard = false;
  boolean referenceDifferentVersions = false;
  boolean nestedGraph = false;
  boolean onlyLatestVersion = false;

  public Version(ConfigNode parentPageNode) {
    versionNode = Config.getConfigNode("Version", parentPageNode);
    if (versionNode != null) {
      Map<String, String> attributMap = versionNode.getAttributes();
      versionPattern = attributMap.get("VersionPattern").toString();
      titlePattern = attributMap.get("TitlePattern").toString();
      source = attributMap.get("Source").toString();
      String tempOptionAllInOne = attributMap.get("AllVersionInOneStandard");
      if (tempOptionAllInOne != null) {
        if (tempOptionAllInOne.toString().equals("true")) {
          allVersionInOneStandard = true;
        }
      }
      String tempOptionReferenceDifferentVersions = attributMap.get("ReferenceDifferentVersions");
      if (tempOptionReferenceDifferentVersions != null) {
        if (tempOptionReferenceDifferentVersions.toString().equals("true")) {
          referenceDifferentVersions = true;
        }
      }
      nestedStandard = null;
      String tempOptionnestedGraph = attributMap.get("NestedGraph");
      if (tempOptionnestedGraph != null) {
        if (tempOptionnestedGraph.toString().equals("true")) {
          nestedGraph = true;
        }
      }
      String tempOptionOnlyLatestVersion = attributMap.get("OnlyLatestVersion");
      if (tempOptionOnlyLatestVersion != null) {
        if (tempOptionOnlyLatestVersion.toString().equals("true")) {
          onlyLatestVersion = true;
        }
      }

    }
  }

  public Map<String, List<Standard>> nestedStandard;

  /**
   * Arbeitet alle Standard-Dokumente durch und verarbeitet ihre Versionen zu den konfigurierten Auspraegungen.
   * 
   * @param standardList
   * @return
   */
  public List<Standard> processStandards(List<Standard> standardList) {
    if (versionNode != null) {
      Map<String, List<Standard>> titleStandard = new HashMap<String, List<Standard>>();
      for (Standard standard : standardList) {
        JexlExecutor innerExecutor = new JexlExecutor(standard);
        String title = compilePattern(titlePattern, innerExecutor.execute(source).toString());
        String version = compilePattern(versionPattern, innerExecutor.execute(source).toString());
        standard.setVersion(version);
        if (!titleStandard.containsKey(title)) {
          List<Standard> list = new ArrayList<Standard>();
          list.add(standard);
          titleStandard.put(title, list);
        } else {
          List<Standard> subStandardList = titleStandard.get(title);
          subStandardList.add(standard);
        }
      }
      sortVersion(titleStandard);
      insertNodeEnd(titleStandard);
      if (referenceDifferentVersions) {
        insertVersionReference(titleStandard);
      }
      if (nestedGraph) {
        nestedStandard = titleStandard;
      }
      if (allVersionInOneStandard) {
        return versionaAsOneStandard(titleStandard);
      } else {
        if (onlyLatestVersion) {
          return onlyLastestStandards(titleStandard);
        }
        return standardList;
      }

    }
    return standardList;
  }
  /**
   * Fuegt Standard-Dokumenten Ende ein, wenn sie von einem verwandten Standard-Dokument abgelaest werden.
   * @param versionedList
   */
  private void insertNodeEnd(Map<String, List<Standard>> versionedList) {
    for (Map.Entry<String, List<Standard>> standard : versionedList.entrySet()) {
      versionLog.info(standard.getKey() + " ");
      List<Standard> listOfVersions = standard.getValue();
      int sizeOfList = listOfVersions.size();
      for (int i = 0; i < sizeOfList; i++) {
        // gibt es ein graežeren Standard
        if (i < (sizeOfList - 1)) {
          Standard currentStandard = listOfVersions.get(i);
          currentStandard.setAttribute("end", listOfVersions.get(i + 1).getDate());
        }
        versionLog.info("\t" + listOfVersions.get(i).getVersion() + " " + listOfVersions.get(i).getID());
      }
    }
  }
  /**
   * Fuegt Referenzen zwischen verwandten Standard-Dokumenten ein.
   * @param versionedList
   */
  private void insertVersionReference(Map<String, List<Standard>> versionedList) {
    for (Map.Entry<String, List<Standard>> standard : versionedList.entrySet()) {
      versionLog.info(standard.getKey() + " ");
      List<Standard> listOfVersions = standard.getValue();
      int sizeOfList = listOfVersions.size();
      for (int i = 0; i < sizeOfList; i++) {
        // gibt es ein graežeren Standard

        if (i < (sizeOfList - 1)) {
          Standard currentStandard = listOfVersions.get(i);
          currentStandard.setReferenceKeys(listOfVersions.get(i + 1).getID());
          currentStandard.setAttribute("end", listOfVersions.get(i + 1).getDate());
        }
        if (i >= 1) {
          Standard currentStandard = listOfVersions.get(i);
          currentStandard.setReferenceKeys(listOfVersions.get(i - 1).getID());
        }
        versionLog.info("\t" + listOfVersions.get(i).getVersion() + " " + listOfVersions.get(i).getID());
      }
    }
  }

  Map<String, String> referenceToTitle;

  /**
   * Gibt eine Liste mit aktuellen Standards zurueck.
   * 
   * @param versionedList
   * @return
   */
  private List<Standard> onlyLastestStandards(Map<String, List<Standard>> versionedList) {
    referenceToTitle = new HashMap<String, String>();
    List<Standard> newNonVersionStandardList = new ArrayList<Standard>();
    for (Map.Entry<String, List<Standard>> standard : versionedList.entrySet()) {
      List<Standard> listOfVersions = standard.getValue();
      Standard latestStandard = getLatestStandard(listOfVersions);
      newNonVersionStandardList.add(latestStandard);
    }
    return newNonVersionStandardList;
  }

  /**
   * Fuer den AIO(AllInOne), also Standard-Dokumente(DSP123_1.2.3 DSP123_1.2.4 )
   * in verschiedenen Versionen von einem Standard(DSP123), werden zu einem
   * Standard(DSP123) zusammengefasst.
   * 
   * @param versionedList
   * @return
   */
  private List<Standard> versionaAsOneStandard(Map<String, List<Standard>> versionedList) {
    referenceToTitle = new HashMap<String, String>();
    List<Standard> newNonVersionStandardList = new ArrayList<Standard>();

    // Laese die zusammengesetzte ID zum Titel auf, fuer alle Standards und
    // Referenzen
    for (Map.Entry<String, List<Standard>> standard : versionedList.entrySet()) {
      String key = standard.getKey();
      List<Standard> listOfVersions = standard.getValue();
      int sizeOfList = listOfVersions.size();
      for (int i = 0; i < sizeOfList; i++) {
        Standard currentStandard = listOfVersions.get(i);
        referenceToTitle.put(currentStandard.getID(), key);
        currentStandard.setID(key);
      }
    }

    for (Map.Entry<String, List<Standard>> standard : versionedList.entrySet()) {
      List<Standard> listOfVersions = standard.getValue();

      Standard latestStandard = getLatestStandard(listOfVersions);
      latestStandard.setAttribute("nestedSize", "0");
      latestStandard = transfromReferencesToTitle(latestStandard, latestStandard);
      listOfVersions.remove(latestStandard);
      int sizeOfList = listOfVersions.size();
      for (int i = 0; i < sizeOfList; i++) {
        Standard currentStandard = listOfVersions.get(i);
        int nestedSize = Integer.parseInt(latestStandard.getAttribute("nestedSize"));
        nestedSize++;
        latestStandard.setAttribute("nestedSize", "" + nestedSize);
        latestStandard = transfromReferencesToTitle(currentStandard, latestStandard);
      }

      newNonVersionStandardList.add(latestStandard);
    }
    return newNonVersionStandardList;
  }

  /**
   * Fuer den AIO(AllInOne) also ein nur Standard und verschiedenen Standard
   * Versionen mehr. Transformieren muessen die Referenzen angepasst werden, da
   * diese nicht mehr auf ein Standard-Dokuemnt(DSP-1234_1.2.3) zeigen, sondern
   * auf einen Standard(DSP-1234).
   * 
   * @param currentStandard
   * @param target
   * @return
   */
  private Standard transfromReferencesToTitle(Standard currentStandard, Standard target) {
    List<String> references = new ArrayList<String>(currentStandard.getReferenceKeys());
    currentStandard.clearReferenceKeys();
    for (String reference : references) {
      target.addReference(referenceToTitle.get(reference));
    }
    return target;
  }

  /**
   * Gibt von den Verschiedenen Versionen eines Standards die mit dem haechsten
   * Publikationsdatum zurueck.
   * 
   * @param differentVersions
   * @return
   */
  private Standard getLatestStandard(List<Standard> differentVersions) {
    Standard lastest = null;
    int latest = 0;
    for (Standard version : differentVersions) {
      if (latest < versionToInt(version.getDate())) {
        lastest = version;
      }
    }
    return lastest;
  }

  /**
   * Sortiert nach Versionen.
   * 
   * @param titleStandard
   */
  private void sortVersion(Map<String, List<Standard>> titleStandard) {
    for (Map.Entry<String, List<Standard>> standard : titleStandard.entrySet()) {
      Collections.sort(standard.getValue());
    }
  }

  /**
   * Konvertiert eine Version zu einem Int und macht sie somit vergleichbar.
   * 
   * @param version
   * @return
   */
  private int versionToInt(String version) {
    Pattern intPattern = Pattern.compile("\\d+", Pattern.CASE_INSENSITIVE);
    if (version != null) {
      Matcher matcher = intPattern.matcher(version);
      String cocatInt = "";
      while (matcher.find()) {
        cocatInt += matcher.group();
      }
      return Integer.parseInt(cocatInt);
    }
    return 1;
  }

  /**
   * Hilfsmethode die PatternMatching kapselt.
   * 
   * @param pattern
   * @param text
   * @return
   */
  private String compilePattern(String pattern, String text) {
    Pattern identifierPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    Matcher matcher = identifierPattern.matcher(text);
    if (matcher.find()) {
      return matcher.group();
    }
    return null;
  }

}

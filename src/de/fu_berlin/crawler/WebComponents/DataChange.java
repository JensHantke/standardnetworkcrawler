package de.fu_berlin.crawler.WebComponents;

import java.util.Map;

import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Zum Ausbessern von gesammelten Standard-Daten.
 * 
 * @author Jens Hantke
 * 
 */
public class DataChange {
	String target;
	String id;
	boolean remove = false;

	public DataChange(ConfigNode xmlAttributeElement) {
		fillAttributeDataChangeFromXmlAttributes(xmlAttributeElement.getAttributes());
	}

	/**
	 * @param filterAttributes
	 */
	private void fillAttributeDataChangeFromXmlAttributes(Map<String, String> filterAttributes) {
		target = (String) filterAttributes.get("Target");
		id = (String) filterAttributes.get("Id");
		String tempRemove = (String) filterAttributes.get("Remove");
		if (tempRemove != null && tempRemove.equals("True")) {
			remove = true;
		}

	}

	/**
	 * �ndert Daten von einem gesammelten Standard.
	 */
	public void changeData() {
		if (!remove) {
			Standard targetStandard = null;
			for (Standard currentStandard : Webstructure.standardList) {
				if (currentStandard.getID().equals(id)) {
					targetStandard = currentStandard;
				}
			}
			if (targetStandard != null) {
				JexlExecutor executor = new JexlExecutor(targetStandard);
				try {
					executor.execute(target);
				} catch (Exception e) {
				}
			}
		} else {
			Standard removeStandard = null;
			for(Standard standard :Webstructure.standardList){
				if(standard.getID().equals(id)){
					removeStandard = standard;
				}
			}
			if(removeStandard != null){
				Webstructure.standardList.remove(removeStandard);
			}
		}
	}

}

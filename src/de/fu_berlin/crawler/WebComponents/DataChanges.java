package de.fu_berlin.crawler.WebComponents;

import java.util.ArrayList;
import java.util.List;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
/**
 * Verwaltet Standard-Anpassungen.
 * 
 * @author Jens Hantke
 *
 */
public class DataChanges {
	
	List<DataChange> allChanged;

	public DataChanges(ConfigNode attributeManagerNode) {
		allChanged = new ArrayList<DataChange>();
		List<ConfigNode> attributeExtactorNodes = Config.getConfigNodes("DataChange", attributeManagerNode);
		for(ConfigNode xmlAttributeElement : attributeExtactorNodes){
			allChanged.add(new DataChange(xmlAttributeElement));
		}
	}
	
	public void executeDataChanges(){
		for(DataChange dataChange : allChanged){
			dataChange.changeData();
		}
	}
	

}

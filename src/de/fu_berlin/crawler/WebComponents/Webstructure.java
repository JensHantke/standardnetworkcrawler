package de.fu_berlin.crawler.WebComponents;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import de.fu_berlin.crawler.Analyse.NetworkAnalyse;
import de.fu_berlin.crawler.Analyse.NetworkResultCollector;
import de.fu_berlin.crawler.Attribute.MultipleStringAttribute;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.ContentExtractor.DocumentContentExtractor;
import de.fu_berlin.crawler.ResultOutput.GraphCreator;
import de.fu_berlin.crawler.StandardCollector.StandardCollector;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.DebugHelper;

public class Webstructure {

	static Logger log = Logger.getLogger(Webstructure.class);
	
	
	static ArrayList<Standard> standardList;
	public static Map<String,Standard> linkToStandardMap;
	public static String organisationName = "";
	public static Map<String,Set<String>> possibleTypesOfMultiValueAttributes = new TreeMap<String,Set<String>>();
	public static MultipleStringAttribute multipleStringValue;
	
	public Webstructure(ConfigNode configNode) {
		this(getPageUrls(configNode.getAttribute("StartingPage")), true, configNode.getAttribute("Name").toString(),configNode);
	}
	
	private static String[] getPageUrls(String startingPages){
		if(startingPages != null) {
			return startingPages.toString().split(" ");
		} else {
			String[] emptyArray = {};
			return emptyArray;
		}
	}
	
	public static ConfigNode configNode;
	
	/**
	 * Das eigentliche Herzstueck des gesamten Werkzeugs, hier wird der Ablauf der einzelnen Achritektur-Filter gesteuert(und initialisiert). 
	 * 
	 * @param pageUrls
	 * @param isPdf
	 * @param organisation
	 * @param configNode
	 */
	public Webstructure(String[] pageUrls, boolean isPdf, String organisation, ConfigNode configNode )  {
		log.info("Webstruktur mit den folgenden Standard-Verzeichnisen gefunden: "+StringUtil.join(Arrays.asList(pageUrls), "\n"));
		organisationName = organisation;
		Map<String,String> graphFiles = null;
		this.configNode = configNode;
		List<Document> htmlStaringPages = new ArrayList<Document>();
		linkToStandardMap = new HashMap<String, Standard>();
		
		// Holen die Verzeichnisseiten, falls angegeben.
		for(String startingPageUrl : pageUrls){
			try {
				htmlStaringPages.add(Jsoup.connect(startingPageUrl).maxBodySize(3145728).get());
			} catch (IOException e) {
				log.error("Folgende Seite konnte nicht aufgerufen werden: "+ startingPageUrl+" Fehler-Trace:");
				e.printStackTrace();
				return;
			}
		}
		/* Da wir nicht wissen, wann genau die MultipleStringAttributes gebraucht werden, lesen wir sie am Anfang ein.
     * Erstmal gibt es nur ein MultipleStringAttribute.
     * Vorher muss ueberprueft werden ob die Verzeichnisseiten ueberhaupt ausgelesen werden sollen.
     */
		ConfigNode standardCollector = Config.getConfigNode("StandardCollector", configNode);
		if(standardCollector!=null){
		  multipleStringValue = new  MultipleStringAttribute(Config.getConfigNode("MultipleStringAttribute",standardCollector ));
		}
		// Verzeichnisseiten nach Standards durchsuchen und Attribute sammeln
		StandardCollector elementCollector = new StandardCollector(configNode , htmlStaringPages);
		elementCollector.collectLinks();
		standardList = elementCollector.getStandards();
		if(standardList != null) {
			for(Element elem : elementCollector.getCollectedLinks()){
				elem.attr("class", "webcrawlerClass");
				elem.attr("style", "background-color: red;");
			}
			elementCollector = null;
			DebugHelper.printDebugStandardCrawledPage(organisation, htmlStaringPages);
			// Aufraeumen:
			for(Standard standard: standardList) {
				linkToStandardMap.put(standard.getLinkToStandard(), standard);
				standard.setHtmlElement(null);
			}
			elementCollector = null;
			System.gc();
			
			// Dokumente crawlen
			DocumentContentExtractor contentExtractor = new DocumentContentExtractor(configNode);
			contentExtractor.crawlDocuemtens(standardList);
			contentExtractor = null;
			System.gc();
			
			// Daten aendern:
			DataChanges changeData = new DataChanges(configNode);
			changeData.executeDataChanges();
			
			// Graphen raus schreiben:
			GraphCreator graph = new GraphCreator(configNode,organisation);
			graphFiles = graph.printGraphOutput(standardList);
			graph = null;
			standardList = null;
			System.gc();
		}
		
		// Analyse und Auswertung:
		NetworkAnalyse analyse = new NetworkAnalyse(configNode, graphFiles);
		analyse.executeExtractors();
		NetworkResultCollector.printResult();
	}

}

package de.fu_berlin.crawler.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalyseUtil {
  
  
  /**
   * Wandelt einen DateDouble von Gephi in einen formatierten Datumsstring um.
   * 
   * @param dateDouble
   * @return
   */
  public static String dateDoubleToFormatedString(double dateDouble){
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date resultdate = new Date((long) dateDouble);
    return sdf.format(resultdate);
  }
  
  /**
   * Wandelt einen DateDouble von Gephi in einen formatierten Datumsstring(Jahr) um, fuer die Jahres-Korrelation.
   * 
   * @param dateDouble
   * @return
   */
  public static String dateDoubleToYear(double dateDouble){
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    Date resultdate = new Date((long) dateDouble);
    return sdf.format(resultdate);
  }

}

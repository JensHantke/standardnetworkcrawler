package de.fu_berlin.crawler.utils;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
/**
 * 
 * @author Jens Hantke
 *
 */
public class OutputHelper {
	
	public static String CSV_OUTPUT_DIR = "result/csv/";
	
	/**
	 * Schreibt CSV raus.
	 * 
	 * @param csvLine
	 * @param fileName
	 */
	public static void writeCsv(String csvLine, String fileName){
		fileName = fileName+".csv";
		System.out.println(csvLine);
		PrintWriter pw;
    try {
	    pw = new PrintWriter(new BufferedWriter(new FileWriter(CSV_OUTPUT_DIR+fileName, true)));
			pw.print(csvLine.replace(".",",")+"\n");
			pw.close();
    } catch (FileNotFoundException e) {
	    e.printStackTrace();
    } catch (IOException e) {
			e.printStackTrace();
		}
	}

}

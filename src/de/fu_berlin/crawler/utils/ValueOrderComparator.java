package de.fu_berlin.crawler.utils;

import java.util.Comparator;
import java.util.Map;
/**
 * Sorgt dafuer, dass eine Map nach dem Wert sortiert wird.
 * Nuetzlich um zum Beispiel die Map von Firma zu Anzahl der Erwaehnungen nach den Erwaehnungen zu sortieren.
 * 
 * @author Jens Hantke
 *
 */
public class ValueOrderComparator implements Comparator<String> {

  Map<String, Integer> base;

  public ValueOrderComparator(Map<String, Integer> base) {
    this.base = base;
  }
 
  /**
   * Geben hierbei keine 0 zurueck, das dies sonst Schluessel zusamemnfuehren wuerde.
   * Allerdings funktioniert daher kein direktes get(x) auf der Map.
   */
  @Override
  public int compare(String l, String r) {
    if (base.get(l) >= base.get(r)) {
      return -1;
    } else {
      return 1;
    }
  }
}

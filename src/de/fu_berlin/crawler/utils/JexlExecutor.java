package de.fu_berlin.crawler.utils;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.apache.log4j.Logger;

import de.fu_berlin.crawler.datastructure.JexlClass;

/**
 * Zum Verwenden von Jexl-Ausdruecken.
 * 
 * @author Jens Hantke
 *
 */
public class JexlExecutor {
	
	JexlEngine jexl;
	JexlContext context;
	Logger log = Logger.getLogger(JexlExecutor.class);
	
	public JexlExecutor(Object obj) {
		jexl = new JexlEngine();
		context = new MapContext();
		addJexleObject(obj);
	}
	
	/**
	 * Fuegt ein Java-Objekt dem JexlContext hinzu.
	 * Damit wird das Java-Object in einem Jexl-Ausdruck verwendbar.
	 * 
	 * @param obj
	 */
	public void addJexleObject(Object obj){
		String jexleObjectName = ((JexlClass) obj).getJexleName();
		log.trace(jexleObjectName);
		context.set(jexleObjectName, obj);
	}
	
	/**
	 * Fuehrt einen Jexl-Ausdruck aus.
	 * 
	 * @param rule - String - Jexl-Ausdruck
	 * @return Objekt - Ergebnis des Jexl-Ausdrucks.
	 */
	public Object execute(String rule) {

		Expression e = jexl.createExpression(rule);
		return e.evaluate(context);
	}
	
	/**
	 * Fuehrt einen Jexl-Ausdruck aus und gibt das Ergebnis als String zurueck.
	 * 
	 * @param rule - String - Jexl-Ausdruck
	 * @return String - Ergebnis des Jexl-Ausdrucks.
	 */
	public String getStringResult(String rule) {
		Expression e = jexl.createExpression(rule);
		return (String) e.evaluate(context);
	}

}

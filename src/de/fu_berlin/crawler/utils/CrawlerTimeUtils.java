package de.fu_berlin.crawler.utils;

import java.text.SimpleDateFormat;

public class CrawlerTimeUtils {
	/**
	 * Gibt TimeStamp fuer Dateinamen zurueck.
	 * @return
	 */
	public static String getFormatedTime(){
		String simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH-mm-ss").format(System.currentTimeMillis());
		return ""+simpleDateFormat;
	}

}

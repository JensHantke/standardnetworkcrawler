package de.fu_berlin.crawler.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jsoup.nodes.Document;
/**
 * 
 * @author Jens Hantke
 *
 */
public class DebugHelper {
	
  /**
   * Speichert HtmlDebugSeite.
   * @param sourceNetwork
   * @param htmlStaringPages
   */
	public static void printDebugStandardCrawledPage(String sourceNetwork, List<Document> htmlStaringPages) {
		int sizeOfStartingPages = htmlStaringPages.size();
		for (int i = 0; i < sizeOfStartingPages; i++) {
			String html = htmlStaringPages.get(i).html();
			// log.info(html);
			PrintWriter writer;
			try {
				writer = new PrintWriter(sourceNetwork + "_"+i+ ".html","UTF-8");
				writer.print(html);
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Speichert HtmlDebugSeite.
	 * @param fileName
	 * @param htmlDocument
	 */
	public static void printDebugCrawledDocument(String fileName, Document htmlDocument) {
			String html = htmlDocument.html();
			PrintWriter writer;
			try {
				writer = new PrintWriter(fileName,"UTF-8");
				writer.print(html);
				writer.close();
				html = null;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	}
	/**
	 * Speichert HtmlDebugSeite.
	 * @param fileName
	 * @param html
	 */
	public static void printDebugCrawledString(String fileName, String html) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName,"UTF-8");
			writer.print(html);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
}

}

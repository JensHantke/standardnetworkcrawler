package de.fu_berlin.crawler;

import org.apache.log4j.Logger;
import de.fu_berlin.crawler.Configuration.Config;
/**
 * Laed die Configuration, wird sowohl vom Test als auch von der ausfuehrbaren Main-Methode der JAR ausgefuehrt.
 * 
 * @author Jens Hantke
 *
 */
public class ConfigLoader {
  static String crawlerConfig = "config/CrawlerConfig.xml";
  static Logger logger = Logger.getLogger(ConfigLoader.class);

  public ConfigLoader() {
    logger.info("Start with CrawlerConfigLoader");
  }

  public static void main(String[] args) {
    logger.info("Start with CrawlerConfigLoader");
    System.out.println("Verfuegbarer maximaler Speicher: " + Runtime.getRuntime().maxMemory() / 1024 / 1024 + " MB");
    if (args.length != 0) {
      crawlerConfig = args[0];
    }
    Config config = new Config(crawlerConfig);
    config.load();
  }

}
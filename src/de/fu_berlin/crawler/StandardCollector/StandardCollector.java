package de.fu_berlin.crawler.StandardCollector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.fu_berlin.crawler.Attribute.AttributeManager;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.Configuration.FilterManager;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Sammelt die vorlaeufige Standard-Dokumente von der Vezeichnisseite und fuellt sie mit den ersten Attributen.
 * 
 * @author Jens Hantke.
 *
 */
public class StandardCollector {

  private static final Logger excludedLinksLogger = Logger.getLogger("EXCLUDED_LINKS_LOGGER");
  private static final Logger includedLinksLogger = Logger.getLogger("INCLUDED_LINKS_LOGGER");
  private static final Logger standardCollectorLogger = Logger.getLogger("StandardCollector");

  private List<Document> indexDocument;
  private Elements linksToElements;
  private static Logger log = Logger.getLogger(StandardCollector.class);
  private ConfigNode collectorNode;
  private List<org.jdom2.Element> attributeExtactorNodes;
  private FilterManager filterManager;
  private AttributeManager attributeManager;
  private Element currentElement;
  private ArrayList<Standard> standards;
  private boolean ignoreDuplicate = true;

  public StandardCollector(ConfigNode parentPageNode, List<Document> htmlStaringPages) {
    indexDocument = htmlStaringPages;
    collectorNode = Config.getConfigNode("StandardCollector", parentPageNode);
    if (collectorNode != null) {
      filterManager = new FilterManager(Config.getConfigNode("FilterManager", collectorNode));
      attributeManager = new AttributeManager(collectorNode);
      standards = new ArrayList<Standard>();
      String tempIgnoreDuplicate = collectorNode.getAttributes().get("ignoreDuplicate");
      if (tempIgnoreDuplicate != null) {
        if (tempIgnoreDuplicate.toString().equals("false")) {
          ignoreDuplicate = false;
        }
      }
    }
  }

  /**
   * Sammelt Links, Filtert sie nach konfigurierten Regeln und extrahiert erste Attribute.
   * 
   */
  public void collectLinks() {
    if (collectorNode != null) {
      linksToElements = new Elements();

      for (Document htmlStartingPage : indexDocument) {
        linksToElements.addAll(htmlStartingPage.select("a[href]"));
      }

      if (linksToElements.size() <= 0) {
        log.error("Es wurden keine URLs auf den Startseite gefunden.");
      }
      
      List<String> listOfLinks = new ArrayList<String>();
      Iterator<Element> linkIterator = linksToElements.iterator();
      int initialSize = linksToElements.size();
      int linkCounter = 1;
      
      // Gehen alle gefundenen Links durch:
      while (linkIterator.hasNext()) {
        Element link = linkIterator.next();
        currentElement = link;
        // Durchlaufen alle Filter und schlie�en die ausgefilterten Links aus.
        Standard standard = new Standard(link);
        HtmlElementWrapper htmlElement = new HtmlElementWrapper(standard.getHtmlElement());
        includedLinksLogger.info("(" + linkCounter + "/" + initialSize + ") " + htmlElement.getHref());
        linkCounter++;
        JexlExecutor executer = new JexlExecutor(htmlElement);
        executer.addJexleObject(standard);

        if (!filterManager.executeFilters(executer)) { // Falls der Filter verneint, ueberspringen wir das Dokument.
          linkIterator.remove();
          excludedLinksLogger.info(currentElement);
        } else {
          /*
           * Sortieren alle doppelten Links aus, dies sorg f�r eine
           * Beschleunigung beim Crawlen von W3C, da diese ca. 300 Doppelungen
           * auf der Verzeichniss Seite hat.
           */
          if (ignoreDuplicate) {
            if (!listOfLinks.contains(htmlElement.getHref())) {
              standards.add(standard);
            } else {

              log.info("- Doppelt: (" + currentElement + ")");
              standard = null;
              continue;
            }
          } else { // Fuegen Standard-Dokument hinzu.
            standards.add(standard);
          }
          listOfLinks.add(htmlElement.getHref());
          log.trace(attributeExtactorNodes);
          log.debug("+ (" + currentElement + ")");
        }
      }
      // AttributeExtraktoren auf Verzeichnisseite:
      Iterator<Standard> standardIterator = standards.iterator();
      while (standardIterator.hasNext()) {
        Standard currentStandard = standardIterator.next();
        HtmlElementWrapper htmlElement = new HtmlElementWrapper(currentStandard.getHtmlElement());
        JexlExecutor executer = new JexlExecutor(htmlElement);
        executer.addJexleObject(currentStandard);
        attributeManager.executeExtractors(executer);

        log.debug(currentStandard);
        standardCollectorLogger.info(currentStandard);
      }
      log.info("Links: (" + linksToElements.size() + ")");
    }
  }

  public boolean isEmpty() {
    return linksToElements.size() > 0;
  }

  public Elements getCollectedLinks() {
    return linksToElements;
  }

  public ArrayList<Standard> getStandards() {
    return standards;
  }

}

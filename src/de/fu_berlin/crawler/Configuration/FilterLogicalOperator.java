package de.fu_berlin.crawler.Configuration;

/**
 * Logische Operatoren die im Filterbaum benutzt werden.
 * 
 * @author Jens Hantke
 *
 */
public class FilterLogicalOperator {
	
	String operator;
	
	public FilterLogicalOperator(String operator){
		this.operator = operator;
	}
	
	public boolean execute(boolean f, boolean s) {
		if (operator.equals("And")) {
			return f && s;
		}
		if (operator.equals("Or")) {
			return f || s;
		}
		return false;

	}

}

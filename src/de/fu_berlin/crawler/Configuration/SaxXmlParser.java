package de.fu_berlin.crawler.Configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * 
 * 
 * @author Jens Hantke
 *
 */
public class SaxXmlParser {

	public Element rootElement;
	public static FilterTree tree;
	public static List<Element> elements = new ArrayList<Element>();
	
  private static Element resultElementOfFindOperation = null;
	
	public static final Logger log = Logger.getLogger(SaxXmlParser.class);
	
	public SaxXmlParser(String crawlerConfig) {
		Document doc;
		try {
			doc = new SAXBuilder().build(crawlerConfig);
			rootElement = doc.getRootElement();
		} catch (JDOMException e) {
		  log.error("Die Xml Configurationsdatei("+crawlerConfig+") konnte nicht verwendet werden. Ueberpruefen sie die Syntax der XML und lesen sie sich die nachfolgende Fehlermeldung an um den Ursprung des Fehlers zu finden.");
      e.printStackTrace();
		} catch (IOException e) {
			log.error("Die Xml Configurationsdatei("+crawlerConfig+") konnte nicht geaeffnet werden. Ueberpruefen Sie ob diese vorhanden ist. Die Anwendung wird beendet.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Baut den FilterTree aus der KonfigurationsXML auf.
	 * 
	 * @param subTreeRoot
	 * @param targetNames
	 * @return
	 */
	public static FilterTree fillStackWithElementOrder(Element subTreeRoot, List<String> targetNames){
		FilterTreeNode rootNode = new FilterTreeNode(null, subTreeRoot);
		tree = new FilterTree(rootNode);
		recfillStackWithElementOrder(rootNode, targetNames);
		return tree;
	}
	
	/**
	 * Baut den Baum rekursive auf.
	 * 
	 * @param parent
	 * @param targetNames
	 */
	private static void recfillStackWithElementOrder(FilterTreeNode parent, List<String> targetNames){
		Element subTreeRoot = parent.getXmlElement();
		if (subTreeRoot.getChildren().size() > 0) {
			for (Element child : subTreeRoot.getChildren()) {
				if (targetNames.contains(child.getName())) {
					FilterTreeNode childNode = new FilterTreeNode(parent, child);
					tree.addNode(parent,childNode);
					recfillStackWithElementOrder(childNode, targetNames);
				}
				
			}
		}
	}
	
	/**
	 * Sucht im Unterbaum unter dem angegebenen Vaterknoten nach einem XML Knoten.
	 * 
	 * @param subTreeRoot
	 * @param targetName
	 * @return
	 */
	public static Element findElementInSubXmlTree(Element subTreeRoot, String targetName) {
		resultElementOfFindOperation = null;
		recfindElementInSubXmlTree(subTreeRoot, targetName);
		return resultElementOfFindOperation;
	}
	
	 /**
   * Sucht im Unterbaum unter dem angegebenen Vaterknoten nach mehreren XML Knoten.
   * 
   * @param subTreeRoot
   * @param targetName
   * @return
   */
	public static List<Element> findElementsInSubXmlTree(Element subTreeRoot, String targetName) {
		// laeschen die alte Liste
		elements = new ArrayList<Element>();
		return recursiveFindElementsInSubXmlTree(subTreeRoot,targetName);
	}
	
  /**
   * Sucht im Unterbaum unter dem angegebenen Vaterknoten nach mehreren XML Knoten, jedoch nur bis zu einer bestimmten Baumtiefe.
   * 
   * @param subTreeRoot
   * @param targetName
   * @return
   */
	public static List<Element> findElementsInSubXmlTree(Element subTreeRoot, String targetName,int maxDeep) {
		// laeschen die alte Liste
		elements = new ArrayList<Element>();
		return recursiveFindElementsInSubXmlTree(subTreeRoot,targetName,maxDeep);
	}

	 /**
   * Sucht in der XML nach einem XML-Knoten. Ist aeqivalent fuer Config.getConfigNode(...).
   * 
   * @param subTreeRoot
   * @param targetName
   * @return
   */
  private static List<Element> recursiveFindElementsInSubXmlTree(Element subTreeRoot, String targetName) {
    if (subTreeRoot.getChildren().size() > 0) {
      for (Element child : subTreeRoot.getChildren()) {
        if (child.getName().equals(targetName)) {
          elements.add(child);
        }
        recursiveFindElementsInSubXmlTree(child, targetName);
      }
    }
    return elements;
  }
  /**
  * Sucht in der XML nach einem XML-Knoten. Ist aeqivalent fuer Config.getConfigNodes(...) mit Tiefe.
  * 
  * @param subTreeRoot
  * @param targetName
  * @return
  */
  private static List<Element> recursiveFindElementsInSubXmlTree(Element subTreeRoot, String targetName,int deep) {
    if (subTreeRoot.getChildren().size() > 0) {
      for (Element child : subTreeRoot.getChildren()) {
        if (child.getName().equals(targetName)) {
          elements.add(child);
        }
        if(deep>0){
          deep--;
          recursiveFindElementsInSubXmlTree(child, targetName,deep);
        }
      }
    }
    return elements;
  }
  
  /**
  * Sucht in der XML nach einem XML-Knoten. Ist aeqivalent fuer Config.getConfigNodes(...) ohne Tiefe.
  * 
  * @param subTreeRoot
  * @param targetName
  * @return
  */
  private static void recfindElementInSubXmlTree(Element subTreeRoot, String targetName) {
    if (subTreeRoot.getChildren().size() > 0) {
      for (Element child : subTreeRoot.getChildren()) {
        if (child.getName().equals(targetName)) {
          resultElementOfFindOperation = child;
          return;
        }
        findElementInSubXmlTree(child, targetName);
      }
    }
  }
	
  /**
   * Liefert eine Map aus allen Attributen eines XML-Knotens zurueck.
   * 
   * @param xmlElement
   * @return
   */
	public static Map<String, String> getAttributes(Element xmlElement) {

		Map<String, String> AttributeMap = new HashMap<String, String>();
		for (Attribute attr : xmlElement.getAttributes()) {
			AttributeMap.put(attr.getName(), attr.getValue());
		}
		return AttributeMap;
	}

}
package de.fu_berlin.crawler.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.Attribute.Path;
import de.fu_berlin.crawler.datastructure.HtmlElementWrapper;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Fuehrt einen Filter auf dem derzeitigen HTMLElement aus. Dabei kann Match , Contains oder Endswith verwendet werden.
 * Desweiteren wird unterschieden in Include oder Exclude. Also ob die gefundenen Elemente aus der Liste heraus gefiltert werden sollen
 *  oder implizit erwuenscht sind.
 * 
 * @author Jens Hantke
 *
 */
public class Filter {

	String pattern;
	String source;
	boolean include;
	String filterOperation;
	boolean not;
	
	boolean hasPath;
	Path path;
	
	public boolean filterResult;
	boolean caseSensitive = true;
	
	static Logger log = Logger.getLogger(Filter.class);

	public Filter(ConfigNode filterXmlNode) {
		Map<String, String> filterAttributes = filterXmlNode.getAttributes();
		fillFilterFromXmlAttributes(filterAttributes);
		ConfigNode xmlPath = Config.getConfigNode("Path", filterXmlNode);
		if (xmlPath == null) {
			hasPath = false;
		} else {
			path = new Path(xmlPath);
			hasPath = true;
		}
	}
	
	/**
	 * Fuer JUnit Tests
	 * @param pattern
	 * @param include
	 * @param filterOperation
	 */
  public Filter(String pattern, boolean include, String filterOperation) {
    this.pattern = pattern;
    this.include = include;
    this.filterOperation = filterOperation;
  }
	
	/**
	 * <Filter Pattern=".pdf" Include="true" Operation="endsWith" />
	 * 
	 * @param filterAttributes
	 */
	private void fillFilterFromXmlAttributes(Map<String, String> filterAttributes) {
		pattern = (String) filterAttributes.get("Pattern");
		include = Boolean.parseBoolean(filterAttributes.get("Include").toString());
		filterOperation = (String) filterAttributes.get("Operation");
		not = Boolean.parseBoolean(filterAttributes.get("Not").toString());
		source = (String) filterAttributes.get("Source");
		if (filterAttributes.get("caseSensitive") != null) {
			caseSensitive = Boolean.parseBoolean(filterAttributes.get("caseSensitive").toString());
		}
	}



	/**
	 * Fuehrt eine Filterung auf dem derzeitigen HtmlElement durch.
	 * 
	 * @param validateString
	 * @return
	 */
	public void execute(JexlExecutor executor) {
		List<HtmlElementWrapper> currentHtmlElements = Arrays.asList( (HtmlElementWrapper)executor.execute("currentHtmlElement") );
		if(hasPath) { 
			/* Falls der Pfad nicht valide ist, nehmen wir an, das dies ein Pattern versto� ist. */
			currentHtmlElements = path.traverseDomWithHtmlElements(currentHtmlElements);
			if(currentHtmlElements.size() <= 0) {
				return;
			} 
			executor.addJexleObject( currentHtmlElements.get(0) );
		}
		HtmlElementWrapper currentHtmlElement = (HtmlElementWrapper)executor.execute("currentHtmlElement");
		String validateString = executor.getStringResult(source);
		if (filterOperation.equals("endsWith")) {
			filterResult = validateString.endsWith(pattern);
			currentHtmlElement.setAttribute(pattern, "endsWith_"+filterResult);
			if(hasPath && filterResult){ 
				currentHtmlElement.setAttribute("style", "background-color: orange;");
			}
		}
		if (filterOperation.equals("contains")) {
			if (not) {
				filterResult = !validateString.contains(pattern);
			} else {
				filterResult = validateString.contains(pattern);
				currentHtmlElement.setAttribute(pattern, "contains_"+filterResult);
				if(hasPath && filterResult){ 
					currentHtmlElement.setAttribute("style", "background-color: orange;");
				}
			}
		}
		if (filterOperation.equals("match")) {
			Matcher matcher;
			if(caseSensitive){
				matcher = Pattern.compile(pattern).matcher(validateString);
			} else {
				matcher = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE).matcher(validateString);
			}
			boolean match = matcher.find();
			if (not) {
				filterResult = !match;
				currentHtmlElement.setAttribute(pattern.replace(" ", ""), "match_"+filterResult);
				if(hasPath && filterResult){ 
					currentHtmlElement.setAttribute("style", "background-color: orange;");
				}
			} else {
				filterResult = match;
				currentHtmlElement.setAttribute(pattern.replace(" ", ""), "match_"+filterResult);
				if(hasPath && filterResult){ 
					currentHtmlElement.setAttribute("style", "background-color: orange;");
				}
			}
		}
	}

	@Override
	public String toString() {
		return "[FilterOperation: "+filterOperation+", Include: "+include+", Pattern: "+pattern+", Not: "+not+"]";
	}

}

package de.fu_berlin.crawler.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jdom2.Element;

import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Schnittstelle zur XML-Konfiguration.
 * 
 * @author Jens Hantke
 *
 */
public class Config {
	static SaxXmlParser  parser;
	public static String INTERNTIMESTAMP ="yyyy-MM-dd";
	
	public Config(String pathToCrawlerConfig){
		parser = new SaxXmlParser(pathToCrawlerConfig);
	}
	
	/**
	 * Laed die Konfiguration und leitet damit den Start einer Instanz des Crawlers mit seiner dazugehaerigen Webstruktur ein.
	 * 
	 */
	public void load(){
		for (ConfigNode page : getRootConfigNodes()) {
				new Webstructure(page);
		}
	}
	
	private List<ConfigNode> getRootConfigNodes() {
		ConfigNode node = new ConfigNode("Pages", parser.rootElement);
		return getConfigNodes("Page", node);
	}
	
	public static ConfigNode getRootConfigNode() {
		ConfigNode node = new ConfigNode(parser.rootElement);
		return node;
	}
	
	/**
	 * Sucht in der Kofiguration den entsprechenden Konfigurationsknoten rekursiv unter dem angegebenen Vaterknoten.
	 * 
	 * @param targetConfigNodeName Name des gesuchten Konfigurationsknoten, z.B. AttributeExtraktor
	 * @param parent - Vaterknoten unter dem gesucht werden soll.
	 * @return ConfigNode - Konfigurationsknoten, falls nicht gefunden null
	 */
	public static ConfigNode getConfigNode(String targetConfigNodeName, ConfigNode parent){
		Element target = SaxXmlParser.findElementInSubXmlTree(parent.getXmlNode(), targetConfigNodeName);
		ConfigNode config = new ConfigNode(targetConfigNodeName, target);
		if(target == null){
			config = null;
		}
		return config;
	}
	 /**
   * Sucht in der Kofiguration den entsprechenden Konfigurationsknoten rekursiv unter dem angegebenen Vaterknoten.
   * 
   * @param targetConfigNodeName Name der gesuchten Konfigurationsknoten, z.B. AttributeExtraktor
   * @param parent - Vaterknoten unter dem gesucht werden soll.
   * @return List<ConfigNode> - Liste von Konfigurationsknoten, falls nicht gefunden , leer
   */
	public static List<ConfigNode> getConfigNodes(String targetConfigNodeName, ConfigNode parent){
		List<Element> elements = SaxXmlParser.findElementsInSubXmlTree(parent.getXmlNode(), targetConfigNodeName);
		List<ConfigNode> configNodes = new ArrayList<ConfigNode>();
		for(Element element : elements) {
			configNodes.add(new ConfigNode(targetConfigNodeName, element));
		}
		return configNodes;
	}
	
	/**
	 * Findet den entsprechenden Filterbaum unter dem angegebenen Knoten und fuehrt die entsprechende Filterung durch.
	 * 
	 * @param parentNode
	 * @param executer
	 * @return
	 */
	public static Boolean evaluateFilterTree(ConfigNode parentNode, JexlExecutor executer){
		FilterTree stack = SaxXmlParser.fillStackWithElementOrder(parentNode.getXmlNode(), Arrays.asList("And","Or","Filter"));
		stack.traverseAndEvaluateFilterTree(stack.rootNode, executer);
		return stack.rootNode.value;
	}
	
	/**
	 *  Siehe getConfigNodes(...) nur das die rekursive Tiefe ein Limit hat.
	 *  
	 * @param i - maximale Tiefe
   * @param targetConfigNodeName Name der gesuchten Konfigurationsknoten, z.B. AttributeExtraktor
   * @param parent - Vaterknoten unter dem gesucht werden soll.
   * @return List<ConfigNode> - Liste von Konfigurationsknoten, falls nicht gefunden , leer
	 */
	public static List<ConfigNode> getConfigNodes(ConfigNode parent, String targetConfigNodeName, int i) {
		List<Element> elements = SaxXmlParser.findElementsInSubXmlTree(parent.getXmlNode(), targetConfigNodeName,i);
		List<ConfigNode> configNodes = new ArrayList<ConfigNode>();
		for(Element element : elements) {
			configNodes.add(new ConfigNode(targetConfigNodeName, element));
		}
		return configNodes;
	}
	
}

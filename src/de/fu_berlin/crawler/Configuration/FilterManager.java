package de.fu_berlin.crawler.Configuration;

import java.util.List;

import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Verwaltet die Filter und fuehrt den Filterbaum aus.
 * 
 * @author Jens Hantkeg
 *
 */
public class FilterManager {

	List<Filter> allFilter;
	FilterTree stack;
	ConfigNode filterManagerNode;


	public FilterManager(ConfigNode filterManagerNode) {
		if(filterManagerNode != null){
			this.filterManagerNode = filterManagerNode;
		} else {
			throw new NullPointerException("Es muss ein XML-Knoten mit dem Namen 'FilterNode' geben. Dieser wurde nicht gefunden.'");
		}
	}

	public boolean executeFilters(JexlExecutor executer){
		return Config.evaluateFilterTree(filterManagerNode, executer);
	}
        

}


package de.fu_berlin.crawler.Configuration;

import org.apache.log4j.Logger;

import de.fu_berlin.crawler.utils.JexlExecutor;

public class FilterTree {
	
	static Logger logger = Logger.getLogger(FilterTree.class);
	static Logger filterTreeLog = Logger.getLogger("FILTER_TREE_LOG");
	
	/**
	 * Bauen den Baum aus dem angegebenen Wurzelknoten auf.
	 * 
	 * @param rootNode
	 */
	public FilterTree(FilterTreeNode rootNode) {
		this.rootNode = rootNode;
	}

	public FilterTreeNode rootNode = null;
	
	/**
	 * Fuegt einen Knoten, an dem angebenen Vater hinzu.
	 * Ueber diese Funktion wird der Baum aufgebaut.
	 * 
	 * @param parent
	 * @param child
	 */
	public void addNode(FilterTreeNode parent,FilterTreeNode child){
		if(parent!=null && child != null){
			parent.addChild(child);
		}else {
			throw new NullPointerException("Weder der Vater Knoten noch der an Ihm einzufuegende Knoten darf null sein.");
		}
	}
	
	/**
	 * Traversiert den Baum in postOrder-Reihenfolge und Wertet dabei die Attribute an den Knoten aus.
	 * <b>Achtung: diese Methode geht davon aus das sich in den Blaettern auswertbare Elemente befinden, in dem Fall Filter.</b>
	 * 
	 * @param node - jetztiger zu traversierender Knoten
	 * @param validateString - Der zu ueberpruefende String
	 */
	public void traverseAndEvaluateFilterTree(FilterTreeNode node, JexlExecutor executor){
		String nodeName = node.xmlNode.getName();
		FilterTreeNode parentNode = node.parentNode;
		
		for(FilterTreeNode child : node.children){
			traverseAndEvaluateFilterTree(child, executor);
		}
		if(nodeName.equals("Filter")){
			Filter filter = new Filter(new ConfigNode(node.xmlNode.getName(), node.xmlNode));
			filter.execute(executor);
			node.value = filter.filterResult; 
		}
		if(parentNode!=null){
			if(parentNode.value==null){
				parentNode.value = node.value;
			}else {
				if(parentNode.getXmlElement().getName().equals("And")){
					parentNode.value &= node.value; 
				}
				if(parentNode.getXmlElement().getName().equals("Or")){
					parentNode.value |= node.value; 
				}
			}
		}
		filterTreeLog.trace(executor.execute("currentHtmlElement"));
		filterTreeLog.trace(node);
		//logger.trace(node);
	}
	
	/**
	 * Hilfs Methode zum Ausgeben des Baumes. Sollte zu Debug-Zwecken verwendet werden.
	 * 
	 * @param node
	 * @param validateString
	 * @return
	 */
	public String traverseValueTree(FilterTreeNode node, String validateString){
		for(FilterTreeNode child : node.children){
			traverseValueTree(child, validateString);
		}
		return node.toString();
	}
	
	@Override
	public String toString() {
		traverseValueTree(rootNode,"");
		return "";
	}

}

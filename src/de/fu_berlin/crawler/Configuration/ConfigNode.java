package de.fu_berlin.crawler.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;
/**
 * Akstraktion eines XMl-Knotens und seiner Attribute. 
 * 
 * @author Jens Hantke
 *
 */
public class ConfigNode {
	
	private Map<String,String> attributes;
	private String configNodeName;
	private Element xmlNode;
	
	public ConfigNode(Element xmlNode){
		setConfigNodeName(xmlNode.getName());
		this.xmlNode = xmlNode;
	}
	
	public ConfigNode(String xmlNodeName, Element xmlNode){
		setConfigNodeName(xmlNodeName);
		this.xmlNode = xmlNode;
	}
	
	public Element getXmlNode() {
		return xmlNode;
	}
	
	public Map<String,String> getAttributes(){
		return SaxXmlParser.getAttributes(xmlNode);
	}
	
	public String getAttribute(String name) {
		if(attributes == null){
			attributes = SaxXmlParser.getAttributes(xmlNode);
		}
		return attributes.get(name);
	}
	
	public List<ConfigNode> getChildren() {
		List<Element> elements = xmlNode.getChildren();
		List<ConfigNode> configNodes = new ArrayList<ConfigNode>();
		for(Element element : elements) {
			configNodes.add(new ConfigNode(element.getName(), element));
		}
		return configNodes;
	}
	
	public String getName() {
		return xmlNode.getName();
	}
	
	public boolean hasAlternativ() {
		return xmlNode.getParentElement().getName().equals("Alternative");
	}
	
	public ConfigNode getParent() {
		 return new ConfigNode(xmlNode.getParentElement().getName(), xmlNode.getParentElement());
	}

  public String getConfigNodeName() {
    return configNodeName;
  }

  public void setConfigNodeName(String configNodeName) {
    this.configNodeName = configNodeName;
  }

}

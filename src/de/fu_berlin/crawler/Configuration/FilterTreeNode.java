package de.fu_berlin.crawler.Configuration;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;


/**
 * Abstraktion eines Knotes des Filterbaums.
 * 
 * @author Jens Hantke
 *
 */
public class FilterTreeNode{
	
	FilterTreeNode parentNode;
	public Boolean value = null;
	Element xmlNode;
	List<FilterTreeNode> children;
	
	public FilterTreeNode(FilterTreeNode parentNode, Element xmlNode) {
		this.parentNode = parentNode;
		this.xmlNode = xmlNode;
		children = new ArrayList<FilterTreeNode>();
	}
	
	
	public void addChild(FilterTreeNode node){
		children.add(node);
	}
	
	public boolean hasChildren(){
		return 0 != children.size();
	}
	
	public Element getXmlElement(){
		return xmlNode;
	}
	
	public boolean hasValue(){
		return null != value;
	}
	
	@Override
	public String toString() {
		if(xmlNode.getName().equals("Filter")){
			return "["+xmlNode+","+value+", "+new Filter(new ConfigNode(xmlNode))+"]";
		}
		return "["+xmlNode+","+value+"]";
	}
	
}
package de.fu_berlin.crawler.datastructure;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import de.fu_berlin.crawler.WebComponents.Webstructure;

public class Standard implements JexlClass, Cloneable, Comparable<Standard> {
	
	
	static final Logger log = Logger.getLogger(Standard.class); 

	
	String  jexleName = "currentStandard";
	String title;
	String linkToStandard;
	
	List<String> references;
	Map<String,Integer> referenceWeight;

	String date;
	String version;
	String status;
	String author;
//	String StandardID;
	String shortTitle;
	String kind;
	String ID;
	String linkedByDirectoryPage;
	private String fileName;
	Long countedCharacter = (long) 0;
	Long countedWords = (long) 0;
	
	Map<String,String> customAttributes;
	
	@Override
	public Standard clone() throws CloneNotSupportedException {
		Standard standard = new Standard(shortTitle, linkToStandard, null, date, version, status, author, "");
		standard.jexleName = new String(this.jexleName);
		if(this.title == null){
			standard.title = new String();
		}else {
			standard.title = new String(this.title);
		}
		standard.setID(new String(this.ID));
		standard.countedCharacter = this.countedCharacter;
		standard.countedWords = this.countedWords;
		standard.references = new ArrayList<String>(references);
		standard.referenceWeight = new HashMap<String,Integer>(referenceWeight);
		standard.customAttributes = new HashMap<String,String>(customAttributes);
		standard.linkedByDirectoryPage = linkedByDirectoryPage;
		standard.setMap = new HashMap<String,Set<String>>(setMap);
		return standard;
	}
	
	public void setJexleName(String jexleName) {
		this.jexleName = jexleName;
	}
	
	public String getLinkedByDirectoryPage() {
		return linkedByDirectoryPage;
	}

	public void setLinkedByDirectoryPage(String linkedByDirectoryPage) {
		this.linkedByDirectoryPage = linkedByDirectoryPage;
	}

	
	//TODO: Incompatible mit Getter
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		String[] partedLink = linkToStandard.split("/");
		int index = partedLink.length-1;
		String result = "";
		if(partedLink[index].equals("index.html")) {
			index--;
		}
		result = partedLink[index];
		if(result.contains(".")) {
			result = result.substring(0, result.indexOf("."));
		}
		return result;
	}

	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}


	public Standard(){
		customAttributes = new HashMap<String,String>();
		references = new ArrayList<String>();
		referenceWeight = new HashMap<String,Integer>();
	}
	
	
	public Standard(String linkTohtmlDocument){
		setLinkToStandard(linkTohtmlDocument);
		customAttributes = new HashMap<String,String>();
		references = new ArrayList<String>();
		referenceWeight = new HashMap<String,Integer>();
	}
	
	public Standard(Element htmlElement){
		this.htmlElement = htmlElement;
		customAttributes = new HashMap<String,String>();
		referenceWeight = new HashMap<String,Integer>();
		references = new ArrayList<String>();
	}
	
	public Standard(String title, String linkToStandard,Element htmlElement){
		this.title = title;
		this.linkToStandard = linkToStandard;
		this.htmlElement = htmlElement;
		customAttributes = new HashMap<String,String>();
		references = new ArrayList<String>();
		referenceWeight = new HashMap<String,Integer>();
	}
	
	/**
	 * Fuer JUnit Tests, die kein HTMLElement zur Verfuegung haben.
	 * 
	 * @param title
	 * @param linkToStandard
	 * @param referenceKeys
	 * @param date
	 * @param version
	 * @param status
	 * @param author
	 * @param standardID
	 */
	public Standard(String title, String linkToStandard, List<String> referenceKeys, String date, String version, String status, String author, String standardID) {
	  this.title = title;
	  this.linkToStandard = linkToStandard;
	  this.references = referenceKeys;
	  this.date = date;
	  this.version = version;
	  this.status = status;
	  this.author = author;
	  this.ID = standardID;
	  customAttributes = new HashMap<String,String>();
	  references = new ArrayList<String>();
	  referenceWeight = new HashMap<String,Integer>();
  }
	
	
	
	
	/**
   * @return the title
   */
  public String getTitle() {
  	return title;
  }

	/**
   * @param title the title to set
   */
  public void setTitle(String title) {
  	this.title = title;
  }

	/**
   * @return the linkToStandard
   */
  public String getLinkToStandard() {
  	return linkToStandard;
  }

	/**
   * @param linkToStandard the linkToStandard to set
   */
  public void setLinkToStandard(String linkToStandard) {
  	this.linkToStandard = linkToStandard;
  }

	/**
   * @return the referenceKeys
   */
  public List<String> getReferenceKeys() {
  	return references;
  }

	/**
   * @param referenceKeys the referenceKeys to set
   */
  public void setReferenceKeys(String referenceKeys) {
  	if(!this.references.contains(referenceKeys)) {
  		if(this.ID!= null) {
  			if(!this.ID.equals(referenceKeys)){
  				this.references.add(referenceKeys);
  				increaseWeightOfReference(referenceKeys);
  			}
  		}
  	}
  }
  
  public void clearReferenceKeys(){
  	this.references = new ArrayList<String>();
  }

	/**
   * @return the date
   */
  public String getDate() {
  	return date;
  }

	/**
   * @param date the date to set
   */
  public void setDate(String date) {
  	
  	this.date = date;
  }

	/**
   * @return the version
   */
  public String getVersion() {
  	return version;
  }

	/**
   * @param version the version to set
   */
  public void setVersion(String version) {
  	this.version = version;
  }

	/**
   * @return the status
   */
  public String getStatus() {
  	return status;
  }

	/**
   * @param status the status to set
   */
  public void setStatus(String status) {
  	this.status = status;
  }

	/**
   * @return the author
   */
  public String getAuthor() {
  	return author;
  }

	/**
   * @param author the author to set
   */
  public void setAuthor(String author) {
  	author = author;
  }

	/**
   * @return the standardID
   */
//  public String getStandardID() {
//  	return StandardID;
//  }

	/**
   * @param standardID the standardID to set
   */
//  public void setStandardID(String standardID) {
//  	StandardID = standardID;
//  }

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}


	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public Element htmlElement;
	
	public Element getHtmlElement() {
		return htmlElement;
	}
	
	public void setHtmlElement(Document htmlDocument){
		if(htmlDocument != null){
			this.htmlElement = htmlDocument.body();
			this.htmlElement.setBaseUri(getFileName(linkToStandard));
		} else {
			this.htmlElement = null;
		}
	}
	
	private String getFileName(String linkOfStandard ) {
		String[] partedLinkOfStandard = linkOfStandard.split("/");
		if(!partedLinkOfStandard[partedLinkOfStandard.length-1].contains(".")) { 
			return linkOfStandard+"/index.html";
		}
		return linkOfStandard;
	}

	@Override
	public String toString() {
	  // TODO Auto-generated method stub
	  return "[ID:"+ID+" title: "+title+", URL: "+linkToStandard+", referenceKeys: "+references+", date: "+date+", version: "+version+", status: "+status+", Author: "+author+", Kind: "+kind+", CustomAttibutes: "+customAttributes.toString()+", countings: { Chars:"+countedCharacter +" , Words: "+countedWords+"},linkedByDirectoryPage: "+linkedByDirectoryPage+",MultipleValuesMap: "+setMap+"]";
	}

	@Override
	public String getJexleName() {
		return jexleName;
	}
	
	public String getAttribute(String nameOfCustomAttribute){
		String result = customAttributes.get( nameOfCustomAttribute );
		if(result == null){
			log.trace("Es wurde ein benutzerdefiniertes Attribut("+nameOfCustomAttribute+") abgefragt, welches nicht exsistiert. Folgende Attribute exsisiteren zu diesen Zeitpunkt: "+customAttributes.toString());
			return null;
		}
		return customAttributes.get( nameOfCustomAttribute );
	}
	
	public void setAttribute(String sourceNameOfCustomAttrbite,String value){
		customAttributes.put(sourceNameOfCustomAttrbite, value);
	}
	
	public Long getCountedCharacter() {
		return countedCharacter;
	}

	public void setCountedCharacter(Long countedCharacter) {
		this.countedCharacter = countedCharacter;
	}

	public Long getCountedWords() {
		return countedWords;
	}

	public void setCountedWords(Long countedWords) {
		this.countedWords = countedWords;
	}

	@Override
	public Object getCurrentElement() {
		// TODO Auto-generated method stub
		return htmlElement;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void addReference(String id){
		if(references.contains(id)){
			increaseWeightOfReference(id);
		}else{
			references.add(id);
			increaseWeightOfReference(id);
		}
	}
	
	private void increaseWeightOfReference(String id){
		if(!referenceWeight.containsKey(id)){
			referenceWeight.put(id, 0);
		}
		int weight = referenceWeight.get(id);
		weight++;
		referenceWeight.put(id, weight);
	}
	
	public int getReferenceWeight(String reference) {
		if(referenceWeight.containsKey(reference)) {
			return referenceWeight.get(reference);
		}
		return 1;
	}
	
	List<HtmlElementWrapper> elements;
	public void setCurrentHTMLElements(List<HtmlElementWrapper> elements) {
		this.elements = elements;
	}
	
	public List<HtmlElementWrapper> getCurrentHTMLElements(){
		return this.elements;
	}

	@Override
	public int compareTo(Standard compareObject) {
		int current = versionToInt(this.getDate());
		int compare = versionToInt(compareObject.getDate());
		if(current != -1 && compare != -1){
			if(current < compare) {
				return -1;
			} else if(current == compare) {
				return 0;
			}else {
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 * Konvertiert eine Version zu einem Int und macht sie somit vergleichbar.
	 * @param version
	 * @return
	 */
	private int versionToInt(String version) {
		Pattern intPattern = Pattern.compile("\\d+", Pattern.CASE_INSENSITIVE );
		if(version != null) {
		Matcher matcher = intPattern.matcher(version);
		String cocatInt = "";
		while(matcher.find())
    {
			cocatInt += matcher.group();
    }
		return Integer.parseInt(cocatInt);
		}
		return -1;
	}
	
	Map<String,Set<String>> setMap= new HashMap<String,Set<String>>(); 
	/**
	 * Speichert MultiStingAttribute in dem Standard.
	 * 
	 * @param nameOFAttribute
	 * @param setOfValues
	 */
	public void setMultiValueAttribute(String nameOFAttribute, Set<String> setOfValues){
	  if(setOfValues.size()>0){
	    if(!Webstructure.possibleTypesOfMultiValueAttributes.containsKey(nameOFAttribute)){
	      Webstructure.possibleTypesOfMultiValueAttributes.put(nameOFAttribute, setOfValues);
	    }else {
	      Set<String> tempSet = Webstructure.possibleTypesOfMultiValueAttributes.get(nameOFAttribute);
	      tempSet.addAll(setOfValues);
	    }
  	  if(!setMap.containsKey(nameOFAttribute)){
  	    setMap.put(nameOFAttribute, setOfValues);
  	    
  	  }else {
  	    Set<String> tempSet = setMap.get(nameOFAttribute);
    	  tempSet.addAll(setOfValues);
  	  }
	  }
	}
	
	public Set<String> getMultiValueAttribute(String nameOFAttribute){
   return setMap.get(nameOFAttribute);
  }
	
}

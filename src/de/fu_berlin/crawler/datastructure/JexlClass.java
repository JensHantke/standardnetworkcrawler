package de.fu_berlin.crawler.datastructure;

public interface JexlClass {
	
	public String getJexleName();

	public Object getCurrentElement();

	public String getText();

}

package de.fu_berlin.crawler.datastructure;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Wrapper fuer das HTMLElement von JSoup.
 * 
 * @author Jens Hantke
 *
 */
public class HtmlElementWrapper implements JexlClass {

	private String link;
	private String href;
	private String text;
	public Element currentElement;

	public void setHref(String href) {
		this.href = href;
	}


	public HtmlElementWrapper(Element currentElement) {
		this.currentElement = currentElement;
	}
	
	public HtmlElementWrapper(String href, String text) {
    this.href = href;
    this.text = text;
  }

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		if(this.href == null){
			return currentElement.attr("abs:href");
		} else {
			return this.href;
		}
		
	}

	/**
	 * Wichtig: Der Text kann auch gesetzt werden. Wenn dies der Fall ist, dann wird der Gesetzte zurueck gegeben.
	 * @return the text
	 */
	public String getText() {
		if(this.text==null){
			return currentElement.text();
		}else{
			return this.text;
		}
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return currentElement.attr("value");
	}

	/**
	 * Gibt ein nicht vordefiniertes Attribut des HTMLElements zurueck.
	 * 
	 * @param attributeName
	 * @return
	 */
	public String getSpecificAttribute(String attributeName) {
		return currentElement.attr(attributeName);
	}

	@Override
	public String toString() {
		if (currentElement != null) {
			return "[Tag: " + currentElement.tag() + ", Text: " + this.getText() + ", href: "+this.getHref()+"]";
		}
		return null;
	}

	@Override
	public String getJexleName() {
		return "currentHtmlElement";
	}
	
	public HtmlElementWrapper getParent(Element element) {
		return new HtmlElementWrapper(element.parent());
	}
	
	public Elements elementFinder(Element element, String query) {
		return element.select(query);
	}	
	
	public void setAttribute(String attributeKey, String attributeValue){
	  if(currentElement != null){
	    currentElement.attr(attributeKey, attributeValue);
	  }
	}
	
	public String getCssClass() {
		return currentElement.className().toString();
	}

	@Override
	public Object getCurrentElement() {
		return currentElement;
	}
	
	public HtmlElementWrapper getNext(Element element){
		return new HtmlElementWrapper(element.nextElementSibling());
	}
	
	public String getBaseURI(){
		return currentElement.baseUri();
		
	}

}

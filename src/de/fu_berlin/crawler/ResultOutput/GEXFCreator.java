package de.fu_berlin.crawler.ResultOutput;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.jsoup.helper.StringUtil;

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.WebComponents.Version;
import de.fu_berlin.crawler.WebComponents.Webstructure;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.JexlExecutor;

/**
 * Erstellt eine GraphenDatei im Gephi Format GEXF.
 * 
 * @author Jens Hantke
 *
 */
public class GEXFCreator implements Creator {
	
	private static final String END_DATE_IF_INFINITIY = "2013-06-20";

  String gexf ="";
	
	String graph_id ="";
	String graph_edgedefault ="";
	String GRAPHML_OUTPUT_DIR = "result/graphml/";
	String datePattern = null;
	ConfigNode keys;
	
	static Logger log = Logger.getLogger(GEXFCreator.class);
	
	Version version;
	
	boolean nestesdGraph = false;
	
	public GEXFCreator(ConfigNode graphMLCreatorNode){
		version = new Version(graphMLCreatorNode);
		Map<String, String> attributes = graphMLCreatorNode.getAttributes();
		graph_edgedefault = attributes.get("graph.edgedefault").toString();
		graph_id = attributes.get("graph.id").toString();
		if(attributes.get("datePattern") != null) {
		  datePattern = attributes.get("datePattern").toString();
		}
		keys = Config.getConfigNode("keys", graphMLCreatorNode);
		generateHead();
		genereateKeys();
	}
	
	
	private void generateHead() {
		gexf = "<gexf xmlns=\"http://www.gexf.net/1.2draft\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd\" version=\"1.2\">" +
				"<meta lastmodifieddate=\"2013-06-14\">" +
				"<creator>StandardNetworkCrawler</creator>" +
				"<description>GEXF_"+Webstructure.organisationName+"</description>" +
				"</meta>" +
				"<graph mode=\"dynamic\" defaultedgetype=\""+graph_edgedefault+"\" timeformat=\"date\" >"; //  
		
	}
	
  /**
   * Erstellt die Attributdefinitionen.
   */
	private void genereateKeys() {
		String key = "";
		key += "<attributes class=\"node\" mode=\"static\">";
		for(ConfigNode node : keys.getChildren()) {
			key += "<attribute";
			for(Attribute attr : node.getXmlNode().getAttributes()) {
				key += " "+attr.getName()+"=\""+attr.getValue()+"\"";
			}
			key += " />";
		}
		
		key += "<attribute title=\"durationTillNext\" type=\"int\" for=\"node\" id=\"durationTillNext\"/>";
		key += "<attribute title=\"durationTillEnd\" type=\"int\" for=\"node\" id=\"durationTillEnd\"/>";
		key += "<attribute title=\"multiStringAttributeIndex\" type=\"string\" for=\"node\" id=\"multiStringAttributeIndex\"/>";
		for(Entry<String, Set<String>> entry :Webstructure.possibleTypesOfMultiValueAttributes.entrySet()){
		  for(String possibleValue: entry.getValue()){
		    possibleValue = escapeXMLString(possibleValue);
		    key += "<attribute title=\""+possibleValue+"\" type=\"boolean\" for=\"node\" id=\""+possibleValue+"\"/>";
		  }
		}
		key += "</attributes>";
		gexf += key;
	}
	
	/**
	 * Wrapper um die Erstellung von Kanten und Knoten. Hier wird Arbeit fuer die NestGraphs verrichtet.
	 */
	public void generateNodesAndEdges(List<Standard> standardList) {
		List<Standard> workwith = new ArrayList<Standard>();
		for(Standard item: standardList)
			try {
				workwith.add(item.clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		workwith = version.processStandards(workwith);
		if(version.nestedStandard != null) {
			nestesdGraph = true;
			generateNestedNodes(version.nestedStandard);
		} else {
			generateNodes(workwith);
		}
		generateEdges(workwith);
		genereateEnd();
		workwith = null;
	}
	
	private void generateNodes(List<Standard> standardList) {
		int nodeCounter = 0;
		for (Standard standard : standardList) {
			nodeCounter++;
			gexf += generateNode(standard);
		}
		log.info(nodeCounter + " Knoten wurden in die GraphML Datei geschrieben.");
	}
	
	
	private String generateNode(Standard standard) {
		String label = "";
		String strdate = "";
		String localGexf = "";
		String durationTillNext = null;
		String durationTillEnd = null;
		Date nowEndDate = null;
		SimpleDateFormat dateFormatter = new SimpleDateFormat(Config.INTERNTIMESTAMP, Locale.ENGLISH);
		try {
      nowEndDate = dateFormatter.parse(END_DATE_IF_INFINITIY);
    } catch (ParseException e) {
      e.printStackTrace();
    }
		JexlExecutor jexleExecutor1 = new JexlExecutor(standard);
		// Verarbeiten das Datums Pattern, finden Start und Ende heraus 
		// Berechnen die Gueltigkeit bis zur naechsten Version und bis zum Ende
    if (datePattern != null) {
      String stringDate = (String) jexleExecutor1.getStringResult("currentStandard.date");
      Object end = standard.getAttribute("end");
      if (stringDate != null) {
        Date startDate = null;
        Date endDate = null;
        try {
          startDate = dateFormatter.parse(stringDate);
          if (end != null) {
            endDate = dateFormatter.parse(end.toString());
          }
        } catch (ParseException e) {
          e.printStackTrace();
        }
        if (startDate != null) {
            strdate += "start=\"" + new SimpleDateFormat(datePattern).format(startDate) + "\" ";
          if (end != null) {;
            durationTillNext = Days.daysBetween(new DateTime(startDate) , new DateTime(endDate) ).getDays()+"";
            strdate += "end=\"" + new SimpleDateFormat(datePattern).format(endDate) + "\" ";
            durationTillEnd = Days.daysBetween(new DateTime(startDate) , new DateTime(nowEndDate) ).getDays()+""; 
          } else {
              durationTillNext = Days.daysBetween(new DateTime(startDate) , new DateTime(nowEndDate) ).getDays()+"";
              durationTillEnd = Days.daysBetween(new DateTime(startDate) , new DateTime(nowEndDate) ).getDays()+""; 
          }
        }
      }
    }
    // Speichern die Titel, in GEXF ist das direkt im Knoten, kein Attribut
		for (ConfigNode key : keys.getChildren()) {
			Map<String, String> attributeMap1 = key.getAttributes();
			if (attributeMap1.get("title").toString().equals("label")) {
				if (jexleExecutor1.execute(attributeMap1.get("Source").toString()) != null && !jexleExecutor1.execute(attributeMap1.get("Source")).equals("")) {
					label = "label=\"" + escapeXMLString(jexleExecutor1.execute(attributeMap1.get("Source").toString()).toString())+ "\" ";
				} else {
					label = "label=\"" + escapeXMLString(standard.getID())+ "\" ";
				} 
			}
		}
		jexleExecutor1 = null;
		localGexf += "<node id =\"" + standard.getID() + "\" " + strdate + " " + label + " ><attvalues>";
		for (ConfigNode key1 : keys.getChildren()) {
			Map<String, String> attributeMap = key1.getAttributes();
			JexlExecutor jexleExecutor = new JexlExecutor(standard);
			if (!attributeMap.get("Source").contains(".getMultiValueAttribute(")) {
				if (jexleExecutor.execute(attributeMap.get("Source").toString()) != null) {
					localGexf += "<attvalue for=\"" + attributeMap.get("id").toString() + "\"  value=\"" + escapeXMLString(jexleExecutor.execute(attributeMap.get("Source").toString()).toString()) + "\" />";
				}
			} else {
			  if (jexleExecutor.execute(attributeMap.get("Source").toString()) != null) {
			    @SuppressWarnings("unchecked")
          Set<String> multiStringAttribute = (Set<String>) jexleExecutor.execute(attributeMap.get("Source"));
			     localGexf += "<attvalue for=\"multiStringAttributeIndex\"  value=\""+escapeXMLString(StringUtil.join(multiStringAttribute, ";"))+"\" />";
			     for(String possibleValue: multiStringAttribute){
			       possibleValue = escapeXMLString(possibleValue);
			       localGexf += "<attvalue for=\"" + possibleValue + "\"  value=\"true\" />"; 
			     }
			  }
			}
		}
		if(durationTillNext != null && !durationTillNext.equals("") ){
			localGexf += "<attvalue for=\"durationTillNext\"  value=\"" + escapeXMLString(durationTillNext) + "\" />";
		}
		if(durationTillEnd != null && !durationTillEnd.equals("") ){
      localGexf += "<attvalue for=\"durationTillEnd\"  value=\"" + escapeXMLString(durationTillEnd) + "\" />";
    }
		
		localGexf += "</attvalues></node>";
		return localGexf;
	}
	
	/**
	 * Falls ein Nested also ein Graph bei dem Knoten in Knoten untergliedert sind.(Histogram)
	 * 
	 * @param nestedStandard
	 */
	private void generateNestedNodes(Map<String, List<Standard>> nestedStandard) {
		for(Map.Entry<String, List<Standard>> standard:nestedStandard.entrySet()){
			List<Standard> listOfVersions = standard.getValue();
			int sizeOfList = listOfVersions.size();
			gexf +="<node label=\""+standard.getKey()+"\" id=\""+standard.getKey()+"\"  ><nodes>";
			for(int i =0 ; i<sizeOfList ;i++){
				gexf += generateNode(listOfVersions.get(i));
			}
			gexf +="</nodes></node>";
		}
	}
	
	private void generateEdges(List<Standard> standardList) {
		int edgeCounter =0;
		Map<String,Standard> idToStandard = new HashMap<String,Standard>();
		for(Standard standard : standardList){
			idToStandard.put(standard.getID(), standard);
		}
		StringBuilder str = new StringBuilder();
		for(Standard standard : standardList){
			for(String references : standard.getReferenceKeys()){
				if(idToStandard.containsKey(references)){
//					Standard target = idToStandard.get(references);
//					String start ="start = \""+target.getDate()+"\"";
//					String end = target.getAttribute("end");
//					if(end!=null){
//						end = "end = \""+end+"\"";
//					}else {
//						end ="";
//					}
					edgeCounter++;
					str.append( "<edge id =\""+standard.getID()+"__"+references+"\" source =\""+standard.getID()+"\" target =\""+references+"\" weight= \""+standard.getReferenceWeight(references)+"\" >");
					str.append("</edge>");
				}
			}
		}
		gexf += str.toString();
		log.info(edgeCounter+" Kanten wurden in die GraphML Datei geschrieben.");
	}
	
	private void genereateEnd() {
		gexf += "</graph></gexf>";
	}
	
	/**
	 * Escapen Zeichen die in XML nicht als Value vorkommen duerfen.
	 * 
	 * @param source
	 * @return
	 */
	private String escapeXMLString(String source) {
		source = source.replace("&", "&amp;");
		source = source.replace("\"", "&quot;");
		source = source.replace("<", "&lt;");
		source = source.replace(">", "&gt;");
		return source;
	}
	
	/**
	 * Erstellen lesbare(formatierte) Graphen-Dateien.
	 * Kaennen die Dateien, bei Fehlern/Problemen spaeter noch lesen.
	 * 
	 */
	private void formatXML(){
		Transformer transformer;
		try {
			Writer outWriter = new StringWriter();
			StreamResult result = new StreamResult( outWriter );
			transformer = TransformerFactory.newInstance()
			    .newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT,"1");
			transformer.transform(new StreamSource(new StringReader(gexf)),
					result);
			gexf = outWriter.toString();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	public String writeTestGraphML(String organisation){
		formatXML();
		PrintWriter pw;
    try {
	    pw = new PrintWriter(GRAPHML_OUTPUT_DIR+CrawlerTimeUtils.getFormatedTime()+"__"+graph_id+".gexf");
			pw.print(gexf);
			pw.close();
    } catch (FileNotFoundException e) {
	    e.printStackTrace();
    }
    gexf = null;
		return CrawlerTimeUtils.getFormatedTime()+"__"+graph_id+".gexf";
	}


  @Override
  public String getID() {
    return graph_id;
  }

}

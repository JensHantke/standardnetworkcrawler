package de.fu_berlin.crawler.ResultOutput;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Element;

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;

import de.fu_berlin.crawler.Attribute.ReferenceCollector;
import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.Configuration.SaxXmlParser;
import de.fu_berlin.crawler.datastructure.Standard;
import de.fu_berlin.crawler.utils.CrawlerTimeUtils;
import de.fu_berlin.crawler.utils.JexlExecutor;
import de.fu_berlin.crawler.WebComponents.Version;

public class GraphMLCreator implements  Creator {
	
	String graphML ="";
	String graph_id ="";
	String graph_edgedefault ="";
	String GRAPHML_OUTPUT_DIR = "result/graphml/";
	
	ConfigNode keys;
	
	static Logger log = Logger.getLogger(GraphMLCreator.class);
	
	Version version;
	
	public GraphMLCreator(ConfigNode graphMLCreatorNode){
		//ConfigNode graphMLCreatorNode = Config.getConfigNode("GraphMLCreator", configNode);
	
		version = new Version(graphMLCreatorNode);
		Map<String, String> attributes = graphMLCreatorNode.getAttributes();
		graph_edgedefault = attributes.get("graph.edgedefault").toString();
		graph_id = attributes.get("graph.id").toString();
		keys = Config.getConfigNode("keys", graphMLCreatorNode);
		generateHead();
		genereateKeys();
	}
	
	
	private void generateHead() {
		graphML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\""+
				"    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""+
				"    xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns"+
				"     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\"><graph id=\""+graph_id+"\" edgedefault=\""+graph_edgedefault+"\">";
	}

  /**
   * Erstellt die Attributdefinitionen.
   */
	private void genereateKeys() {
		String key = "";
		for(ConfigNode node : keys.getChildren()) {
			key += "<key";
			for(Attribute attr : node.getXmlNode().getAttributes()) {
				key += " "+attr.getName()+"=\""+attr.getValue()+"\"";
			}
			key += " />";
		}
		key += "<key id=\"weight\" for=\"edge\" attr.name=\"weight\" attr.type=\"int\"/>";
		graphML += key;
	}
	
	public void generateNodesAndEdges(List<Standard> standardList) {
		List<Standard> workwith = new ArrayList<Standard>();
		for(Standard item: standardList){
			try {
				workwith.add(item.clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		workwith = version.processStandards(workwith);
		generateNodes(workwith);
		generateEdges(workwith);
		genereateEnd();
		workwith = null;
	}
	
	private void generateNodes(List<Standard> standardList) {
		int nodeCounter =0;
		for(Standard standard : standardList){
			nodeCounter ++;
			graphML += "<node id =\""+standard.getID()+"\">";
			for(ConfigNode key : keys.getChildren()) {
				Map<String,String> attributeMap = key.getAttributes();
				JexlExecutor jexleExecutor = new JexlExecutor(standard);
				if(attributeMap.get("attr.name").toString().equals("label")) {
					// Falls das Label nicht vorhanden ist, dann setzten wir die ID des Standards ein.
					if(jexleExecutor.execute(attributeMap.get("Source")) != null && !jexleExecutor.execute(attributeMap.get("Source")).equals("")) {
						graphML += "<data key=\""+attributeMap.get("attr.name").toString()+"\">" + escapeXMLString(jexleExecutor.execute(attributeMap.get("Source").toString()).toString()) + "</data>";
					} else {
						graphML += "<data key=\""+attributeMap.get("attr.name").toString()+"\">" + escapeXMLString(standard.getID()) + "</data>";
					}
				} else {
					if(jexleExecutor.execute(attributeMap.get("Source").toString()) != null) {
						graphML += "<data key=\""+attributeMap.get("attr.name").toString()+"\">" + escapeXMLString(jexleExecutor.execute(attributeMap.get("Source").toString()).toString()) + "</data>";
					}
				}
			}
			graphML += "</node>";
		}
		log.info(nodeCounter+" Knoten wurden in die GraphML Datei geschrieben.");
	}
	
	private void generateEdges(List<Standard> standardList) {
		int edgeCounter =0;
		for(Standard standard : standardList){
			for(String references : standard.getReferenceKeys()){
				edgeCounter++;
				graphML += "<edge id =\""+standard.getID()+"__"+references+"\" source =\""+standard.getID()+"\" target =\""+references+"\" >";
				graphML += "<data key=\"weight\">" + standard.getReferenceWeight(references)+ "</data>";
				graphML += "</edge>";
			}
		}
		log.info(edgeCounter+" Kanten wurden in die GraphML Datei geschrieben.");
	}
	
	private void genereateEnd() {
		graphML += "</graph></graphml>";
	}
	
	private String escapeXMLString(String source) {
		source = source.replace("&", "&amp;"); // TODO: "&amp;" => ??
		source = source.replace("\"", "&quot;");
		source = source.replace("<", "&lt;");
		source = source.replace(">", "&gt;");
		
		return source;
	}
	
	private void formatXML(){
		Transformer transformer;
		try {
			Writer outWriter = new StringWriter();
			StreamResult result = new StreamResult( outWriter );
			transformer = TransformerFactory.newInstance()
			    .newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT,"1");
			transformer.transform(new StreamSource(new StringReader(graphML)),
					result);
			graphML = outWriter.toString();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	public String writeTestGraphML(String organisation){
		formatXML();
		PrintWriter pw;
    try {
	    pw = new PrintWriter(GRAPHML_OUTPUT_DIR+CrawlerTimeUtils.getFormatedTime()+"__"+graph_id+".graphml");
			pw.print(graphML);
			pw.close();
    } catch (FileNotFoundException e) {
	    e.printStackTrace();
    }
    graphML = null;
		return CrawlerTimeUtils.getFormatedTime()+"__"+graph_id+".graphml";
	}


  @Override
  public String getID() {
    return graph_id;
  }

}

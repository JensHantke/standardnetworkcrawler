package de.fu_berlin.crawler.ResultOutput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fu_berlin.crawler.Configuration.Config;
import de.fu_berlin.crawler.Configuration.ConfigNode;
import de.fu_berlin.crawler.datastructure.Standard;
/**
 * Verwaltet die GraphenAusgabeFormate und startet das Ausgeben eines dieser Formate.
 * 
 * @author Jens Hantke
 *
 */
public class GraphCreator {
	ConfigNode graphCreatorNode;
	
	List<Creator> allGraphCreators;
	String organisation;
	
	public GraphCreator(ConfigNode parentPageNode, String organisation) {
		graphCreatorNode = Config.getConfigNode("GraphCreator", parentPageNode);
		if(graphCreatorNode != null){
			List<ConfigNode>allGraphCreatorsNodes = Config.getConfigNodes("GraphMLCreator", graphCreatorNode);
			allGraphCreators = new ArrayList<Creator>();
			if(graphCreatorNode != null) { // Fuer den Fall das es keinen ContentExtractor gibt.
				allGraphCreatorsNodes = graphCreatorNode.getChildren();
			} 
			for(ConfigNode graphNode : allGraphCreatorsNodes) {
				if(graphNode.getName().equals("GraphMLCreator")){
					allGraphCreators.add(new GraphMLCreator(graphNode));
					continue;
				}
				if(graphNode.getName().equals("GEXFCreator")){
					allGraphCreators.add(new GEXFCreator(graphNode));
				}
			}
			this.organisation = organisation;
		}
	}
	
	public Map<String,String> printGraphOutput(List<Standard> standardList) {
		
		Map<String,String> outputGraphs = new HashMap<String,String>();
		if(graphCreatorNode != null){
			for(Creator graph: allGraphCreators) {
				graph.generateNodesAndEdges(standardList);
				outputGraphs.put(graph.getID(),graph.writeTestGraphML(organisation));
				graph = null;
			}
			allGraphCreators = null;
		}
		return outputGraphs;
	}

}

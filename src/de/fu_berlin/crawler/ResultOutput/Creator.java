package de.fu_berlin.crawler.ResultOutput;

import java.util.List;

import de.fu_berlin.crawler.datastructure.Standard;

public interface Creator {
	public String writeTestGraphML(String organisation);
	public void generateNodesAndEdges(List<Standard> standardList);
	public String getID();
}

package de.fu_berlin.crawler;


import org.apache.log4j.Logger;
/**
 * Main-Methode der ausfuehrtbaren JAR.
 * Fragt die Parameter ab.
 * 
 * @author Jens Hantke
 *
 */
public class CrawlerMain {

	static Logger logger = Logger.getLogger(CrawlerMain.class);

	public static void main(String[] args) {
		logger.info("CrawlerMain");
		if(args.length<=0){
		  System.out.println("Parameter fuer den Aufruf: 'Pfad zur Konfiguration'");
		}else {
		  String pathToConfig = args[0];
		  String[] xml = {pathToConfig};
      ConfigLoader.main(xml);
		}
	}

}
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
 
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
 
  <xsl:template match="/">
    <html>
      <head> <title>IEFT RFCs</title> </head>
      <body>
        <table>
          <xsl:apply-templates>
          </xsl:apply-templates>
        </table>
      </body>
    </html>
  </xsl:template>
 
  <xsl:template match="rfc-entry">
    <tr>
    <td>
      <a href="#"><xsl:value-of select="doc-id"/></a>
    </td>
    <td>
      <xsl:value-of select="title"/>
     </td>
    <td>
      <xsl:value-of select="publication-status"/>
     </td>
     <td>
      <xsl:value-of select="date/month"/>_<xsl:value-of select="date/year"/>
     </td>
     <!-- <td>
      <xsl:value-of select="updates"/>
     </td> -->
    </tr>
    
  </xsl:template>
 
</xsl:stylesheet>